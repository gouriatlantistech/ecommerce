@extends('layouts.menu')

@section('title')
India's Most Popular Jewellery Shopping Website : SSJ Jewellery
@endsection

@section('content')

<script>
$(window).on('popstate', function(event) {
 alert("pop");
});
</script>

<div class="ps-section--vendor ps-vendor-about">
            <div class="container">
                <br><br><br>
                <div class="ps-section__content">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 ">
                            
                            
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                            <div class="ps-block--icon-box-2">
                                <div class="ps-block__thumbnail" style="text-align: center;"><img src="https://www.mahindrafirstchoiceservices.com/assests/images/bd/success-2.gif" alt=""></div>
                              
                                <div class="ps-block__content" style="text-align: center;">
                                  <br>
                                    <h2>Booking Successfull</h2>
                                    <div class="ps-block__desc" data-mh="about-desc">
                                 
                                        @if(Session::has('OrderID'))
                                        Order ID : <b>{{ Session::get('OrderID') }}</b>   ||   
                                    @endif
                                    @if(Session::has('amount'))
                                        Total Payble Amount : <b>₹ {{ Session::get('amount') }}</b><BR>
                                    @endif
                                    
                                  
                                        <b>Thank You for Your Purchase. Order again.</b><br>
                                        <p>Payment successfully completed. 
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 ">
                            
                        </div>
                    </div>
                </div>
                <br><br><br>
            </div>
        </div>





@endsection