@extends('layouts.menu')

@section('title')
Shopping Cart : SSJ Jewellery
@endsection

@section('content')

 <!-- ========== MAIN CONTENT ========== -->
 <main id="content" role="main" class="cart-page">
            <!-- breadcrumb -->
            <div class="bg-gray-13 bg-md-transparent">
                <div class="container">
                    <!-- breadcrumb -->
                    <div class="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Cart</li>
                            </ol>
                        </nav>
                    </div>
                    <!-- End breadcrumb -->
                </div>
            </div>
            <!-- End breadcrumb -->

            <div class="container" style="    min-height: 248px;">
                <div class="mb-4">
                    <h1 class="text-center">Shopping Cart</h1>
                </div>
              <span id="cart_view" ></span>
            </div>
        </main>
        <!-- ========== END MAIN CONTENT ========== -->
           
        <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
    <script>
$(document).ready(function(){
    
    
show_cart();

});

  function show_cart(){
    

 var token=$("#_token").val();

$.ajax({

 url:'/cart_page_ajax',

 type:'POST',

 data:{_token:token},

success:function(response)
 {




 $('#cart_view').html(response);
 
 sub_total();

 
}



 });
}

function remove_item(id,price_id){


remove_cart(id,price_id);

show_cart() ;



}

function up(product_id){
// alert(product_id);


var value=Number($('#val-'+product_id).val())+1;
//alert(value);

$('#val-'+product_id).val(value);

    var price= Number($('#database_price-'+product_id).val());
    var total_price=price*value;

// alert(price);
// alert(total_price);
$('#total_product_price-'+product_id).val(total_price);
$('#product_price-'+product_id).val(price);
sub_total();
update(product_id);
}
function down(product_id){
   
//alert(value);

var value=Number($('#val-'+product_id).val())-1;
 //alert(value);
 if(value>0){
 $('#val-'+product_id).val(value);
var price= Number($('#database_price-'+product_id).val());
    var total_price=price*value;

// alert(price);
// alert(total_price);
$('#total_product_price-'+product_id).val(total_price);
$('#product_price-'+product_id).val(price);
sub_total();
update(product_id);
 }
}


function update(id){
 
 var value=$('#val-'+id).val();


 var token=$("#_token").val();

 

$.ajax({

url:'/update_qty',

type:'POST',

data:{_token:token,id:id,value:value},

success:function(response)
{


}

});
}
 
function sub_total(){
    
    var total_product=$('#total_product').val();
    //alert(total_product);
var sub_total=0;
var i;
//alert($('.tp-0').val());
//var p=$('.tp-0').val();
    //alert(p);
for(i=0;i<=total_product;i++){
    var p=$('.tp-'+i).val();
   // alert(p);
    sub_total=sub_total+Number(p);
   // alert(sub_total);
}
$('#sub_total').val(sub_total);
}



</script>
    @endsection