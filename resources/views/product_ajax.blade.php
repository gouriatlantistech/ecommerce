{{$pro_count}}malayssj
@if($pro_count==0)
    <h2 style="margin-top: 50px;"><center>No Product Found</center></h2>
@else
 <div class="tab-pane fade pt-2 show active" id="pills-one-example1" role="tabpanel" aria-labelledby="pills-one-example1-tab" data-target-group="groups">
                                <ul class="row list-unstyled products-group no-gutters">

                                @foreach($pro as $proa)
                                  @php($view_product_id=$proa->product_id)
                             
                                     @php($mediator_price=$proa->mediator_price)
                                     @php($selling_price=$proa->selling_price)
                                 
                                
                                    <li class="col-6 col-md-3 col-wd-2gdot4 product-item">
                                        <div class="product-item__outer h-100">
                                            <div class="product-item__inner px-xl-4 p-3">
                                                <div class="product-item__body pb-xl-2">
                                                @php($product_image=DB::table('product_images')->where('product_id',$view_product_id)->limit(1)->get())
                                                @php($brand_name=DB::table('brands')->where('brand_id',$proa->brand_id)->first())
                                                    <div class="mb-2"><a  class="font-size-12 text-gray-5">{{$brand_name->brand_name}}</a></div>
                                                    <h5 class="mb-1 product-item__title"><a href="/product_details?id={{$view_product_id}}" class="text-blue font-weight-bold">{{$proa->product_name}}<span style="color: #878787;font-size: 12px;font-weight: normal;">({{$proa->size}})</span></a></h5>
                                                    <div class="mb-2">
                                                    @foreach($product_image as $product_image)
                                                        <a href="/product_details?id={{$view_product_id}}" class="d-block text-center"><img class="img-fluid" src="/small_product_image/{{$product_image->image}}" alt="{{$proa->product_name}}"></a>
                                                   @endforeach
                                                    </div>
                                                    <div class="flex-center-between mb-1">
                                                        <div class="prodcut-price">
                                                            <div class="text-gray-100">
                                                            
                                                            
                                                            @guest
                                                                &#8377;{{$selling_price}}
                                                            @else
                                                               @if(Auth::user()->usertype=='MEDIATOR')
                                                               &#8377;{{$mediator_price}}
                                                               @else
                                                               &#8377;{{$selling_price}}
                                                               @endif
                                                            @endguest
      
                                                            
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="d-none d-xl-block prodcut-add-cart">
                                                        <div class="text-warning mr-2">

                                                        @php($a=(int)$proa->review)
                                                    @for($i=0;$i<$a;$i++)
                                                    <small class="fas fa-star"></small>
                                                    @endfor
                                                 

                                                     @for($i=0;$i<5-$a;$i++)
                                                     <small class="far fa-star text-muted"></small>
                                                     @endfor

                                                                                                                   
                                                        </div>
                                                               
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="product-item__footer">
                                                    <div class="border-top pt-2 flex-center-between flex-wrap">
                                                        <a href="javascript:void(0)" onclick="cart({{$proa->product_id}},{{$proa->product_prices_id}})" class="text-gray-6 font-size-13"><i class="ec ec-add-to-cart mr-1 font-size-15"></i> Cart</a>
                                                        <a href="javascript:void(0)" onclick="wishlist({{$proa->product_id}},{{$proa->product_prices_id}})" class="text-gray-6 font-size-13"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                              
                                </ul>
                            </div>
                            
                            <div class="tab-pane fade pt-2" id="pills-three-example1" role="tabpanel" aria-labelledby="pills-three-example1-tab" data-target-group="groups">
                                <ul class="d-block list-unstyled products-group prodcut-list-view">
                                @foreach($pro as $proa)
                                  @php($view_product_id=$proa->product_id)
                                  @if($price==1)
                                     @php($mediator_price=$proa->mediator_price)
                                     @php($selling_price=$proa->selling_price)
                                  @else
                                      @php($product_price=DB::table('product_prices')->where('product_id',$view_product_id)->first())
                                      @php($mediator_price=$product_price->mediator_price)
                                      @php($selling_price=$product_price->selling_price)
                                  @endif
                                
                                  
                                    <li class="product-item remove-divider">
                                        <div class="product-item__outer w-100">
                                            <div class="product-item__inner remove-prodcut-hover py-4 row">
                                                <div class="product-item__header col-6 col-md-4">
                                                    <div class="mb-2">
                                                    @php($product_image=DB::table('product_images')->where('product_id',$view_product_id)->limit(1)->get())
                                                @php($brand_name=DB::table('brands')->where('brand_id',$proa->brand_id)->first())
                                                      
                                                @foreach($product_image as $product_image)
                                                        <a href="/product_details?id={{$view_product_id}}" class="d-block text-center"><img class="img-fluid" src="/small_product_image/{{$product_image->image}}" alt="{{$proa->product_name}}" style="max-width: 186px;"></a>
                                                   @endforeach
                                                      </div>
                                                </div>
                                                <div class="product-item__body col-6 col-md-5">
                                                    <div class="pr-lg-10">
                                                        <div class="mb-2"><a class="font-size-12 text-gray-5">{{$brand_name->brand_name}}</a></div>
                                                        <h5 class="mb-2 product-item__title"><a href="/product_details?id={{$view_product_id}}" class="text-blue font-weight-bold">{{$proa->product_name}}<span style="color: #878787;font-size: 12px;font-weight: normal;">({{$proa->size}})</span></a></h5>
                                                        <div class="prodcut-price mb-2 d-md-none">
                                                            <div class="text-gray-100">
                                                            
                                                            @guest
                                                                &#8377;{{$selling_price}}
                                                            @else
                                                               @if(Auth::user()->usertype=='MEDIATOR')
                                                               &#8377;{{$mediator_price}}
                                                               @else
                                                               &#8377;{{$selling_price}}
                                                               @endif
                                                            @endguest

                                                            </div>
                                                        </div>
                                                        <div class="mb-3 d-none d-md-block">
                                                            <a class="d-inline-flex align-items-center small font-size-14" href="#">
                                                                <div class="text-warning mr-2">
                                                                                    @php($a=(int)$proa->review)
                                                    @for($i=0;$i<$a;$i++)
                                                    <small class="fas fa-star"></small>
                                                    @endfor
                                                 

                                                     @for($i=0;$i<5-$a;$i++)
                                                     <small class="far fa-star text-muted"></small>
                                                     @endfor
                                                                </div>
                                                                <span class="text-secondary">({{$proa->total_review}})</span>
                                                            </a>
                                                        </div>
                                                        <ul class="font-size-12 p-0 text-gray-110 mb-4 d-none d-md-block">
                                                       
                                                        @php($product_feature=DB::table('product_features')->where('product_id',$view_product_id)->limit(3)->get())
                                                        @foreach($product_feature as $product_feature)
                                                        <li class="line-clamp-1 mb-1 list-bullet">{{$product_feature->features}}.</li>
                                                        @endforeach
                                                     
                                                             
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="product-item__footer col-md-3 d-md-block">
                                                    <div class="mb-3">
                                                        <div class="prodcut-price mb-2">
                                                            <div class="text-gray-100">
                                                           <center>
                                                            @guest
                                                                &#8377;{{$selling_price}}
                                                            @else
                                                               @if(Auth::user()->usertype=='MEDIATOR')
                                                               &#8377;{{$mediator_price}}
                                                               @else
                                                               &#8377;{{$selling_price}}
                                                               @endif
                                                            @endguest
                                                            </center>
                                                            </div>
                                                        </div>
                                                        <div class="prodcut-add-cart">
                                                            <a href="/product_details?id={{$view_product_id}}" class="btn btn-sm btn-block btn-primary-dark btn-wide transition-3d-hover">Add to cart</a>
                                                        </div>
                                                    </div>
                                                    <div class="flex-horizontal-center justify-content-between justify-content-wd-center flex-wrap">
                                                    <a href="javascript:void(0)" onclick="cart()" class="text-gray-6 font-size-13"><i class="ec ec-add-to-cart mr-1 font-size-15"></i> Cart</a>
                                                        <a href="javascript:void(0)" onclick="wishlist({{$proa->product_id}},{{$proa->product_prices_id}})" class="text-gray-6 font-size-13"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                 @endforeach
                                </ul>
                            </div>

                            @endif