
                        @guest
<i>0</i> |aubdghbjncartview| 

                         
              <div class="flex-center-between px-4 pt-2" style="display: block;">
            <center>Please login for view your cart</center>
                </div>

@else
<i>{{$cart_count}}</i>

                        |aubdghbjncartview|
                        @if($cart_count>0)


                        <ul class="list-unstyled px-3 pt-3">
                        @foreach($cart_items as $cart_item)
                                @php($view_product_id=$cart_item->product_id)
                                  @php($product_image=DB::table('product_images')->where('product_id',$view_product_id)->limit(1)->get())
                                 
                                  <li class="border-bottom pb-3 mb-3">
                                                        <div class="">
                                                            <ul class="list-unstyled row mx-n2">
                                                                <li class="px-2 col-auto">
                                                                @foreach($product_image as $product_image)
                                                                    <img class="img-fluid" src="product_image/{{$product_image->image}}" alt="{{$cart_item->product_name}}" style="width:100px">
                                                              @endforeach
                                                                </li>
                                                                <li class="px-2 col">
                                                                    <h5 class="text-blue font-size-14 font-weight-bold">{{$cart_item->product_name}}</h5>
                                                                    <span class="font-size-14">
                                                                    @php($for_price=DB::table('product_prices')->where('product_id',$view_product_id)->where('product_prices_id',$cart_item->product_price_id)->first())
                                                                    {{$cart_item->quantity}} × @if(Auth::user()->usertype=='MEDIATOR')&#8377;{{$for_price->mediator_price}} @else &#8377;{{$for_price->selling_price}} @endif
                                                                   
                                                                    
                                                                    
                                                                    </span>
                                                                </li>
                                                                <li class="px-2 col-auto">
                                                                    <a onclick="remove_cart({{$cart_item->product_id}},{{$cart_item->product_price_id}});" href="javascript:void(0)" class="text-gray-90"><i class="ec ec-close-remove"></i></a>
                                                                </li>
                                                                </ul>
                                                        </div>
                                                    </li>
                                                    @endforeach


                                                                
                                                      
                                                </ul>
                                                <div class="flex-center-between px-4 pt-2">
                                                    <a href="/cart" class="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5">View cart</a>
                                                    <a href="/checkout" class="btn btn-primary-dark-w ml-md-2 px-5 px-md-4 px-lg-5">Checkout</a>
                                                </div>


              @else

            
                <div class="flex-center-between px-4 pt-2" style="display: block;">
                <center>No item found in your cart</center>
                </div>

              
              @endif


              @endguest

