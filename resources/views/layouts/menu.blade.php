<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Title -->
        <title>@yield('title')</title>


        <!-- Required Meta Tags Always Come First -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <?php $favicon_path = "/img/fevicon/"; ?>

<!-- HTML Meta Tags -->

<meta name="description" content="Shopping firstly, easily and secure more with SSJ. India's most popular Jewellery shopping website.">

<!-- Google / Search Engine Tags -->
<meta itemprop="name" content="Online Jewellery Shopping Site : SSJ">
<meta itemprop="description" content="Shopping firstly, easily and secure more with SSj . India's most popular Jewellery shopping website.">
<meta itemprop="image" content="http://auveraa.com/img/fevicon/favicon.png">

<!-- Facebook Meta Tags -->
<meta property="og:url" content="https://auveraa.com">
<meta property="og:type" content="website">
<meta property="og:title" content="Online Jewellery Shopping Site :SSJ">
<meta property="og:description" content="Shopping firstly, easily and secure more with SSJ. India's most popular Jewellery shopping website.">
<meta property="og:image" content="http://auveraa.com/fevicon/favicon.png">

<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Online Jewellery Shopping Site : Auveraa Life Style">
<meta name="twitter:description" content="Shopping firstly, easily and secure more with SSJ Life Style. India's most popular Jewellery shopping website.">
<meta name="twitter:image" content="http://auveraa.com/img/fevicon/favicon.png">



<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">


        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">

        <!-- CSS Implementing Plugins -->
        <link rel="stylesheet" href="assets/vendor/font-awesome/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="assets/css/font-electro.css">

        <link rel="stylesheet" href="assets/vendor/animate.css/animate.min.css">
        <link rel="stylesheet" href="assets/vendor/hs-megamenu/src/hs.megamenu.css">
        <link rel="stylesheet" href="assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" href="assets/vendor/fancybox/jquery.fancybox.css">
        <link rel="stylesheet" href="assets/vendor/slick-carousel/slick/slick.css">
        <link rel="stylesheet" href="assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css">

        <!-- CSS Electro Template -->
        <link rel="stylesheet" href="assets/css/theme.css">

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link rel="stylesheet" href="Material-Toast-master\mdtoast.min.css" />
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	
  
        <style>

        
@media only screen and (max-width: 600px) {
    #mobilehidenewsletter{display:none}
}

#overlay{	
	position: fixed;
	top: 0;
	z-index: 100;
	width: 100%;
	height:100%;
	display: none;
	background: rgba(0,0,0,0.6);
}
.cv-spinner {
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;  
}
.spinner {
	width: 80px;
	height: 80px;
	border: 4px #ddd solid;
	border-top: 4px #2e93e6 solid;
	border-radius: 50%;
	animation: sp-anime 0.8s infinite linear;

}
@keyframes sp-anime {
	100% { 
		transform: rotate(360deg); 
	}
}
#text{
  position: absolute;
  top: 35%;
  left: 50%;
  font-size: 30px;
  color: white;
  transform: translate(-50%,-50%);
  -ms-transform: translate(-50%,-50%);	
 
}
#text1{
  position: absolute;
  top: 96%;
  left: 50%;
  font-size: 17px;
  color: #ffffffb8;
  
  transform: translate(-50%,-50%);
  -ms-transform: translate(-50%,-50%);
}
.is-hide{
	display:none;
}


</style> 
    </head>

    <body>
    <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>	
    <div id="overlay" style="    z-index: 1002;">
<div id="text" style="display:none">Do not press back button or refresh the page.</div>
	<div class="cv-spinner">
		<span class="spinner"></span>
	</div>
    <div id="text1" style=""><center><font style="font-size:14px">Design and develop by</font><br><span style="line-height: 0.8;">Quantex</span><center></div>
</div>
@guest
<input type="hidden" id="login_data" value="0">

@else

<input type="hidden" id="login_data" value="{{Auth::user()->id}}">
@endguest        <!-- ========== HEADER ========== -->
        <header id="header" class="u-header u-header-left-aligned-nav">
            <div class="u-header__section">
                <!-- Topbar -->
                <div class="u-header-topbar py-2 d-none d-xl-block">
                    <div class="container">
                        <div class="d-flex align-items-center">
                            <div class="topbar-left">
                                <a href="#" class="text-gray-110 font-size-13 hover-on-dark">Welcome to SSJ Jewellery</a>
                            </div>
                            <div class="topbar-right ml-auto">
                                <ul class="list-inline mb-0">
                                    
                                    
                                    <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border u-header-topbar__nav-item-no-border u-header-topbar__nav-item-border-single">
                                        <div class="d-flex align-items-center">
                                            <!-- Language -->
                                            <div class="position-relative">
                                                  @auth
                                                <a id="languageDropdownInvoker" class="dropdown-nav-link dropdown-toggle d-flex align-items-center u-header-topbar__nav-link font-weight-normal" href="javascript:;" role="button"
                                                    aria-controls="languageDropdown"
                                                    aria-haspopup="true"
                                                    aria-expanded="false"
                                                    data-unfold-event="hover"
                                                    data-unfold-target="#languageDropdown"
                                                    data-unfold-type="css-animation"
                                                    data-unfold-duration="300"
                                                    data-unfold-delay="300"
                                                    data-unfold-hide-on-scroll="true"
                                                    data-unfold-animation-in="slideInUp"
                                                    data-unfold-animation-out="fadeOut">

                                                    <span class="d-none d-sm-inline-flex align-items-center"><i class="ec ec-user mr-1"></i> My Profile</span>
                                                  
                                                </a>  @endauth

                                                <div id="languageDropdown" class="dropdown-menu dropdown-unfold" aria-labelledby="languageDropdownInvoker">
                                                    <a class="dropdown-item active" href="/profile">My Account</a>
                                                    <a class="dropdown-item" href="/order">My Order</a>
                                                    <a class="dropdown-item" href="/my_wallet">My wallet</a>
                                                    <a class="dropdown-item" href="/offer">Offer zone</a>
                                                    <a class="dropdown-item" href="/refer">Refer & Earn</a>
                                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                                                        onclick="event.preventDefault();
                                                                                        document.getElementById('logout-form').submit();">
                                                                            {{ __('Logout') }}
                                                     </a>
                                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                           @csrf
                                                     </form>

                                                </div>
                                            </div>
                                            <!-- End Language -->
                                        </div>
                                    </li>
                                    <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                        <!-- Account Sidebar Toggle Button -->
                                        @guest
                                        
                                            <i class="ec ec-user mr-1"></i> <a href="/register" style="color:black">Register</a>  <span class="text-gray-50">or</span> <a href="/login" style="color:black"> Sign in</a>
                                       
                                        @else
                                        
                                        {{ Auth::user()->name }}
                                      

                                        @endguest
                                        <!-- End Account Sidebar Toggle Button -->
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Topbar -->

                <!-- Logo-Search-header-icons -->
                <div class="py-2 py-xl-5 bg-primary-down-lg">
                    <div class="container my-0dot5 my-xl-0">
                        <div class="row align-items-center">
                            <!-- Logo-offcanvas-menu -->
                            <div class="col-auto">
                                <!-- Nav -->
                                <nav class="navbar navbar-expand u-header__navbar py-0 justify-content-xl-between max-width-270 min-width-270">
                                    <!-- Logo -->
                                    <a class="order-1 order-xl-0 navbar-brand u-header__navbar-brand u-header__navbar-brand-center" href="/" aria-label="Electro">
                                    <img src="/logo/SSJ Jewellery Logo PNG.png">
                                    </a>
                                    <!-- End Logo -->

                                    <!-- Fullscreen Toggle Button -->
                                    <button id="sidebarHeaderInvokerMenu" type="button" class="navbar-toggler d-block btn u-hamburger mr-3 mr-xl-0"
                                        aria-controls="sidebarHeader"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                        data-unfold-event="click"
                                        data-unfold-hide-on-scroll="false"
                                        data-unfold-target="#sidebarHeader1"
                                        data-unfold-type="css-animation"
                                        data-unfold-animation-in="fadeInLeft"
                                        data-unfold-animation-out="fadeOutLeft"
                                        data-unfold-duration="500">
                                        <span id="hamburgerTriggerMenu" class="u-hamburger__box">
                                            <span class="u-hamburger__inner"></span>
                                        </span>
                                    </button>
                                    <!-- End Fullscreen Toggle Button -->
                                </nav>
                                <!-- End Nav -->

                                <!-- ========== HEADER SIDEBAR ========== -->
                                <aside id="sidebarHeader1" class="u-sidebar u-sidebar--left" aria-labelledby="sidebarHeaderInvoker">
                                    <div class="u-sidebar__scroller">
                                        <div class="u-sidebar__container">
                                            <div class="u-header-sidebar__footer-offset">
                                                <!-- Toggle Button -->
                                                <div class="position-absolute top-0 right-0 z-index-2 pt-4 pr-4 bg-white">
                                                    <button type="button" class="close ml-auto"
                                                        aria-controls="sidebarHeader"
                                                        aria-haspopup="true"
                                                        aria-expanded="false"
                                                        data-unfold-event="click"
                                                        data-unfold-hide-on-scroll="false"
                                                        data-unfold-target="#sidebarHeader1"
                                                        data-unfold-type="css-animation"
                                                        data-unfold-animation-in="fadeInLeft"
                                                        data-unfold-animation-out="fadeOutLeft"
                                                        data-unfold-duration="500">
                                                        <span aria-hidden="true"><i class="ec ec-close-remove text-gray-90 font-size-20"></i></span>
                                                    </button>
                                                </div>
                                                <!-- End Toggle Button -->

                                                <!-- Content -->
                                                <div class="js-scrollbar u-sidebar__body">
                                                    <div id="headerSidebarContent" class="u-sidebar__content u-header-sidebar__content">
                                                        <!-- Logo -->
                                                        <a class="navbar-brand u-header__navbar-brand u-header__navbar-brand-center mb-3" href="/" aria-label="Electro">
                                                        <img src="/logo/SSJ Jewellery Logo PNG.png">
                                                        </a>
                                                        <!-- End Logo -->

                                                        <!-- List -->
                                                        <ul id="headerSidebarList" class="u-header-collapse__nav">
                                                        @php($cat=DB::table('cats')->get())
                                                        @foreach($cat as $key=>$cats)
                                                            <!-- Computers & Accessories -->
                                                            <li class="u-has-submenu u-header-collapse__submenu">
                                                                <a class="u-header-collapse__nav-link u-header-collapse__nav-pointer" href="/shop?cat_id={{$cats->cat_id}}" data-target="#headerSidebarComputersCollapse" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="headerSidebarComputersCollapse">
                                                                {{$cats->cat_name}}
                                                                </a>
                                                                @php($sub_cat_count=DB::table('sub_cats')->where('cat_id',$cats->cat_id)->count())
                                                                @if($sub_cat_count!=0)
                                                                <div id="headerSidebarComputersCollapse" class="collapse" data-parent="#headerSidebarContent">
                                                                    <ul class="u-header-collapse__nav-list">
                                                                   
                                                                    
                                                                    @php($sub_cat=DB::table('sub_cats')->where('cat_id',$cats->cat_id)->get())
                                                                    @foreach($sub_cat as $sub_cats)
                                                                        <li><a href="/shop?cat_id={{$cats->cat_id}}&sub_cat_id={{$sub_cats->sub_cat_id}}"><span class="u-header-sidebar__sub-menu-title">{{$sub_cats->sub_cat_name}}</span></a></li>
                                                                        @php($sub_sub_cat_count=DB::table('sub_sub_cats')->where('sub_cat_id',$sub_cats->sub_cat_id)->count())
                                                                        @if($sub_sub_cat_count!=0)
                                                                        @php($sub_sub_cat=DB::table('sub_sub_cats')->where('sub_cat_id',$sub_cats->sub_cat_id)->get())
                                                                        @foreach($sub_sub_cat as $sub_sub_cats)

                                                                        <li class=""><a class="u-header-collapse__submenu-nav-link" href="/shop?cat_id={{$cats->cat_id}}&sub_cat_id={{$sub_cats->sub_cat_id}}&sub_sub_cat_id={{$sub_sub_cats->sub_sub_cat_id}}">{{$sub_sub_cats->sub_sub_cat_name}}</a></li>
                                                        
                                                                         @endforeach
                                                                         @endif

                                                                    @endforeach
                                                                    
                                                                    </ul>
                                                                </div>
                                                                @endif
                                                            </li>
                                                            <!-- End Computers & Accessories -->
                                                        @endforeach
                                                  
                                                        </ul>
                                                        <!-- End List -->
                                                    </div>
                                                </div>
                                                <!-- End Content -->
                                            </div>
                                            <!-- Footer -->
                                            <footer id="SVGwaveWithDots" class="svg-preloader u-header-sidebar__footer">
                                                <ul class="list-inline mb-0">
                                                    <li class="list-inline-item pr-3">
                                                        <a class="u-header-sidebar__footer-link text-gray-90" href="#">Privacy</a>
                                                    </li>
                                                    <li class="list-inline-item pr-3">
                                                        <a class="u-header-sidebar__footer-link text-gray-90" href="#">Terms</a>
                                                    </li>
                                                    <li class="list-inline-item">
                                                        <a class="u-header-sidebar__footer-link text-gray-90" href="#">
                                                            <i class="fas fa-info-circle"></i>
                                                        </a>
                                                    </li>
                                                </ul>

                                                <!-- SVG Background Shape -->
                                                <div class="position-absolute right-0 bottom-0 left-0 z-index-n1">
                                                    <img class="js-svg-injector" src="assets/svg/components/wave-bottom-with-dots.svg" alt="Image Description"
                                                    data-parent="#SVGwaveWithDots">
                                                </div>
                                                <!-- End SVG Background Shape -->
                                            </footer>
                                            <!-- End Footer -->
                                        </div>
                                    </div>
                                </aside>
                                <!-- ========== END HEADER SIDEBAR ========== -->

                                <aside id="sidebarHeader2" class="u-sidebar u-sidebar--left" aria-labelledby="sidebarHeaderInvoker">
                                    <div class="u-sidebar__scroller">
                                        <div class="u-sidebar__container">
                                            <div class="u-header-sidebar__footer-offset">
                                                <!-- Toggle Button -->
                                                <div class="position-absolute top-0 right-0 z-index-2 pt-4 pr-4 bg-white">
                                                    <button type="button" class="close ml-auto"
                                                        aria-controls="sidebarHeader"
                                                        aria-haspopup="true"
                                                        aria-expanded="false"
                                                        data-unfold-event="click"
                                                        data-unfold-hide-on-scroll="false"
                                                        data-unfold-target="#sidebarHeader2"
                                                        data-unfold-type="css-animation"
                                                        data-unfold-animation-in="fadeInLeft"
                                                        data-unfold-animation-out="fadeOutLeft"
                                                        data-unfold-duration="500">
                                                        <span aria-hidden="true"><i class="ec ec-close-remove text-gray-90 font-size-20"></i></span>
                                                    </button>
                                                </div>
                                                <!-- End Toggle Button -->

                                                <!-- Content -->
                                                <div class="js-scrollbar u-sidebar__body">
                                                    <div id="headerSidebarContent" class="u-sidebar__content u-header-sidebar__content">
                                                        <!-- Logo -->
                                                        <a class="navbar-brand u-header__navbar-brand u-header__navbar-brand-center mb-3" href="/" aria-label="Electro">
                                                        <img src="/logo/SSJ Jewellery Logo PNG.png">
                                                        </a>
                                                        <!-- End Logo -->

                                                        <!-- List -->
                                                        <ul id="headerSidebarList" class="u-header-collapse__nav">
                                                            <!-- Value of the Day -->
                                                            @auth
                                                            <li class="">
                                                                <a class="u-header-collapse__nav-link font-weight-bold" style="color:blue" href="javascript:void(0);">Hii {{Auth::user()->name}}</a>
                                                            </li>
                                                            <li class="u-has-submenu u-header-collapse__submenu">
                                                                <a class="u-header-collapse__nav-link u-header-collapse__nav-pointer font-weight-bold" href="javascript:;" data-target="#headerSidebarWatchesCollapse" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="headerSidebarWatchesCollapse">
                                                                   Profile
                                                                </a>

                                                                <div id="headerSidebarWatchesCollapse" class="collapse" data-parent="#headerSidebarContent">
                                                                    <ul class="u-header-collapse__nav-list">
                                                                        
                                                                        <li><a class="u-header-collapse__submenu-nav-link" href="/profile">My account</a></li>
                                                                        <li><a class="u-header-collapse__submenu-nav-link" href="/my_order">My order</a></li>
                                                                        <li><a class="u-header-collapse__submenu-nav-link" href="/my_wallet">My wallet</a></li>
                                                                        <li><a class="u-header-collapse__submenu-nav-link" href="wishlist">My wishlist</a></li>
                                                                        <li><a class="u-header-collapse__submenu-nav-link" href="offer">Offer zone</a></li>
                                                                        <li><a class="u-header-collapse__submenu-nav-link" href="refer">Refer & Earn</a></li>
                                              
                                                                       
                                                                       
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                            @endauth
                                                            <li class="">
                                                                <a class="u-header-collapse__nav-link font-weight-bold" href="/">Home</a>
                                                            </li>
                                                            <li class="">
                                                                <a class="u-header-collapse__nav-link font-weight-bold" href="/shop">Shop</a>
                                                            </li>
                                                            <li class="">
                                                                <a class="u-header-collapse__nav-link font-weight-bold" href="/about">About Us</a>
                                                            </li>
                                                            <li class="">
                                                                <a class="u-header-collapse__nav-link font-weight-bold" href="/contact">Contact Us</a>
                                                            </li>
                                                           
                                                            @auth
                                                           
                                                            <li class="">
                                                            <a class="u-header-collapse__nav-link font-weight-bold" href="{{ route('logout') }}"
                                                                        onclick="event.preventDefault();
                                                                                        document.getElementById('logout-form').submit();">
                                                                            {{ __('Logout') }}
                                                            </a>

                                                            </li>





                                                            @else
                                                            <li class="">
                                                                <a class="u-header-collapse__nav-link font-weight-bold" href="/login">login</a>
                                                            </li>

                                                            <li class="">
                                                                <a class="u-header-collapse__nav-link font-weight-bold" href="/register">Register</a>
                                                            </li>
                                                            @endauth

                                                           

                                                           
                                                       

                                                        

                                                               
                                                        
                                                            <!-- Watches & Eyewear -->
                                                          
                                                            <!-- End Watches & Eyewear -->

                                                           

                                                          
                                                        </ul>
                                                        <!-- End List -->
                                                    </div>
                                                </div>
                                                <!-- End Content -->
                                            </div>
                                            <!-- Footer -->
                                            <footer id="SVGwaveWithDots" class="u-header-sidebar__footer" style="">
                                                <ul class="list-inline mb-0">
                                                    <li class="list-inline-item pr-3">
                                                        <a class="u-header-sidebar__footer-link text-gray-90" href="#">Privacy</a>
                                                    </li>
                                                    <li class="list-inline-item pr-3">
                                                        <a class="u-header-sidebar__footer-link text-gray-90" href="#">Terms</a>
                                                    </li>
                                                    <li class="list-inline-item">
                                                        <a class="u-header-sidebar__footer-link text-gray-90" href="#">
                                                            <i class="fas fa-info-circle"></i>
                                                        </a>
                                                    </li>
                                                </ul>

                                                <!-- SVG Background Shape -->
                                                <div class="position-absolute right-0 bottom-0 left-0 z-index-n1">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 300 126.5" style="margin-bottom: -5px; enable-background:new 0 0 300 126.5;" xml:space="preserve" class="injected-svg js-svg-injector" data-parent="#SVGwaveWithDots">
<style type="text/css">
	.wave-bottom-with-dots-0{fill:#377DFF;}
	.wave-bottom-with-dots-1{fill:#377DFF;}
	.wave-bottom-with-dots-2{fill:#DE4437;}
	.wave-bottom-with-dots-3{fill:#00C9A7;}
	.wave-bottom-with-dots-4{fill:#FFC107;}
</style>
<path class="wave-bottom-with-dots-0 fill-primary" opacity=".6" d="M0,58.9c0-0.9,5.1-2,5.8-2.2c6-0.8,11.8,2.2,17.2,4.6c4.5,2.1,8.6,5.3,13.3,7.1C48.2,73.3,61,73.8,73,69  c43-16.9,40-7.9,84-2.2c44,5.7,83-31.5,143-10.1v69.8H0C0,126.5,0,59,0,58.9z"></path>
<path class="wave-bottom-with-dots-1 fill-primary" d="M300,68.5v58H0v-58c0,0,43-16.7,82,5.6c12.4,7.1,26.5,9.6,40.2,5.9c7.5-2.1,14.5-6.1,20.9-11  c6.2-4.7,12-10.4,18.8-13.8c7.3-3.8,15.6-5.2,23.6-5.2c16.1,0.1,30.7,8.2,45,16.1c13.4,7.4,28.1,12.2,43.3,11.2  C282.5,76.7,292.7,74.4,300,68.5z"></path>
<g>
	<circle class="wave-bottom-with-dots-2 fill-danger" cx="259.5" cy="17" r="13"></circle>
	<circle class="wave-bottom-with-dots-1 fill-primary" cx="290" cy="35.5" r="8.5"></circle>
	<circle class="wave-bottom-with-dots-3 fill-success" cx="288" cy="5.5" r="5.5"></circle>
	<circle class="wave-bottom-with-dots-4 fill-warning" cx="232.5" cy="34" r="2"></circle>
</g>
</svg>
                                                </div>
                                                <!-- End SVG Background Shape -->
                                            </footer>
                                            <!-- End Footer -->
                                        </div>
                                    </div>
                                </aside>
                            </div>
                            <!-- End Logo-offcanvas-menu -->
                            <!-- Search Bar -->
                            <div class="col d-none d-xl-block">
                                <form class="js-focus-state" action="/shop" method="get">
                                    <label class="sr-only" for="searchproduct">Search</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control py-2 pl-5 font-size-15 border-right-0 height-40 border-width-2 rounded-left-pill border-primary" value="@if(isset($_GET['Product_name'])) {{$_GET['Product_name']}} @endif" name="Product_name" id="searchproduct-item" placeholder="Search for Products" aria-label="Search for Products" aria-describedby="searchProduct1" required>
                                        <div class="input-group-append">
                                            <!-- Select -->
                                            <select name="cat_id" class="js-select selectpicker dropdown-select custom-search-categories-select"
                                                data-style="btn height-40 text-gray-60 font-weight-normal border-top border-bottom border-left-0 rounded-0 border-primary border-width-2 pl-0 pr-5 py-2">
                                                <option value="one" selected>All Categories</option>
                                                @php($cat=DB::table('cats')->get())
                                                @foreach($cat as $cats)
                                                <option value="{{$cats->cat_id}}" @if(isset($_GET['cat_id'])) @if($cats->cat_id==$_GET['cat_id']) selected @endif @endif>{{$cats->cat_name}}</option>
                                                @endforeach
                                               
                                            </select>
                                            <!-- End Select -->
                                            <button class="btn btn-primary height-40 py-2 px-3 rounded-right-pill" type="submit" id="searchProduct1">
                                                <span class="ec ec-search font-size-24"></span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- End Search Bar -->
                            <!-- Header Icons -->
                            <div class="col col-xl-auto text-right text-xl-left pl-0 pl-xl-3 position-static">
                                <div class="d-inline-flex">
                                    <ul class="d-flex list-unstyled mb-0 align-items-center">
                                        <!-- Search -->
                                        <li class="col d-xl-none px-2 px-sm-3 position-static">
                                            <a id="searchClassicInvoker" class="font-size-22 text-gray-90 text-lh-1 btn-text-secondary" href="javascript:;" role="button"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                title="Search"
                                                aria-controls="searchClassic"
                                                aria-haspopup="true"
                                                aria-expanded="false"
                                                data-unfold-target="#searchClassic"
                                                data-unfold-type="css-animation"
                                                data-unfold-duration="300"
                                                data-unfold-delay="300"
                                                data-unfold-hide-on-scroll="true"
                                                data-unfold-animation-in="slideInUp"
                                                data-unfold-animation-out="fadeOut">
                                                <span class="ec ec-search"></span>
                                            </a>

                                            <!-- Input -->
                                            <div id="searchClassic" class="dropdown-menu dropdown-unfold dropdown-menu-right left-0 mx-2" aria-labelledby="searchClassicInvoker">
                                                <form class="js-focus-state input-group px-3">
                                                    <input class="form-control" type="search" placeholder="Search Product">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-primary px-3" type="button"><i class="font-size-18 ec ec-search"></i></button>
                                                    </div>
                                                </form>
                                            </div>
                                            <!-- End Input -->
                                        </li>
                                        <!-- End Search -->
                                         <li class="col d-none d-xl-block"><a href="/wishlist" class="text-gray-90" data-toggle="tooltip" data-placement="top" title="Wishlist">
                                         <div id="basicDropdownHoverInvoker" class="text-gray-90 position-relative d-flex " data-toggle="tooltip" data-placement="top" title="Wishlist">
                                                <i class="font-size-22 ec ec-favorites"></i>
                                                <span class="bg-lg-down-black width-22 height-22 bg-primary position-absolute d-flex align-items-center justify-content-center rounded-circle left-12 top-8 font-weight-bold font-size-12" id="wish_value">0</span>
                                          
                                            </div>
                                         
                                         </a></li>
                                     
                                        <li class="col pr-xl-0 px-2 px-sm-3 d-xl-none">
                                            <a href="/cart" class="text-gray-90 position-relative d-flex " data-toggle="tooltip" data-placement="top" title="Cart">
                                                <i class="font-size-22 ec ec-shopping-bag"></i>
                                                <span class="bg-lg-down-black width-22 height-22 bg-primary position-absolute d-flex align-items-center justify-content-center rounded-circle left-12 top-8 font-weight-bold font-size-12" id="cart_value">0</span>
                                             
                                            </a>
                                        </li>
                                        
                                        <li class="col pr-xl-0 px-2 px-sm-3 d-none d-xl-block">
                                            <div id="basicDropdownHoverInvoker" class="text-gray-90 position-relative d-flex " data-toggle="tooltip" data-placement="top" title="Cart"
                                                aria-controls="basicDropdownHover"
                                                aria-haspopup="true"
                                                aria-expanded="false"
                                                data-unfold-event="click"
                                                data-unfold-target="#basicDropdownHover"
                                                data-unfold-type="css-animation"
                                                data-unfold-duration="300"
                                                data-unfold-delay="300"
                                                data-unfold-hide-on-scroll="true"
                                                data-unfold-animation-in="slideInUp"
                                                data-unfold-animation-out="fadeOut">
                                                <i class="font-size-22 ec ec-shopping-bag"></i>
                                                <span class="bg-lg-down-black width-22 height-22 bg-primary position-absolute d-flex align-items-center justify-content-center rounded-circle left-12 top-8 font-weight-bold font-size-12" id="cart_value1">0</span>
                                          
                                            </div>
                                            <div id="basicDropdownHover" class="cart-dropdown dropdown-menu dropdown-unfold border-top border-top-primary mt-3 border-width-2 border-left-0 border-right-0 border-bottom-0 left-auto right-0" aria-labelledby="basicDropdownHoverInvoker">
                                               
                                            </div>
                                        </li>
                                        <li class="col d-xl-none px-2 px-sm-3">    <button id="sidebarHeaderInvokerMenu" type="button" class="navbar-toggler d-block btn u-hamburger  mr-xl-0"
                                        aria-controls="sidebarHeader"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                        data-unfold-event="click"
                                        data-unfold-hide-on-scroll="false"
                                        data-unfold-target="#sidebarHeader2"
                                        data-unfold-type="css-animation"
                                        data-unfold-animation-in="fadeInLeft"
                                        data-unfold-animation-out="fadeOutLeft"
                                        data-unfold-duration="500" style="    margin-left: 1rem !important;   margin-right: 0;">
                                        <span id="hamburgerTriggerMenu" class="u-hamburger__box">
                                            <span class="u-hamburger__inner"></span>
                                        </span>
                                    </button></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- End Header Icons -->
                        </div>
                    </div>
                </div>
                <!-- End Logo-Search-header-icons -->

                <!-- Vertical-and-secondary-menu -->
                <div class="d-none d-xl-block container">
                    <div class="row">
                        <!-- Vertical Menu -->
                        <div class="col-md-auto d-none d-xl-block">
                            <div class="max-width-270 min-width-270">
                                <!-- Basics Accordion -->
                                <div id="basicsAccordion">
                                    <!-- Card -->
                                    <div class="card border-0">
                                        <div class="card-header card-collapse border-0" id="basicsHeadingOne">
                                            <button type="button" class="btn-link btn-remove-focus btn-block d-flex card-btn py-3 text-lh-1 px-4 shadow-none btn-primary rounded-top-lg border-0 font-weight-bold text-gray-90"
                                                data-toggle="collapse"
                                                data-target="#basicsCollapseOne"
                                                aria-expanded="true"
                                                aria-controls="basicsCollapseOne">
                                                <span class="ml-0 text-gray-90 mr-2">
                                                    <span class="fa fa-list-ul"></span>
                                                </span>
                                                <span class="pl-1 text-gray-90">All Departments</span>
                                            </button>
                                        </div>
                                        <div id="basicsCollapseOne" class="collapse <?php if(basename($_SERVER['PHP_SELF'])=="index.php" || basename($_SERVER['PHP_SELF'])=="home"){ echo "show"; }?> vertical-menu"
                                            aria-labelledby="basicsHeadingOne"
                                            data-parent="#basicsAccordion">
                                            <div class="card-body p-0">
                                                <nav class="js-mega-menu navbar navbar-expand-xl u-header__navbar u-header__navbar--no-space hs-menu-initialized">
                                                    <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
                                                        <ul class="navbar-nav u-header__navbar-nav">
                                                           
                                                        @php($cat=DB::table('cats')->get())
        
                                                        @php($cat_count=DB::table('cats')->count())
                                                        @foreach($cat as $key=>$cats)
                                                            <!-- Nav Item MegaMenu -->
                                                            <li class="nav-item hs-has-mega-menu u-header__nav-item"
                                                                data-event="hover"
                                                                data-position="left">
                                                                <a id="basicMegaMenu1" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="/shop?cat_id={{$cats->cat_id}}" aria-haspopup="true" aria-expanded="false">{{$cats->cat_name}}</a>
                                                                @php($sub_cat_count=DB::table('sub_cats')->where('cat_id',$cats->cat_id)->count())
                                                                @if($sub_cat_count!=0)
                                                                <!-- Nav Item - Mega Menu -->
                                                                <div class="hs-mega-menu vmm-tfw u-header__sub-menu" aria-labelledby="basicMegaMenu1">
                                                                @if($cat_count>6)
                                                                    <div class="vmm-bg">
                                                                        <img class="img-fluid" src="/cat_background/{{$cats->cat_background}}" alt="{{$cats->cat_name}}">
                                                                    </div>
                                                                @else
                                                                <div class="vmm-bg" style="color:white">
                                                                       
                                                                </div>
                                                                @endif
                                                                   
                                                                    <div class="row u-header__mega-menu-wrapper">
                                                                    @php($sub_cat=DB::table('sub_cats')->where('cat_id',$cats->cat_id)->get())
                                                                    @foreach($sub_cat as $sub_cats)
                                                                        <div class="col mb-3 mb-sm-0">
                                                                            <span class="u-header__sub-menu-title">{{$sub_cats->sub_cat_name}}</span>
                                                                            @php($sub_sub_cat_count=DB::table('sub_sub_cats')->where('sub_cat_id',$sub_cats->sub_cat_id)->count())
                                                                            @if($sub_sub_cat_count!=0)
                                                                            <ul class="u-header__sub-menu-nav-group mb-3">
                                                                            @php($sub_sub_cat=DB::table('sub_sub_cats')->where('sub_cat_id',$sub_cats->sub_cat_id)->get())
                                                                            @foreach($sub_sub_cat as $sub_sub_cats)

                                                                                <li><a class="nav-link u-header__sub-menu-nav-link" href="/shop?cat_id={{$cats->cat_id}}&sub_cat_id={{$sub_cats->sub_cat_id}}&sub_sub_cat_id={{$sub_sub_cats->sub_sub_cat_id}}">{{$sub_sub_cats->sub_sub_cat_name}}</a></li>
                                                                               
                                                                            @endforeach
                                                                                <li>
                                                                                    <a class="nav-link u-header__sub-menu-nav-link u-nav-divider border-top pt-2 flex-column align-items-start" href="/shop?cat_id={{$cats->cat_id}}&sub_cat_id={{$sub_cats->sub_cat_id}}">
                                                                                        <div class="">All {{$sub_cats->sub_cat_name}}</div>
                                                                                        <div class="u-nav-subtext font-size-11 text-gray-30">Discover more products</div>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                            @endif
                                                                        </div>
                                                                    @endforeach
                                                                      

                                                                    </div>
                  
                                                                </div>
                                                                @endif
                                                                <!-- End Nav Item - Mega Menu -->
                                                            </li>
                                                            <!-- End Nav Item MegaMenu-->
                                                        @endforeach

                                        
                                                          
                                                        </ul>
                                                    </div>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Card -->
                                </div>
                                <!-- End Basics Accordion -->
                            </div>
                        </div>
                        <!-- End Vertical Menu -->
                        <!-- Secondary Menu -->
                        <div class="col" style="border-top-left-radius: 8px;background-color: #fed700;border-top-right-radius: 8px;">
                            <!-- Nav -->
                            <nav class="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--no-space">
                                <!-- Navigation -->
                                <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
                                    <ul class="navbar-nav u-header__navbar-nav">
                                      <!-- Featured Brands -->
                                      <li class="nav-item u-header__nav-item">
                                            <a class="nav-link u-header__nav-link" href="/" aria-haspopup="true" aria-expanded="false" aria-labelledby="pagesSubMenu">Home</a>
                                        </li>
                                        <!-- End Featured Brands -->

                                        <!-- Trending Styles -->
                                        <li class="nav-item u-header__nav-item">
                                            <a class="nav-link u-header__nav-link" href="/about" aria-haspopup="true" aria-expanded="false" aria-labelledby="blogSubMenu">About Us</a>
                                        </li>
                                        <!-- End Trending Styles -->

                                        <!-- Featured Brands -->
                                        <li class="nav-item u-header__nav-item">
                                            <a class="nav-link u-header__nav-link" href="/shop" aria-haspopup="true" aria-expanded="false" aria-labelledby="pagesSubMenu">Shop</a>
                                        </li>
                                        <!-- End Featured Brands -->

                               
                                        <!-- Gift Cards -->
                                        <li class="nav-item u-header__nav-item">
                                            <a class="nav-link u-header__nav-link" href="/contact" aria-haspopup="true" aria-expanded="false">Contact  Us</a>
                                        </li>
                                        <!-- End Gift Cards -->

                                        <!-- Button -->
                                        <li class="nav-item u-header__nav-last-item">
                                            <a class="text-gray-90" href="tel:9733118409" target="_blank">
                                            <i class="fa fa-mobile"></i>
                                            Hotline : <b>+91 9733118409</b>
                                            </a>
                                        </li>
                                        <!-- End Button -->
                                    </ul>
                                </div>
                                <!-- End Navigation -->
                            </nav>
                            <!-- End Nav -->
                        </div>
                        <!-- End Secondary Menu -->
                    </div>
                </div>
                <!-- End Vertical-and-secondary-menu -->
            </div>
        </header>
        <!-- ========== END HEADER ========== -->


              @yield('content')


        <!-- ========== FOOTER ========== -->
        <footer>
            <!-- Footer-top-widget -->
         
            <!-- End Footer-top-widget -->
            <!-- Footer-newsletter -->


            <div class="bg-primary py-3">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-7 mb-md-3 mb-lg-0">
                            <div class="row align-items-center">
                                <div class="col-auto flex-horizontal-center">
                                    <i class="ec ec-newsletter font-size-40"></i>
                                    <h2 class="font-size-20 mb-0 ml-3">Sign up to Newsletter</h2>
                                </div>
                                <div class="col my-4 my-md-0" id="mobilehidenewsletter">
                                  
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <!-- Subscribe Form -->
                            <span class="js-validate js-form-message">
                            <label class="sr-only" for="subscribeSrEmail">Email address</label>
                                <div class="input-group input-group-pill">
                            @guest
                               
                                    <input type="email" class="form-control border-0 height-40" name="email" id="subscribeSrEmail" placeholder="Email address" id="subscribee_email" aria-label="Email address" aria-describedby="subscribeButton" required
                                    data-msg="Please enter a valid email address.">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-dark btn-sm-wide height-40 py-2" id="subscribee">Subscribe</button>
                                    </div>
                                
                                @else
                                @php($id= Auth::user()->id )
                                @php($Subscribe=DB::table('subscribers')->where('user_id',$id)->count())
                                <input type="email" class="form-control border-0 height-40" name="email" value="{{Auth::user()->email}}"  id="subscribee_email" placeholder="Email address" aria-label="Email address" aria-describedby="subscribeButton" required
                                    data-msg="Please enter a valid email address.">
                                @if($Subscribe=='0')
                                <div class="input-group-append">
                                        <button type="submit" class="btn btn-dark btn-sm-wide height-40 py-2"  id="subscribee" style="border-bottom-right-radius: 30px;border-top-right-radius: 30px;">Subscribe</button>
                                        <button type="submit" class="btn btn-dark btn-sm-wide height-40 py-2" id="un_subscribee" style="display:none">Unsubscribe</button>
                                    </div>
                                @else
                                <div class="input-group-append">
                                       
                                        <button type="submit" class="btn btn-dark btn-sm-wide height-40 py-2"  id="subscribee" style="display:none;border-bottom-right-radius: 30px;border-top-right-radius: 30px;">Subscribe</button>
                                        <button type="submit" class="btn btn-dark btn-sm-wide height-40 py-2" id="un_subscribee">Unsubscribe</button>
                                        
                                    </div>

                                @endif
                                @endguest
                                
                                </div>
                            </span>
                            <!-- End Subscribe Form -->
                        </div>
                    </div>
                </div>
            </div>




            <!-- End Footer-newsletter -->
            <!-- Footer-bottom-widgets -->
            <div class="pt-8 pb-4 bg-gray-13">
                <div class="container mt-1">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="mb-6">
                                <a href="/" class="d-inline-block">
                                <img src="/logo/SSJ Jewellery Logo PNG.png" style="width:160px">
                                </a>
                            </div>
                            <div class="mb-4">
                                <div class="row no-gutters">
                                    <div class="col-auto">
                                        <i class="ec ec-support text-primary font-size-56"></i>
                                    </div>
                                    <div class="col pl-3">
                                        <div class="font-size-13 font-weight-light">Got questions? Call us 24/7!</div>
                                        <a href="tel//:02532599299" class="font-size-20 text-gray-90">0253 2599299, </a><a href="tel:9108177916999" class="font-size-20 text-gray-90">+91 081779 16999</a>
                                    </div>
                                </div>
                            </div>
                          
                           
                        </div>
                        <div class="col-lg-7">
                            <div class="row">
                              
                                <div class="col-12 col-md mb-4 mb-md-0">
                                <h6 class="mb-3 font-weight-bold">Useful Links</h6>
                                    <!-- List Group -->
                                    <ul class="list-group list-group-flush list-group-borderless mb-0 list-group-transparent">
                                        <li><a class="list-group-item list-group-item-action" href="/">Home</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="/shop">Shop</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="/about">About us</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="/contact">Contact Us</a></li>
                                        </ul>
                                    <!-- End List Group -->
                                </div>

                                <div class="col-12 col-md mb-4 mb-md-0">
                                    <h6 class="mb-3 font-weight-bold">Customer Care</h6>
                                    <!-- List Group -->
                                    <ul class="list-group list-group-flush list-group-borderless mb-0 list-group-transparent">
                                        <li><a class="list-group-item list-group-item-action" href="/trams">Trams & Condition</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="/privacy">Privacy Policy</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="/return">Return & Cancellation Policy</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="/faq">Frequently Asked Questions</a></li>

                                    </ul>
                                    <!-- End List Group -->
                                </div>
                                <div class="col-12 col-md mb-4 mb-md-0">
                                <h6 class="mb-1 font-weight-bold">Contact info</h6>
                                <address class="">
                                Siddharth Shah Jewelers,<br>
1453, Jain Bhavan, Pagadbandh Ln, <br>Saraf Bazar, Nashik, Maharashtra India-422001
                                </address>
                                <ul class="list-inline mb-0 opacity-7">
                                    <li class="list-inline-item mr-0">
                                        <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                            <span class="fab fa-facebook-f btn-icon__inner"></span>
                                        </a>
                                    </li>
                                    <li class="list-inline-item mr-0">
                                        <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                            <span class="fab fa-google btn-icon__inner"></span>
                                        </a>
                                    </li>
                                    <li class="list-inline-item mr-0">
                                        <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                            <span class="fab fa-twitter btn-icon__inner"></span>
                                        </a>
                                    </li>
                                    <li class="list-inline-item mr-0">
                                        <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                            <span class="fab fa-github btn-icon__inner"></span>
                                        </a>
                                    </li>
                                </ul>
                               </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Footer-bottom-widgets -->
            <!-- Footer-copy-right -->
            <div class="bg-gray-14 py-2">
                <div class="container">
                    <div class="flex-center-between d-block d-md-flex">
                        <div class="mb-3 mb-md-0">© <a href="#" class="font-weight-bold text-gray-90">SSJ Jewellery</a> - All rights Reserved</div>
                        <div class="text-md-right">
                        Design and developed By : <a href="https://www.aricinfolink.com/" target="_blank" style="color:black"> Aric Infolink Global Services </a> <br><span><a href="javascriot:void(0);" style="color:black; ">Power By: Quantex</a></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Footer-copy-right -->
        </footer>
        <!-- ========== END FOOTER ========== -->

        <!-- ========== SECONDARY CONTENTS ========== -->
        <!-- Account Sidebar Navigation -->
        <aside id="sidebarContent" class="u-sidebar u-sidebar__lg " aria-labelledby="sidebarNavToggler">
            <div class="u-sidebar__scroller">
                <div class="u-sidebar__container">
                    <div class="js-scrollbar u-header-sidebar__footer-offset pb-3">
                        <!-- Toggle Button -->
                        <div class="d-flex align-items-center pt-4 px-7">
                            <button type="button" class="close ml-auto"
                                aria-controls="sidebarContent"
                                aria-haspopup="true"
                                aria-expanded="false"
                                data-unfold-event="click"
                                data-unfold-hide-on-scroll="false"
                                data-unfold-target="#sidebarContent"
                                data-unfold-type="css-animation"
                                data-unfold-animation-in="fadeInRight"
                                data-unfold-animation-out="fadeOutRight"
                                data-unfold-duration="500">
                                <i class="ec ec-close-remove"></i>
                            </button>
                        </div>
                        <!-- End Toggle Button -->

                        <!-- Content -->
                        <div class="js-scrollbar u-sidebar__body">
                            <div class="u-sidebar__content u-header-sidebar__content">
                                    <!-- Login -->
                                    <div id="login" data-target-group="idForm">
                                        <!-- Title -->
                                        <header class="text-center mb-7">
                                        <h2 class="h4 mb-0">Welcome Back!</h2>
                                        <p>Login to manage your account.</p>
                                        </header>
                                        <!-- End Title -->

                                        <!-- Form Group -->
                                        <form class="js-validate" method="POST" action="{{ route('login') }}">
                                         @csrf


                                        <div class="form-group">
                                            <div class="js-form-message js-focus-state">
                                                <label class="sr-only" for="email">Email</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="signinEmailLabel">
                                                            <span class="fas fa-user"></span>
                                                        </span>
                                                    </div>
                                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="signinEmail" placeholder="Email" aria-label="Email" aria-describedby="signinEmailLabel" value="{{ old('email') }}" required autocomplete="email" autofocus
                                                    data-msg="Please enter a valid email address."
                                                    data-error-class="u-has-error"
                                                    data-success-class="u-has-success">
                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                   @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <input type="hidden" name="role" value="USER">
                                        <!-- End Form Group -->

                                        <!-- Form Group -->
                                        <div class="form-group">
                                            <div class="js-form-message js-focus-state">
                                              <label class="sr-only" for="password">Password</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="signinPasswordLabel">
                                                        <span class="fas fa-lock"></span>
                                                    </span>
                                                </div>
                                                <input type="password" id="password" placeholder="Password" aria-label="Password" aria-describedby="signinPasswordLabel" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password"
                                                   data-msg="Your password is invalid. Please try again."
                                                   data-error-class="u-has-error"
                                                   data-success-class="u-has-success">
                                                    @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                            </div>
                                            </div>               
                                        </div>
                                        @if (Route::has('password.request'))
                                        <a class="d-flex justify-content-end mb-4" href="{{ route('password.request') }}">
                                                        {{ __('Forgot Your Password?') }}
                                                    </a>
                                        @endif

                                       
                                        <div class="mb-2">
                                            <button type="submit" class="btn btn-block btn-sm btn-primary transition-3d-hover">
                                            {{ __('Login') }}
                                            </button>
                                        </div>
                                </form>

                                       <div class="mb-2">
                                            <a href="/login" class="btn btn-block btn-sm btn-secondary transition-3d-hover">
                                            {{ __('Login with Otp') }}
                                            </a>
                                        </div>
                                        <!-- End Form Group -->

                                      
                                        
                                        <div class="text-center mb-4">
                                            <span class="small text-muted">Do not have an account?</span>
                                            <a class="js-animation-link small text-dark" href="javascript:;"
                                               data-target="#signup"
                                               data-link-group="idForm"
                                               data-animation-in="slideInUp">Signup
                                            </a>
                                        </div>

                                        <div class="text-center">
                                            <span class="u-divider u-divider--xs u-divider--text mb-4">OR</span>
                                        </div>

                                        <!-- Login Buttons -->
                                        <div class="d-flex">
                                            <a class="btn btn-block btn-sm btn-soft-facebook transition-3d-hover mr-1" href="#">
                                              <span class="fab fa-facebook-square mr-1"></span>
                                              Facebook
                                            </a>
                                            <a class="btn btn-block btn-sm btn-soft-google transition-3d-hover ml-1 mt-0" href="#">
                                              <span class="fab fa-google mr-1"></span>
                                              Google
                                            </a>
                                        </div>
                                        <!-- End Login Buttons -->
                                    </div>

                                    <!-- Signup -->
                                    <div id="signup" style="display: none; opacity: 0;" data-target-group="idForm">
                                        <!-- Title -->
                                        <header class="text-center mb-7">
                                        <h2 class="h4 mb-0">Welcome to Electro.</h2>
                                        <p>Fill out the form to get started.</p>
                                        </header>
                                        <!-- End Title -->

                                        <!-- Form Group -->
                                        <div class="form-group">
                                            <div class="js-form-message js-focus-state">
                                                <label class="sr-only" for="signupEmail">Email</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="signupEmailLabel">
                                                            <span class="fas fa-user"></span>
                                                        </span>
                                                    </div>
                                                    <input type="email" class="form-control" name="email" id="signupEmail" placeholder="Email" aria-label="Email" aria-describedby="signupEmailLabel" required
                                                    data-msg="Please enter a valid email address."
                                                    data-error-class="u-has-error"
                                                    data-success-class="u-has-success">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Input -->

                                        <!-- Form Group -->
                                        <div class="form-group">
                                            <div class="js-form-message js-focus-state">
                                                <label class="sr-only" for="signupPassword">Password</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="signupPasswordLabel">
                                                            <span class="fas fa-lock"></span>
                                                        </span>
                                                    </div>
                                                    <input type="password" class="form-control" name="password" id="signupPassword" placeholder="Password" aria-label="Password" aria-describedby="signupPasswordLabel" required
                                                    data-msg="Your password is invalid. Please try again."
                                                    data-error-class="u-has-error"
                                                    data-success-class="u-has-success">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Input -->

                                        <!-- Form Group -->
                                        <div class="form-group">
                                            <div class="js-form-message js-focus-state">
                                            <label class="sr-only" for="signupConfirmPassword">Confirm Password</label>
                                                <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="signupConfirmPasswordLabel">
                                                        <span class="fas fa-key"></span>
                                                    </span>
                                                </div>
                                                <input type="password" class="form-control" name="confirmPassword" id="signupConfirmPassword" placeholder="Confirm Password" aria-label="Confirm Password" aria-describedby="signupConfirmPasswordLabel" required
                                                data-msg="Password does not match the confirm password."
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Input -->

                                        <div class="mb-2">
                                            <button type="submit" class="btn btn-block btn-sm btn-primary transition-3d-hover">Get Started</button>
                                        </div>

                                        <div class="text-center mb-4">
                                            <span class="small text-muted">Already have an account?</span>
                                            <a class="js-animation-link small text-dark" href="javascript:;"
                                                data-target="#login"
                                                data-link-group="idForm"
                                                data-animation-in="slideInUp">Login
                                            </a>
                                        </div>

                                        <div class="text-center">
                                            <span class="u-divider u-divider--xs u-divider--text mb-4">OR</span>
                                        </div>

                                        <!-- Login Buttons -->
                                        <div class="d-flex">
                                            <a class="btn btn-block btn-sm btn-soft-facebook transition-3d-hover mr-1" href="#">
                                                <span class="fab fa-facebook-square mr-1"></span>
                                                Facebook
                                            </a>
                                            <a class="btn btn-block btn-sm btn-soft-google transition-3d-hover ml-1 mt-0" href="#">
                                                <span class="fab fa-google mr-1"></span>
                                                Google
                                            </a>
                                        </div>
                                        <!-- End Login Buttons -->
                                    </div>
                                    <!-- End Signup -->

                                    <!-- Forgot Password -->
                                    <div id="forgotPassword" style="display: none; opacity: 0;" data-target-group="idForm">
                                        <!-- Title -->
                                        <header class="text-center mb-7">
                                            <h2 class="h4 mb-0">Recover Password.</h2>
                                            <p>Enter your email address and an email with instructions will be sent to you.</p>
                                        </header>
                                        <!-- End Title -->

                                        <!-- Form Group -->
                                        <div class="form-group">
                                            <div class="js-form-message js-focus-state">
                                                <label class="sr-only" for="recoverEmail">Your email</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="recoverEmailLabel">
                                                            <span class="fas fa-user"></span>
                                                        </span>
                                                    </div>
                                                    <input type="email" class="form-control" name="email" id="recoverEmail" placeholder="Your email" aria-label="Your email" aria-describedby="recoverEmailLabel" required
                                                    data-msg="Please enter a valid email address."
                                                    data-error-class="u-has-error"
                                                    data-success-class="u-has-success">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Form Group -->

                                        <div class="mb-2">
                                            <button type="submit" class="btn btn-block btn-sm btn-primary transition-3d-hover">Recover Password</button>
                                        </div>

                                        <div class="text-center mb-4">
                                            <span class="small text-muted">Remember your password?</span>
                                            <a class="js-animation-link small" href="javascript:;"
                                               data-target="#login"
                                               data-link-group="idForm"
                                               data-animation-in="slideInUp">Login
                                            </a>
                                        </div>
                                    </div>
                                    <!-- End Forgot Password -->
                                </form>
                            </div>
                        </div>
                        <!-- End Content -->
                    </div>
                </div>
            </div>
        </aside>
        <!-- End Account Sidebar Navigation -->
        <!-- ========== END SECONDARY CONTENTS ========== -->

        <!-- Go to Top -->
        <a class="js-go-to u-go-to" href="#"
            data-position='{"bottom": 15, "right": 15 }'
            data-type="fixed"
            data-offset-top="400"
            data-compensation="#header"
            data-show-effect="slideInUp"
            data-hide-effect="slideOutDown">
            <span class="fas fa-arrow-up u-go-to__inner"></span>
        </a>
        <!-- End Go to Top -->

        <!-- JS Global Compulsory -->
        <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
        <script src="assets/vendor/jquery-migrate/dist/jquery-migrate.min.js"></script>
        <script src="assets/vendor/popper.js/dist/umd/popper.min.js"></script>
        <script src="assets/vendor/bootstrap/bootstrap.min.js"></script>

        <!-- JS Implementing Plugins -->
        <script src="assets/vendor/appear.js"></script>
        <script src="assets/vendor/jquery.countdown.min.js"></script>
        <script src="assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
        <script src="assets/vendor/svg-injector/dist/svg-injector.min.js"></script>
        <script src="assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="assets/vendor/fancybox/jquery.fancybox.min.js"></script>
        <script src="assets/vendor/typed.js/lib/typed.min.js"></script>
        <script src="assets/vendor/slick-carousel/slick/slick.js"></script>
        <script src="assets/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

        <!-- JS Electro -->
        <script src="assets/js/hs.core.js"></script>
        <script src="assets/js/components/hs.countdown.js"></script>
        <script src="assets/js/components/hs.header.js"></script>
        <script src="assets/js/components/hs.hamburgers.js"></script>
        <script src="assets/js/components/hs.unfold.js"></script>
        <script src="assets/js/components/hs.focus-state.js"></script>
        <script src="assets/js/components/hs.malihu-scrollbar.js"></script>
        <script src="assets/js/components/hs.validation.js"></script>
        <script src="assets/js/components/hs.fancybox.js"></script>
        <script src="assets/js/components/hs.onscroll-animation.js"></script>
        <script src="assets/js/components/hs.slick-carousel.js"></script>
        <script src="assets/js/components/hs.show-animation.js"></script>
        <script src="assets/js/components/hs.svg-injector.js"></script>
        <script src="assets/js/components/hs.go-to.js"></script>
        <script src="assets/js/components/hs.selectpicker.js"></script>



    <script src="Material-Toast-master\mdtoast.min.js"></script>

        <!-- JS Plugins Init. -->
        <script>
            $(window).on('load', function () {
                // initialization of HSMegaMenu component
                $('.js-mega-menu').HSMegaMenu({
                    event: 'hover',
                    direction: 'horizontal',
                    pageContainer: $('.container'),
                    breakpoint: 767.98,
                    hideTimeOut: 0
                });

                // initialization of svg injector module
                $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
            });

            $(document).on('ready', function () {
                // initialization of header
                $.HSCore.components.HSHeader.init($('#header'));

                // initialization of animation
                $.HSCore.components.HSOnScrollAnimation.init('[data-animation]');

                // initialization of unfold component
                $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
                    afterOpen: function () {
                        $(this).find('input[type="search"]').focus();
                    }
                });

                // initialization of popups
                $.HSCore.components.HSFancyBox.init('.js-fancybox');

                // initialization of countdowns
                var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
                    yearsElSelector: '.js-cd-years',
                    monthsElSelector: '.js-cd-months',
                    daysElSelector: '.js-cd-days',
                    hoursElSelector: '.js-cd-hours',
                    minutesElSelector: '.js-cd-minutes',
                    secondsElSelector: '.js-cd-seconds'
                });

                // initialization of malihu scrollbar
                $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

                // initialization of forms
                $.HSCore.components.HSFocusState.init();

                // initialization of form validation
                $.HSCore.components.HSValidation.init('.js-validate', {
                    rules: {
                        confirmPassword: {
                            equalTo: '#signupPassword'
                        }
                    }
                });

                // initialization of show animations
                $.HSCore.components.HSShowAnimation.init('.js-animation-link');

                // initialization of fancybox
                $.HSCore.components.HSFancyBox.init('.js-fancybox');

                // initialization of slick carousel
                $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

                // initialization of go to
                $.HSCore.components.HSGoTo.init('.js-go-to');

                // initialization of hamburgers
                $.HSCore.components.HSHamburgers.init('#hamburgerTrigger');

                // initialization of unfold component
                $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
                    beforeClose: function () {
                        $('#hamburgerTrigger').removeClass('is-active');
                    },
                    afterClose: function() {
                        $('#headerSidebarList .collapse.show').collapse('hide');
                    }
                });

                $('#headerSidebarList [data-toggle="collapse"]').on('click', function (e) {
                    e.preventDefault();

                    var target = $(this).data('target');

                    if($(this).attr('aria-expanded') === "true") {
                        $(target).collapse('hide');
                    } else {
                        $(target).collapse('show');
                    }
                });

                // initialization of unfold component
                $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

                // initialization of select picker
                $.HSCore.components.HSSelectPicker.init('.js-select');
            });
        </script>
    </body>
</html>




<script>
    $(document).ready(function() {
    wishlist_ajax();
    cart_ajax();
    wallet();
});
  function cart_ajax(){
//   alert();
   var token = $("#_token").val();
$.ajax({

    url:'cart_ajax',

    type:'POST',

    data:{_token:token},
   
  success:function(response)
    {
   //  alert(response);
    
   var response1=response.split('|aubdghbjncartview|');
    
    $("#basicDropdownHover").html(response1[1]);
    $("#cart_value").html(response1[0]);
    $("#cart_value1").html(response1[0]);
   // $("#cart-mobile").html(response1[1]);
       
  
    }

    });
}
function wishlist_ajax(){
//   alert();
   var token = $("#_token").val();
$.ajax({

    url:'wishlist_ajax',

    type:'POST',

    data:{_token:token},
   
  success:function(response)
    {
    // alert(response);
   
        $("#wish_value").html(response);


  
    }

    });  
}

function toggle(id,price_id){
 var token=$("#_token").val();
$.ajax({

 url:'/toggle',

 type:'POST',

 data:{_token:token,product_id:id,price_id:price_id},

success:function(response)
 {

 $('#ffr').html(response);
 

 }

 });
}

    function toggle_wish(id,price_id)
    {
        var token=$("#_token").val();
        $.ajax({
        url:'/toggle_wish',
        type:'POST',
        data:{_token:token,product_id:id,price_id:price_id},
        success:function(response)
        {
        $('#ffr11').html(response);
        }
        });
    }






function cart(id,price_id){
    
    //alert(id);
var user=$("#login_data").val();
if(user!=0){
//alert();
var token = $("#_token").val();
$.ajax({

    url:'addtocart_ajax',

    type:'POST',

    data:{_token:token,product_id:id,product_price_id:price_id},
   
  success:function(response)
    {
       
     //alert(response);
   if(response==2){
      // swal('Already Exits');
      mdtoast('Itam already in cart.', { 
        type: 'info',
        duration: 1000
        });
   }else{

    cart_ajax();

    mdtoast('Add to cart successfully.', { 
        interaction: true, 
        actionText: 'View Cart',
        
        action: function(){
            this.hide(); 
            window.location.href="cart"
        },
 

        });

        }
    }   
    });
    
    
    }else{
    
        mdtoast('Please Login', { 
    type: 'error',
    duration: 3000
    });
    }
    
}
function remove_cart(id,price_id){
    //alert(id);
   
   var token = $("#_token").val();
 //  alert();
$.ajax({

    url:'removetocart_ajax',

    type:'POST',

    data:{_token:token,product_id:id,product_price_id:price_id},
   
  success:function(response)
    {
      
 mdtoast('Remove  to cart sucessfully.', { 
  type: 'info',
  duration: 3000
});
        cart_ajax();
        toggle(id);
        cart_ajax();
    }   
 });
  
  

   
}
function wishlist(id,price_id){
  //  alert(id);
    var user=$("#login_data").val();
   if(user!=0){
//   alert();
   var token = $("#_token").val();
$.ajax({

    url:'addtowishlist_ajax',


    type:'POST',

    data:{_token:token,product_id:id,product_price_id:price_id},
   
  success:function(response)
    {
     //alert(response);
   if(response==2){
    mdtoast('Iteam alredy in Wishlist.', { 
    type: 'info',
    duration: 1000
    });
   }else{
        
        cart_ajax();
        wishlist_ajax();
        
        mdtoast('Add to wishlist sucessfully.', { 
    interaction: true, 
    actionText: 'View Wishlist',
    
    action: function(){
        this.hide(); 
        window.location.href="wishlist"
    },
    

    });
}
}   
 });
  
  
}else{
   
   // swal('Please Login');
    mdtoast('Please Login', { 
  type: 'warning',
  duration: 3000
});
}
}
function remove_wishlist(id,price_id){
  //  alert(id);
   
   var token = $("#_token").val();
 //  alert();
$.ajax({

    url:'removetowishlist_ajax',
    type:'POST',
    data:{_token:token,product_id:id,product_price_id:price_id},
   
  success:function(response)
    {
mdtoast('Remove  to Wishlist sucessfully.', { 
  type: 'info',
  duration: 3000
});
        cart_ajax();
        wishlist_table();
        
        wishlist_ajax();
    }   
 });
  
  

   
}
</script>

<script defer>
$(document).ready(function() {
$('#subscribee').click(function () {

  var token=$("#_token").val();
  var email=$("#subscribee_email").val();
 
$.ajax({
  
 url:'/subscribe',

 type:'POST',

 data:{email:email,_token:token},


success:function(response)
 {

if(response==1)
{
  mdtoast('You are not Registered User', { 
  type: 'warning',
  duration: 3000
});


}

if(response==2)
{
    $('.popup_off').click();
  mdtoast('You are now Prime Member of SSJ Jewellery', { 
  type: 'info',
  duration: 5000
});
}
if(response==3)
{
  
  mdtoast('Please login', { 
  type: 'warning',
  duration: 3000
});
}
   // swal('You are now Prime Member of Auveraa Life Style');
    //location.reload();

    $("#subscribee").css("display", "none");
$("#un_subscribee").css("display", "block");
 }

 })
});

$('#un_subscribee').click(function () {

var token=$("#_token").val();
var email=$("#subscribee_email").val();

$.ajax({

url:'/unsubscribe',

type:'POST',

data:{email:email,_token:token},


success:function(response)
{
  mdtoast('Unsubcribe Sucessfully', { 
  type: 'warning',
  duration: 3000
});
$("#un_subscribee").css("display", "none");
$("#subscribee").css("display", "block");
   // location.reload();

}

})
});
});

function wallet(){
  var token=$("#_token").val();
$.ajax({
 url:'/wallet',
 type:'POST',
 data:{_token:token},
success:function(response)
 {
     
 }
 });
}

</script>

