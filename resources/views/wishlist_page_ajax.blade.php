
                             <div class="table-responsive">
                            <table class="table" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="product-remove">&nbsp;</th>
                                        <th class="product-thumbnail">&nbsp;</th>
                                        <th class="product-name">Product</th>
                                        <th class="product-price">Unit Price</th>
                                        <th class="product-Stock w-lg-15">Stock Status</th>
                                        <th class="product-subtotal min-width-200-md-lg">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                           
                             @php($user_id=Auth::user()->id)
                             @php($for_cart_view=DB::table('wishlists')->where('user_id',$user_id)->count())
                              
                              @if($for_cart_view!=0)
                              
                             
                            @foreach($item as $item)  
                            @php($view_product_id=$item->product_id)
                                                   @php($product=DB::table('products')->join('product_prices','product_prices.product_id','=','products.product_id')->where('product_prices.product_prices_id',$item->product_price_id)->where('products.product_id',$view_product_id)->first())
                                                   @php($product_image=DB::table('product_images')->where('product_id',$view_product_id)->limit(1)->get())
                                                


                                                
                      
                                    <tr>
                                        <td class="text-center">
                                            <a href="javascript:void(0)" onclick="remove_wishlist({{$view_product_id}},{{$item->product_price_id}});" class="text-gray-32 font-size-26">×</a>
                                        </td>
                                        <td class="d-none d-md-table-cell">
                                        @foreach($product_image as $product_image)
                                            <a href="/product_details?id={{$view_product_id}}"><img class="img-fluid max-width-100 p-1 border border-color-1" src="product_image/{{$product_image->image}}" alt="{{$product->product_name}}"></a>
                                       @endforeach
                                        </td>

                                        <td data-title="Product">
                                            <a href="/product_details?id={{$view_product_id}}" class="text-gray-90">{{$product->product_name}}({{$product->size}})</a>
                                        </td>

                                        <td data-title="Unit Price">
                                            <span class="">
                                            @if(Auth::user()->usertype=='MEDIATOR')
                                            &#8377;{{$product->mediator_price}}
                                            @else
                                            &#8377;{{$product->selling_price}}
                                            @endif
                                            </span>
                                        </td>

                                        <td data-title="Stock Status">
                                        @if($product->available_stock>0)
                                                <span>In-stock </span>
                                                @else
                                                <span>Out-stock </span>
                                                @endif
                                            <!-- Stock Status -->
                                            <span>In stock</span>
                                            <!-- End Stock Status -->
                                        </td>

                                        <td>
                                            <button type="button" onclick="wishlisttoaddtocat({{$view_product_id}},{{$item->product_price_id}});" class="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5 w-100 w-md-auto">Add to Cart</button>
                                        </td>
                                    </tr>
                                  



                              
                              
                                           @endforeach
                                           </tbody>
                            </table>
                        </div>
                   

                            @else
                       <tr><td colspan="6">
                           <br>
                                 <center> <h4> No Item In Your Wishlist </h4> </center><br>
                                 </td>
                                 
                                 </tr>
                            @endif

                            <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
<script type="text/javascript">

$(document).ready(function() {

  //  wishlist_table();
});

function wishlisttoaddtocat(id,price_id){
//   alert();
   var token = $("#_token").val();

 
   remove_wishlist(id,price_id);
   cart(id,price_id);
   wishlist_table();
   wishlist_ajax();
}

	
</script>
