@extends('layouts.menu')

@section('title')
Contact Us : SSJ Jewellery
@endsection

@section('content')

<main id="content" role="main">
            <!-- breadcrumb -->
            <div class="bg-gray-13 bg-md-transparent">
                <div class="container">
                    <!-- breadcrumb -->
                    <div class="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Contact Us</li>
                            </ol>
                        </nav>
                    </div>
                    <!-- End breadcrumb -->
                </div>
            </div>
            <!-- End breadcrumb -->
            <div class="mb-8">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.835252972956!2d144.95592398991224!3d-37.817327693787625!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4c2b349649%3A0xb6899234e561db11!2sEnvato!5e0!3m2!1sen!2sin!4v1575470633967!5m2!1sen!2sin" width="100%" height="514" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>

            <div class="container">
                <div class="row mb-10">
                    <div class="col-md-8 col-xl-9">
                        <div class="mr-xl-6">
                            <div class="border-bottom border-color-1 mb-5">
                                <h3 class="section-title mb-0 pb-2 font-size-25">Leave us a Message</h3>
                            </div>
                            <p class="max-width-830-xl text-gray-90">Maecenas dolor elit, semper a sem sed, pulvinar molestie lacus. Aliquam dignissim, elit non mattis ultrices, neque odio ultricies tellus, eu porttitor nisl ipsum eu massa.</p>
                           
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Input -->
                                        <div class="js-form-message mb-4">
                                            <label class="form-label">
                                                Name
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" name="con_name" id="con_name" class="form-control" required="" value="@guest @else {{Auth::user()->name}}@endif">
                                                                                 </div>
                                        <!-- End Input -->
                                    </div>
                                    <div class="col-md-12">
                                    <div class="js-form-message mb-4">
                                            <label class="form-label">
                                                Email
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="email" name="con_email" id="con_email" class="form-control" required="" value="@guest @else {{Auth::user()->email}}@endif">
                                                                                 </div>
                                        <!-- End Input -->
                                    </div>
          

                                    <div class="col-md-12">
                                        <!-- Input -->
                                        <div class="js-form-message mb-4">
                                            <label class="form-label">
                                                Subject
                                            </label>
                                            <input type="text" name="con_subject" id="con_subject" class="form-control">     </div>
                                        <!-- End Input -->
                                    </div>
                                    <div class="col-md-12">
                                        <div class="js-form-message mb-4">
                                            <label class="form-label">
                                                Your Message
                                            </label>

                                            <div class="input-group">
                                            <textarea name="con_message" id="con_message" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <button type="submit" class="btn btn-primary-dark-w px-5"  id="submit">Send Message</button>
                                </div>
                         
                        </div>
                    </div>
                    <div class="col-md-4 col-xl-3">
                        <div class="border-bottom border-color-1 mb-5">
                            <h3 class="section-title mb-0 pb-2 font-size-25">Our Store</h3>
                        </div>
                        <div class="mr-xl-6">
                            <address class="mb-6">
                            Siddharth Shah Jewelers,<br>
1453, Jain Bhavan, Pagadbandh Ln, <br>Saraf Bazar, Nashik, Maharashtra India-422001
                            </address>
                           
                           
                            <h5 class="font-size-14 font-weight-bold mb-3">Phone</h5>
                            <p class="text-gray-90">If you’re face any problem, please call us: <a class="text-blue text-decoration-on" href="tel://0253 2599299">0253 2599299</a><br><a class="text-blue text-decoration-on" href="tel://9108177916999">+91 081779 16999</a></p>
                            <h5 class="font-size-14 font-weight-bold mb-3">Emails</h5>
                            <p class="text-gray-90">If you’re face any problem, please mail us: <a class="text-blue text-decoration-on" href="mailto:support@ssj.com">support@ssjewelers.com</a><br><a class="text-blue text-decoration-on" href="mailto:info@ssj.com">info@ssjewelers.com</a></p>
                        
                        </div>
                    </div>
                </div>
                <!-- Brand Carousel -->
                <div class="mb-8">
                   
                </div>
                <!-- End Brand Carousel -->
            </div>
        </main>

        <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
<script>
 $(document).ready(function() {
   
    $('#submit').click(function () {
       
       var token=$("#_token").val();
       var name=$("#con_name").val();
       var email=$("#con_email").val();
       var subject=$("#con_subject").val();
       var message=$("#con_message").val();
        if(name.length==0){
            mdtoast('Please Enter Your Name', { 
                        type: 'warning',
                        duration: 3000
                        });
     
        }else if(email.length==0){
            mdtoast('Please Enter Your email', { 
                        type: 'warning',
                        duration: 3000
                        });
        }else if(subject.length==0){
            mdtoast('Please Enter Subject', { 
                        type: 'warning',
                        duration: 3000
                        });
        }else if(message.length==0){
            mdtoast('Please Enter Message', { 
                        type: 'warning',
                        duration: 3000
                        });
        }else{
            $.ajax({

url:'/contactmail',

type:'POST',

data:{name:name,email:email,subject:subject,message:message,_token:token},


success:function(response)
{
    if(response==1){
        mdtoast('Mail send successfully', { 
        type: 'success',
        duration: 3000
        });

    }else{
        mdtoast('Mail cannot send. Please try again later', { 
        type: 'error',
        duration: 3000
        });
    }
    
}

})
        }



      




    });
});
</script>
        @endsection