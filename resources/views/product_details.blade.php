@extends('layouts.menu')

@section('title')
{{$product_details->product_name}} : SSJ Jewellery
@endsection

@section('content')
<style>
p{
    margin: 0px;
}
</style>

   <!-- ========== MAIN CONTENT ========== -->
   <main id="content" role="main">
            <!-- breadcrumb -->
            <div class="bg-gray-13 bg-md-transparent">
                <div class="container">
                    <!-- breadcrumb -->
                    <div class="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                                @php($cat1=DB::table('cats')->where('cat_id',$product_details->cat_id)->count())
                               @php($sub_cats1=DB::table('sub_cats')->where('sub_cat_id',$product_details->sub_cat_id)->count())
                               @php($sub_sub_cats1=DB::table('sub_sub_cats')->where('sub_sub_cat_id',$product_details->sub_sub_cat_id)->count())
                                @if($cat1!=0)
                                    @php($cat=DB::table('cats')->where('cat_id',$product_details->cat_id)->first())
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a  href="/shop?cat_id={{$product_details->cat_id}}">{{$cat->cat_name}}</a></li>
                                    
                                        @if($sub_cats1!=0)
                                        @php($sub_cats=DB::table('sub_cats')->where('sub_cat_id',$product_details->sub_cat_id)->first())
                                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a  href="/shop?cat_id={{$cat->cat_id}}&sub_cat_id={{$sub_cats->sub_cat_id}}">{{$sub_cats->sub_cat_name}}</a></li>
     
                                            @if($sub_sub_cats1!=0)
                                            @php($sub_sub_cats=DB::table('sub_sub_cats')->where('sub_sub_cat_id',$product_details->sub_sub_cat_id)->first())
                                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a  href="/shop?cat_id={{$cat->cat_id}}&sub_cat_id={{$sub_cats->sub_cat_id}}&sub_sub_cat_id={{$sub_sub_cats->sub_sub_cat_id}}">{{$sub_sub_cats->sub_sub_cat_name}}</a></li>
     
                                          @endif
                                        @endif
                                    @endif


                        
                                     <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">{{$product_details->product_name}}</li>
                            </ol>
                        </nav>
                    </div>
                    <!-- End breadcrumb -->
                </div>
            </div>
            <!-- End breadcrumb -->

            <div class="container">
            <input type="hidden" id="_token" value="<?php echo csrf_token();?>">
            @auth
            <input type="hidden" id="user_id" value="{{ Auth::user()->name }}">
            @else
            <input type="hidden" id="user_id" value="0">
            @endauth
            <input type="hidden" id="product_id" value="{{$product_details->product_id}}">
                <!-- Single Product Body -->
                <div class="mb-14">
                    <div class="row">
                        <div class="col-md-6 col-lg-4 col-xl-5 mb-4 mb-md-0">
                            <div id="sliderSyncingNav" class="js-slick-carousel u-slick mb-2"
                                data-infinite="true"
                                data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-classic u-slick__arrow-centered--y rounded-circle"
                                data-arrow-left-classes="fas fa-arrow-left u-slick__arrow-classic-inner u-slick__arrow-classic-inner--left ml-lg-2 ml-xl-4"
                                data-arrow-right-classes="fas fa-arrow-right u-slick__arrow-classic-inner u-slick__arrow-classic-inner--right mr-lg-2 mr-xl-4"
                                data-nav-for="#sliderSyncingThumb">


                                @foreach($product_image as $product_image1)
                                <div class="js-slide">
                                    <img class="img-fluid" src="product_image/{{$product_image1->image}}" alt="Image Description">
                                </div>
                               @endforeach
                                 
                               
                               
                            </div>

                            <div id="sliderSyncingThumb" class="js-slick-carousel u-slick u-slick--slider-syncing u-slick--slider-syncing-size u-slick--gutters-1 u-slick--transform-off"
                                data-infinite="true"
                                data-slides-show="5"
                                data-is-thumbs="true"
                                data-nav-for="#sliderSyncingNav">
                               
                               
                               
                                @foreach($product_image as $product_image1)
                                <div class="js-slide" style="cursor: pointer;">
                                    <img class="img-fluid" src="product_image/{{$product_image1->image}}" alt="Image Description">
                                </div>
                                @endforeach




                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-4 mb-md-6 mb-lg-0">
                            <div class="mb-2">
                               
                                <h2 class="font-size-25 text-lh-1dot2">{{$product_details->product_name}}</h2>
                               
                                <div class="mb-2">
                                    <a class="d-inline-flex align-items-center small font-size-15 text-lh-1" href="#">
                                        <div class="text-warning mr-2">
                                        @php($a=(int)$product_details->review)
                                                    @for($i=0;$i<$a;$i++)
                                                    <small class="fas fa-star"></small>
                                                    @endfor
                                                 

                                                     @for($i=0;$i<5-$a;$i++)
                                                     <small class="far fa-star text-muted"></small>
                                                     @endfor
                                                    
                                            
                                        </div>
                                        <span class="text-secondary font-size-13">({{$product_details->total_review}} customer reviews)</span>
                                    </a>
                                </div>
                                <a href="#" class="font-size-12 text-gray-5 mb-2 d-inline-block">Brand: {{$product_details->brand_name}}</a><br>
                                <a href="#" class="d-inline-block max-width-150 ml-n2 mb-2"><img class="img-fluid" src="brand_logo/{{$product_details->brand_image}}" style="width:80px;height:35px" alt="Image Description"></a>
                                <br>
<h6 class="d-inline-block ml-n2 mb-2" style="color:#333e48"><b>Features</b></h6>
                                <div class="mb-2">
                                    <ul class="font-size-14 pl-3 ml-1 text-gray-110">
                                        @foreach($product_feature as $key=> $product_feature1)
                                            
                                        <li>{{$product_feature1->features}}</li>
                                            @if($key == 4)
                                               @break
                                            @endif 

                                        @endforeach

                                       
                                    </ul>
                                </div>
                                @foreach($product_specification as $key1=> $product_specification111)
                                        <li><b>{{$product_specification111->title}}</b> : {{$product_specification111->description}}</li>
                                            @if($key1 == 2)
                                               @break
                                            @endif 

                                        @endforeach
                                        <div class="" style="margin-left:7px;margin-top:18px"><h6 class="d-inline-block ml-n2 mb-2" style="color:#333e48"><b>Delivery Availability</b></h6> </div>
                                        <div class="input-group input-group-pill max-width-660-xl" style="width: 69%;">
                                   
                                            <input class="form-control" type="text" id="pin" name="coupon_code" placeholder="Enter Your Pincode">
                                            <div class="input-group-append">
                                                <a type="submit" class="btn btn-block btn-dark font-weight-normal btn-pill px-4" id="pincheck">
                                                
                                                    <i class="fas fa-tags d-md-none"></i>
                                                    <span class="d-none d-md-inline" style="color:white" onclick="coupon()"  >Check</span>
                                                </a>
                                            </div>
                                        </div>
                                        <div style="padding-top:15px">
                                <span style="font-size: 16px;"><b>HSN Code:</b> {{$product_details->hsn_code}}</span>
                               <br> <span style="font-size: 16px;"><b>Product Code : </b> SSJ-{{sprintf("%04d", $product_details->product_code)}}</span>
                               
                               </div>
                               @php($cat1=DB::table('cats')->where('cat_id',$product_details->cat_id)->count())
                               @php($sub_cats1=DB::table('sub_cats')->where('sub_cat_id',$product_details->sub_cat_id)->count())
                               @php($sub_sub_cats1=DB::table('sub_sub_cats')->where('sub_sub_cat_id',$product_details->sub_sub_cat_id)->count())
                               
                                <div class="hiraola-tag-line" style="padding-top:0px">
                                    <span style="font-size:16px">Tags:</span>
                                    @if($cat1!=0)
                                    @php($cat=DB::table('cats')->where('cat_id',$product_details->cat_id)->first())
                                        <a style="font-size:16px" class="text-gray-6 font-size-13" href="/shop?cat_id={{$product_details->cat_id}}">{{$cat->cat_name}}</a>,
                                        @if($sub_cats1!=0)
                                        @php($sub_cats=DB::table('sub_cats')->where('sub_cat_id',$product_details->sub_cat_id)->first())
                                        <a style="font-size:16px" class="text-gray-6 font-size-13" href="/shop?cat_id={{$cat->cat_id}}&sub_cat_id={{$sub_cats->sub_cat_id}}">{{$sub_cats->sub_cat_name}}</a>,
                                            @if($sub_sub_cats1!=0)
                                            @php($sub_sub_cats=DB::table('sub_sub_cats')->where('sub_sub_cat_id',$product_details->sub_sub_cat_id)->first())
                                                <a style="font-size:16px" class="text-gray-6 font-size-13" href="/shop?cat_id={{$cat->cat_id}}&sub_cat_id={{$sub_cats->sub_cat_id}}&sub_sub_cat_id={{$sub_sub_cats->sub_sub_cat_id}}">{{$sub_sub_cats->sub_sub_cat_name}}</a>
                                            @endif
                                        @endif
                                    @endif
                                </div>
                            </div>



                        </div>
                        <div class="mx-md-auto mx-lg-0 col-md-6 col-lg-4 col-xl-3">
                            <div class="mb-2">
                                <div class="card p-5 border-width-2 border-color-1 borders-radius-17">
                                  
                                    <!-- <div class="mb-3">
                                        <div class="font-size-36">₹ 685.00</div>

                                        <h4><del style="color:red">₹ 685.00 </del></h4>
                                    </div> -->

                                    <div id="price"></div>

                                    
                                    <div class="mb-3">
                                        <h6 class="font-size-14">Quantity</h6>
                                        <!-- Quantity -->
                                        <div class="border rounded-pill py-1 w-md-60 height-35 px-3 border-color-1">
                                            <div class="js-quantity row align-items-center">
                                                <div class="col">
                                                    <input class="js-result form-control h-auto border-0 rounded p-0 shadow-none gst1" id="quantity" type="text" value="1">
                                                </div>
                                                <div class="col-auto pr-1">
                                                    <a class="js-minus btn btn-icon btn-xs btn-outline-secondary rounded-circle border-0" href="javascript:;" onClick="down();" >
                                                        <small class="fas fa-minus btn-icon__inner" ></small>
                                                    </a>
                                                    <a class="js-plus btn btn-icon btn-xs btn-outline-secondary rounded-circle border-0" href="javascript:;" onClick="up();">
                                                        <small class="fas fa-plus btn-icon__inner"  ></small>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Quantity -->
                                    </div>
                                    <div class="mb-3">
                                        <h6 class="font-size-14">Product Size</h6>
                                        <!-- Select -->
                                     
                                        <select class="js-select selectpicker dropdown-select btn-block col-12 px-0"
                                            data-style="btn-sm bg-white font-weight-normal py-2 border" onChange="price();" id="price_id" name="price_id">
                                         
                                           
                                         
                                           @foreach($product_price as $key=> $product_price1)
                                           @php($price_id=$product_price1->product_prices_id)
                                        

                                            <option value="{{$price_id}}" @if($key==0) selected  @endif  >{{$product_price1->size}}</option>
                                          
                                           @endforeach

                                       
                                        </select>
                                        <!-- End Select -->
                                    </div>
                                    <!-- <div class="mb-2 pb-0dot5">
                                        <a href="javascript:void(0);" class="btn btn-block btn-primary-dark" onClick="add_to_cart();"><i class="ec ec-add-to-cart mr-2 font-size-20"></i> Add to Cart</a>
                                    </div> -->

                                    <div id="ffr"></div>
                                    
                                    <div class="mb-3">
                                    @guest
                                    
                                        <a href="/login" class="btn btn-block btn-dark">Buy Now</a>
                                        @else
                                        <a href="javascript:void(0)" class="btn btn-block btn-dark" onClick="buy_now();">Buy Now</a>
                                    @endauth
                                    </div>
                                     <!-- <div class="flex-content-center flex-wrap">
                                                           
                                        <a href="#" class="btn btn-block btn-primary-dark" onClick="add_to_wishlist();"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                                     </div>  -->

                                     <div id="ffr11"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Single Product Body -->
            </div>
            <div class="bg-gray-7 pt-6 pb-3 mb-6">
                <div class="container">
                    <div class="js-scroll-nav">
                        
                        <div class="bg-white pt-4 pb-6 px-xl-11 px-md-5 px-4 mb-6 overflow-hidden">
                            <div id="Description" class="mx-md-2">
                                <div class="position-relative mb-6">
                                    <ul class="nav nav-classic nav-tab nav-tab-lg justify-content-xl-center mb-6 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble border-lg-down-bottom-0 pb-1 pb-xl-0 mb-n1 mb-xl-0">
                                        
                                        <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                                            <a class="nav-link active" href="#Description">
                                                <div class="d-md-flex justify-content-md-center align-items-md-center">
                                                    Description
                                                </div>
                                            </a>
                                        </li>
                                        <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                                            <a class="nav-link" href="#Specification">
                                                <div class="d-md-flex justify-content-md-center align-items-md-center">
                                                    Specification
                                                </div>
                                            </a>
                                        </li>
                                        <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                                            <a class="nav-link" href="#Reviews">
                                                <div class="d-md-flex justify-content-md-center align-items-md-center">
                                                    Reviews
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="mx-md-4 pt-1">
                                    <h3 class="font-size-24 mb-3">Description</h3>
                                                                     
                                     <?php  echo $product_details->description; ?>

                                </div>
                            </div>
                        </div>
                        <div class="bg-white py-4 px-xl-11 px-md-5 px-4 mb-6">
                            <div id="Specification" class="mx-md-2">
                                <div class="position-relative mb-6">
                                    <ul class="nav nav-classic nav-tab nav-tab-lg justify-content-xl-center mb-6 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble border-lg-down-bottom-0 pb-1 pb-xl-0 mb-n1 mb-xl-0">
                                       
                                        <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                                            <a class="nav-link" href="#Description">
                                                <div class="d-md-flex justify-content-md-center align-items-md-center">
                                                    Description
                                                </div>
                                            </a>
                                        </li>
                                        <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                                            <a class="nav-link active" href="#Specification">
                                                <div class="d-md-flex justify-content-md-center align-items-md-center">
                                                    Specification
                                                </div>
                                            </a>
                                        </li>
                                        <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                                            <a class="nav-link" href="#Reviews">
                                                <div class="d-md-flex justify-content-md-center align-items-md-center">
                                                    Reviews
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="mx-md-5 pt-1">
                                    <!-- <div class="table-responsive mb-4">
                                        <table class="table table-hover">
                                            <tbody>
                                                <tr>
                                                    <th class="px-4 px-xl-5 border-top-0">Weight</th>
                                                    <td class="border-top-0">7.25kg</td>
                                                </tr>
                                                <tr>
                                                    <th class="px-4 px-xl-5">Dimensions</th>
                                                    <td>90 x 60 x 90 cm</td>
                                                </tr>
                                                <tr>
                                                    <th class="px-4 px-xl-5">Size</th>
                                                    <td>One Size Fits all</td>
                                                </tr>
                                                <tr>
                                                    <th class="px-4 px-xl-5">color</th>
                                                    <td>Black with Red, White with Gold</td>
                                                </tr>
                                                <tr>
                                                    <th class="px-4 px-xl-5">Guarantee</th>
                                                    <td>5 years</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div> -->
                                    <h3 class="font-size-18 mb-4">Specification</h3>
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <tbody>
                                           @foreach($product_specification as $product_specification1)
                                                <tr>
                                                    <th class="px-4 px-xl-5 border-top-0">{{$product_specification1->title}}</th>
                                                    <td class="border-top-0">{{$product_specification1->description}}</td>
                                                </tr>
                                             @endforeach

                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bg-white py-4 px-xl-11 px-md-5 px-4 mb-6">
                            <div id="Reviews" class="mx-md-2">
                                <div class="position-relative mb-6">
                                    <ul class="nav nav-classic nav-tab nav-tab-lg justify-content-xl-center mb-6 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble border-lg-down-bottom-0 pb-1 pb-xl-0 mb-n1 mb-xl-0">
                                       
                                        <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                                            <a class="nav-link" href="#Description">
                                                <div class="d-md-flex justify-content-md-center align-items-md-center">
                                                    Description
                                                </div>
                                            </a>
                                        </li>
                                        <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                                            <a class="nav-link" href="#Specification">
                                                <div class="d-md-flex justify-content-md-center align-items-md-center">
                                                    Specification
                                                </div>
                                            </a>
                                        </li>
                                        <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                                            <a class="nav-link active" href="#Reviews">
                                                <div class="d-md-flex justify-content-md-center align-items-md-center">
                                                    Reviews
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="mb-4 px-lg-4">
                                    <div class="row mb-8">
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <h3 class="font-size-18 mb-6">Based on {{$product_details->total_review}} reviews</h3>
                                                <h2 class="font-size-30 font-weight-bold text-lh-1 mb-0">{{$product_details->review}}</h2>
                                                <div class="text-lh-1">overall</div>
                                            </div>
                                            
                                         @if($product_details->total_review !=0) 
                                            @php($p5=round(((($review_star5)/$product_details->total_review)*100),2))
                                            @php($p4=round(((($review_star4)/$product_details->total_review)*100),2))
                                            @php($p3=round(((($review_star3)/$product_details->total_review)*100),2))
                                            @php($p2=round(((($review_star2)/$product_details->total_review)*100),2))
                                            @php($p1=round(((($review_star1)/$product_details->total_review)*100),2))
                                     @else
                                           @php($p5=0)
                                            @php($p4=0)
                                            @php($p3=0)
                                            @php($p2=0)
                                            @php($p1=0)
                                     
                                        @endif

                                            <!-- Ratings -->
                                            <ul class="list-unstyled">
                                                <li class="py-1">
                                                    <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                                        <div class="col-auto mb-2 mb-md-0">
                                                            <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                                <small class="fas fa-star"></small>
                                                                <small class="fas fa-star"></small>
                                                                <small class="fas fa-star"></small>
                                                                <small class="fas fa-star"></small>
                                                                <small class="fas fa-star"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto mb-2 mb-md-0">
                                                            <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                                                <div class="progress-bar" role="progressbar" style="width: {{$p5}}%;" aria-valuenow="" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto text-right">
                                                            <span class="text-gray-90">{{$p5}} %</span>
                                                        </div>
                                                    </a>
                                                </li>
                                                
                                                <li class="py-1">
                                                    <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                                        <div class="col-auto mb-2 mb-md-0">
                                                            <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                                <small class="fas fa-star"></small>
                                                                <small class="fas fa-star"></small>
                                                                <small class="fas fa-star"></small>
                                                                <small class="fas fa-star"></small>
                                                                <small class="far fa-star text-muted"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto mb-2 mb-md-0">
                                                            <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                                                <div class="progress-bar" role="progressbar" style="width: {{$p4}}%;" aria-valuenow="53" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto text-right">
                                                            <span class="text-gray-90">{{$p4}} %</span>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li class="py-1">
                                                    <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                                        <div class="col-auto mb-2 mb-md-0">
                                                            <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                                <small class="fas fa-star"></small>
                                                                <small class="fas fa-star"></small>
                                                                <small class="fas fa-star"></small>
                                                                <small class="far fa-star text-muted"></small>
                                                                <small class="far fa-star text-muted"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto mb-2 mb-md-0">
                                                            <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                                                <div class="progress-bar" role="progressbar" style="width: {{$p3}}%;" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto text-right">
                                                            <span class="text-gray-90">{{$p3}} %</span>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li class="py-1">
                                                    <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                                        <div class="col-auto mb-2 mb-md-0">
                                                            <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                                <small class="fas fa-star"></small>
                                                                <small class="fas fa-star"></small>
                                                                <small class="far fa-star text-muted"></small>
                                                                <small class="far fa-star text-muted"></small>
                                                                <small class="far fa-star text-muted"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto mb-2 mb-md-0">
                                                            <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                                                <div class="progress-bar" role="progressbar" style="width: {{$p2}}%;" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto text-right">
                                                            <span class="text-gray-90">{{$p2}} %</span>
                                                        </div>
                                                    </a>
                                                </li>
                                         
                                                <li class="py-1">
                                                    <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                                        <div class="col-auto mb-2 mb-md-0">
                                                            <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                                <small class="fas fa-star"></small>
                                                                <small class="far fa-star text-muted"></small>
                                                                <small class="far fa-star text-muted"></small>
                                                                <small class="far fa-star text-muted"></small>
                                                                <small class="far fa-star text-muted"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto mb-2 mb-md-0">
                                                            <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                                                <div class="progress-bar" role="progressbar" style="width: {{$p1}}%;" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto text-right">
                                                            <span class="text-gray-90">{{$p1}} %</span>
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                            <!-- End Ratings -->
                                        </div>
                                        <div class="col-md-6">
                                            <h3 class="font-size-18 mb-5">Add a review</h3>
                                            <!-- Form -->
                                            <form class="js-validate">
                                                <div class="row align-items-center mb-4">
                                                    <div class="col-md-4 col-lg-3">
                                                        <label for="rating" class="form-label mb-0">Your Review</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-9">
                                                        <a href="javascript:void(0)" class="d-block">
                                                            <div class="text-warning text-ls-n2 font-size-16">


                                                               

                                                                  <div id="rev"></div>

                                                                  @if($review_update1!=0)
                                           <input type="hidden" id="dr" value=" {{$review_update->review}} ">
                                           @else
                                           <input type="hidden" id="dr" value="0">
                                           @endif
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="js-form-message form-group mb-3 row">
                                                    <div class="col-md-4 col-lg-3">
                                                        <label for="emailAddress" class="form-label">Review Title<span class="text-danger">*</span></label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-9">
                                                        <input type="text" class="form-control" name="title" id="title" aria-label="alexhecker@pixeel.com" required value="@if($review_update1!=0) {{$review_update->review_title}} @endif"
                                                       >
                                                    </div>
                                                </div>
                                                <div class="js-form-message form-group mb-3 row">
                                                    <div class="col-md-4 col-lg-3">
                                                        <label for="descriptionTextarea" class="form-label">Comments</label>
                                                    </div>
                                                    <div class="col-md-8 col-lg-9">
                                                        <textarea class="form-control" rows="3" id="comments" name="comments"
                                                        data-msg="Please enter your message."
                                                        data-error-class="u-has-error"
                                                        data-success-class="u-has-success">@if($review_update1!=0) {{$review_update->review_details}} @endif</textarea>
                                                    </div>
                                                </div>
                                               
                                                @auth
                                             @if($review_submit>0)   
                                                <div class="row">
                                                    <div class="offset-md-4 offset-lg-3 col-auto">
                                                        <a href="javascript:void(0);" type="submit" onClick="send();" class="btn btn-primary-dark btn-wide transition-3d-hover">Add Review</a>
                                                    </div>
                                                </div>
                                             @endif
                                                @endauth

                                            </form>
                                            <!-- End Form -->
                                        </div>
                                    </div>

                                    
                                    <!-- Review -->
                                    @foreach($revie as $revie1)
                                    <div class="border-bottom border-color-1 pb-4 mb-4">
                                        <!-- Review Rating -->
                                        <div class="d-flex justify-content-between align-items-center text-secondary font-size-1 mb-2">
                                            <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">

                                            @if( $revie1->review==5)
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>

                                            @elseif($revie1->review==4)
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="far fa-star text-muted"></small>

                                            @elseif($revie1->review==3)
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="far fa-star text-muted"></small>
                                                <small class="far fa-star text-muted"></small>
                                            @elseif($revie1->review==2)
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="far fa-star text-muted"></small>
                                                <small class="far fa-star text-muted"></small>
                                                <small class="far fa-star text-muted"></small>
                                            @elseif($revie1->review==1)
                                                <small class="fas fa-star"></small>
                                                <small class="far fa-star text-muted"></small>
                                                <small class="far fa-star text-muted"></small>
                                                <small class="far fa-star text-muted"></small>
                                                <small class="far fa-star text-muted"></small>
                                                @endif


                                            </div>
                                        </div>
                                        <!-- End Review Rating -->

                                        <p class="text-gray-90">{{$revie1->review_details}}</p>

                                        <!-- Reviewer -->
                                      @php($dde=DB::table('users')->where('id',$revie1->user_id)->first())

                                        <div class="mb-2">
                                            <strong>{{$dde->name}}</strong>
                                            <span class="font-size-13 text-gray-23">- {{date('d/m/Y', strtotime($revie1->updated_at))}}</span>
                                        </div>
                                        <!-- End Reviewer -->
                                    </div>
                                    @endforeach
                                    <!-- End Review -->
                                  
                                   
                                </div>
                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>
            <div class="container">
                <!-- Related products -->
                <div class="mb-6">
                    <div class="d-flex justify-content-between align-items-center border-bottom border-color-1 flex-lg-nowrap flex-wrap mb-4">
                        <h3 class="section-title mb-0 pb-2 font-size-22">Related products</h3>
                    </div>
                    <ul class="row list-unstyled products-group no-gutters">
                     

                    @foreach($related_product_details as $related_product_details1) 
                    @php($product_id=$related_product_details1->product_id)
                    @php($image=DB::table('product_images')->where('product_id',$product_id)->limit(1)->get())

                    @php($product_prices=DB::table('product_prices')->where('product_id',$product_id)->limit(1)->get())
                        <li class="col-6 col-md-3 col-xl-2gdot4-only col-wd-2 product-item">
                            <div class="product-item__outer h-100">
                                <div class="product-item__inner px-xl-4 p-3">
                                    <div class="product-item__body pb-xl-2">
                                        <div class="mb-2"><a href="/product_details?id={{$product_id}}" class="font-size-12 text-gray-5">Speakers</a></div>
                                        <h5 class="mb-1 product-item__title"><a href="/product_details?id={{$product_id}}" class="text-blue font-weight-bold">{{$related_product_details1->product_name}}</a></h5>
                                        <div class="mb-2">
                                        @foreach($image as $image1)
                                            <a href="/product_details?id={{$product_id}}" class="d-block text-center"><img class="img-fluid" src="product_image/{{$image1->image}}" alt="Image Description"></a>
                                       @endforeach
                                        </div>
                                        <div class="flex-center-between mb-1">
                                            <div class="prodcut-price"> 
                                             @foreach($product_prices as $product_prices1)
                                            @guest
                                             

                                                <div class="text-gray-100">₹ {{$product_prices1->selling_price}} <br><del style="color:red; font-size:20px">₹{{$product_prices1->mrp}}</del></div>
                                               
                                              
                                             @else

                                             @foreach($product_prices as $product_prices1)
                                            @if(Auth::user()->usertype=='MEDIATOR')
                                            <div class="text-gray-100">₹ {{$product_prices1->mediator_price}} <br><del style="color:red;font-size:20px">₹{{$product_prices1->mrp}}</del></div>
                                               @else
                                             
                                               <div class="text-gray-100">₹ {{$product_prices1->selling_price}} <br><del style="color:red;font-size:20px">₹{{$product_prices1->mrp}}</del></div>
                                               @endif
                                            @endforeach
                                           

                                             @endguest

                                            </div>
                                            <div class="d-none d-xl-block prodcut-add-cart">
                                            <div class="text-warning mr-2">
                                            <small class="fas fa-star"></small>
                                            <small class="fas fa-star"></small>
                                            <small class="fas fa-star"></small>
                                            <small class="fas fa-star"></small>
                                            <small class="far fa-star text-muted"></small>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-item__footer">
                                        <div class="border-top pt-2 flex-center-between flex-wrap">
                                        @guest
                                           <a href="/login" class="text-gray-6 font-size-13" ><i class="ec ec-add-to-cart"></i> Cart</a>
                                            <a href="/login" class="text-gray-6 font-size-13" ><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                                       @else
                                            <a href="javascript:void(0);" class="text-gray-6 font-size-13" onclick="cart({{$product_id}},{{$product_prices1->product_prices_id}})"><i class="ec ec-add-to-cart"></i> Cart</a>
                                            <a href="javascript:void(0);" class="text-gray-6 font-size-13" onclick="wishlist({{$product_id}},{{$product_prices1->product_prices_id}})"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                                   
                                       @endguest
                                       @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                     @endforeach   
                    


                    </ul>
                </div>
                <!-- End Related products -->
                <!-- Brand Carousel -->
                <div class="mb-8">
                    <div class="py-2 border-top border-bottom">
                        <div class="js-slick-carousel u-slick my-1"
                            data-slides-show="5"
                            data-slides-scroll="1"
                            data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-normal u-slick__arrow-centered--y"
                            data-arrow-left-classes="fa fa-angle-left u-slick__arrow-classic-inner--left z-index-9"
                            data-arrow-right-classes="fa fa-angle-right u-slick__arrow-classic-inner--right"
                            data-responsive='[{
                                "breakpoint": 992,
                                "settings": {
                                    "slidesToShow": 2
                                }
                            }, {
                                "breakpoint": 768,
                                "settings": {
                                    "slidesToShow": 1
                                }
                            }, {
                                "breakpoint": 554,
                                "settings": {
                                    "slidesToShow": 1
                                }
                            }]'>
                           

                           @php($brands=DB::table('brands')
                            ->get())
@foreach($brands as $brands)
                            <div class="js-slide">
                                <a href="#" class="link-hover__brand">
                                    <img class="img-fluid m-auto max-height-50" src="brand_logo/{{$brands->brand_image}}" title="{{$brands->brand_name}}" alt="{{$brands->brand_name}}">
                                </a>
                            </div>

         @endforeach         
                        </div>
                    </div>
                </div>
                <!-- End Brand Carousel -->
            </div>

        </main>
        <!-- ========== END MAIN CONTENT ========== -->
        <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
        </script>

                
        <script>
        $(document).ready(function(){
            var dr=$("#dr").val();
           
           if(dr!=0)
           {
           revi(dr);
        }
         else{
            revi(0);
        }
        
            var product_id=$("#product_id").val();
            var price_id=$("#price_id").val();
           
            price();
            toggle(product_id,price_id);
            toggle_wish(product_id,price_id);
          

            $("#pincheck").click(function(){

var pin=$("#pin").val();
var token=$("#_token").val();
var product_id=$("#product_id").val();

if(pin.length==6){
    
    $.ajax({
        url:'pinckeck',
        type:'POST',
        data:{pin:pin,_token:token,product_id:product_id},
        success:function(response)
        {
            if(response==1){
                swal("Delivary Unavailable!", "Please try another pincode", "warning");
            //  $("#pinstatus").css("color", "red").css("weight", "bold").html("Not available in this location");
            }else if(response==2){
                swal("Aww yiss!", "Delivary available in your pincode!", "success");
            //  $("#pinstatus").css("color", "green").css("weight", "bold").html("Available in this location");
            }
        }
    });
}else{
// swal('Not a valid Pincode');
    swal("Invalid Pincode!", "Please check your pincode!", "error");
// $("#pinstatus").css("color", "red","weight", "bold").html("Not a valid Pincode");
}
});







        });
        </script>
        <script>
        function  up()
        {
                var value1=$("#quantity").val();
                var value2=Number(value1)+1;
                $('.gst1').val(value2);        
        }

        function down()
        {
        // alert();
            var value1=$("#quantity").val();
            var value2=Number(value1)-1;
            if(value2<=1)
            {
            $('.gst1').val(1);
            }
            else{
            $('.gst1').val(value2);

            }
        }

        function price()
        {
            var product_id = $("#product_id").val();  
            var price_id = $("#price_id").val();  
            var token=$("#_token").val();
            $.ajax({
                url:"price",
                type:"POST",
                data:{price_id:price_id,_token:token}
                }).done(function(resp){	
                    toggle(product_id,price_id);
                    toggle_wish(product_id,price_id); 
                     $("#price").html(resp)
                });

        }

     

        function  add_to_cart()
        {
            var quantity=$("#quantity").val();
            var user_id=$("#user_id").val();
            var product_id=$("#product_id").val();
            var product_price_id=$("#price_id").val();
            var token=$("#_token").val();
            $.ajax({
            url:'/add_to_cart_ajax1',
            type:'POST',
            data:{_token:$("#_token").val(),product_id:product_id,product_price_id:product_price_id,quantity:quantity},
            success:function(response)
            {
                if(response==2)
                {
                    swal("Item Already Exit In The Cart!");
                }
                else 
                {
                    toggle(product_id,product_price_id);
                    cart_ajax();
                    mdtoast('Add to cart sucessfully.', { 
                    interaction: true, 
                    duration: 2000,
                    actionText: 'View Cart',
                    action: function(){
                    this.hide(); 
                     window.location.href="cart"
                     },
                      });
                }
            }
           });
            }





function add_to_wishlist()
{
            var qty=$("#quantity").val();
            var user_id=$("#user_id").val();
            var product_id=$("#product_id").val();
            var price_id=$("#price_id").val();
            var token=$("#_token").val();
            $.ajax({
            url:'/addtowishlist_ajax1',
            type:'POST',
            data:{_token:$("#_token").val(),product_id:product_id,price_id:price_id,qty:qty},
            success:function(response)
            {
             
            if(response==2)
            {
           swal("Item Already Exit In The Wishlist!");
            }
            else
            {
                 toggle_wish(product_id,price_id); 
                 wishlist_ajax();
                    mdtoast('Add to Wishlist sucessfully.', { 
                    interaction: true, 
                    duration: 2000,
                    actionText: 'View Wishlist',
                    action: function(){
                    this.hide(); 
                     window.location.href="wishlist"
                     },
                    });
            }
            }


            });
}
        function  buy_now()
        {
            var quantity=$("#quantity").val();
            var user_id=$("#user_id").val();
            var product_id=$("#product_id").val();
            var product_price_id=$("#price_id").val();
            var token=$("#_token").val();
            var qty=$("#qty111111").val();

if(qty>0){

	$("#overlay").fadeIn(300);
            $.ajax({
                        url:'/add_to_noe_cart',
                        type:'POST',
                        data:{_token:$("#_token").val(),product_id:product_id,product_price_id:product_price_id,quantity:quantity},
                        success:function(response)
                        {
                            window.location.href='checkout?id='+product_id+''
                            
                            
                        
                        }
                   });
}else{
    swal('Sorry!! This Product Currently Unavailable');
}

         }

        </script>

        
<script>
					   var review1;	
						function revi(a)
                        {
					 		review1=a;
                          // alert(review1);
                            $.ajax({
                            type:'POST',
                            url:'/fafasend',
                            data:{
                                review1:a,
                               
                               _token:$("#_token").val()
                            },
                            success:function(data1) {

                       
                                
                               $("#rev").html(data1);
				  
                             }
                       });

		            }

   
     function send(){

			var review=review1;
            var title = $("#title").val();
            var message = $("#comments").val();
            var product_id=$("#product_id").val();
            var token=$("#_token").val();
           
           if(review!='' || title!='' || message!='')
           {
            $.ajax({
               type:'POST',
               url:'sendReview',
               data:{
                   review:review,
                   title: title,
                   product_id:product_id,
                   message : message,
                   _token : token
               },
               success:function(data) {
               
              
                swal("Review Submited Successfully", "You clicked the button!", "success").then(function(){
                location.reload();
                });

               }
            });
           }
           else
           {

            swal("Please Fillup Properly");  

           }
            
        }
  



					
        </script>
















        @endsection
