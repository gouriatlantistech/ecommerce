@extends('layouts.menu')

@section('title')
Offer Zone : SSJ Jewellery
@endsection

@section('content')
<style>
.btn-sm{
    font-size: 12px;
    padding: 8px 15px;
    border-radius: 0px;
    color: inherit;
    text-decoration: none;
    margin: 2px;
}
</style>


<main id="content" role="main" class="cart-page">
            <!-- breadcrumb -->
            <div class="bg-gray-13 bg-md-transparent">
                <div class="container">
                    <!-- breadcrumb -->
                    <div class="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Offer Zone</li>
                            </ol>
                        </nav>
                    </div>
                    <!-- End breadcrumb -->
                </div>
            </div>
            <!-- End breadcrumb -->

            <div class="container">
                <div class="my-6">
                    <h1 class="text-center">Offer Zone</h1>
                </div>
                <div class="mb-16 wishlist-table">
                    <form class="mb-4" action="#" method="post">
                        <div class="table-responsive">
                            <table class="table" cellspacing="0">
                                <thead>
                                    <tr>
                               
                                        <th class="product-thumbnail">&nbsp;</th>
                                        <th class="product-name">Title</th>
                                        <th class="product-price">Coupon Code</th>
                                        <th class="product-Stock w-lg-15">Coupon Value</th>
                                        <th class="product-Stock w-lg-15">Minimum <br>Order Price</th>
                                        <th class="product-Stock w-lg-15">Valid Upto</th>
                                      
                                    </tr>
                                </thead>
                                <tbody>
                                @php($i=0)
                                @foreach($coupon as $coupon1)
                                    <tr>
                                     
                                        <td class="d-none d-md-table-cell">
                                            <a href="javascript:void(0)"><img class="img-fluid max-width-100 p-1 border border-color-1" src="/coupon_image/{{$coupon1->coupon_image}}" alt="Image Description"></a>
                                        </td>

                                        <td data-title="Coupon Title">
                                            <a  class="text-gray-90">{{$coupon1->title}}</a>
                                        </td>
                                        <td data-title="Coupon Code">
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Copy to Clipboard" onclick="myFunction({{$coupon1->id}})" style="color:#007bff" class="text-gray-90"><input type="text" readonly style="  cursor: pointer;  width: max-content;;color:#007bff;    border: 0;"  value="{{$coupon1->coupon_code}}" id="myInput-{{$coupon1->id}}">
                                        </a>
                                         
                                        </td>
                                        <td data-title="Coupon Value">
                                            <span class="">@if($coupon1->coupon_type=='FLAT')FLAT <b>{{$coupon1->coupon_value}}</b> @else <b>{{$coupon1->coupon_value}}</b>% @endif Discount<br>
                                            @if($coupon1->cat_id!=0) On <b>

                                            @php($cat=DB::table('cats')->where('cat_id',$coupon1->cat_id)->first())
											{{$cat->cat_name}}
                                            </b> Category Products
                                            @endif
                                        
                                        </span>
                                        </td>

                                        <td data-title="Minimum Order Price">
                                        <span class="">&#8377;{{$coupon1->min_price}}</span>
                                        </td>
                                        <td data-title="Valid Upto">
                                        <span class="">{{$coupon1->coupon_validity}}</span>
                                        </td>
                                      
                                    </tr>
                                    @php($i++)
                                   @endforeach
                                </tbody>
                            </table>
                            @if($i==0)
<center>No Offer found. All offier expair or deactived</center>
                @endif
                        </div>
                    </form>
                </div>
            </div>
        </main>


   
<script
src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous">
</script>

<script>
  function myFunction(id) {
    var copyText = document.getElementById("myInput-"+id);

    copyText.select();
    document.execCommand("copy");
  //  copyText.setSelectionRange(0, 99999);

    swal(copyText.value + " is Copied!");
    
  }
</script>

@endsection