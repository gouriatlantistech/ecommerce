@extends('layouts.menu')

@section('title')
Wishlist : SSJ Jewellery
@endsection

@section('content')

    <!-- ========== MAIN CONTENT ========== -->
    <main id="content" role="main" class="cart-page">
            <!-- breadcrumb -->
            <div class="bg-gray-13 bg-md-transparent">
                <div class="container">
                    <!-- breadcrumb -->
                    <div class="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Wishlist</li>
                            </ol>
                        </nav>
                    </div>
                    <!-- End breadcrumb -->
                </div>
            </div>
            <!-- End breadcrumb -->

            <div class="container" style="    min-height: 248px;">
                <div class="my-6">
                    <h1 class="text-center">My wishlist</h1>
                </div>
                <div class="mb-16 wishlist-table" ID="wishlist_poduct_view">
                   
                </div>
            </div>
        </main>
        <!-- ========== END MAIN CONTENT ========== -->
        <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
        <script type="text/javascript">

$(document).ready(function() {

    wishlist_table();
});

function wishlist_table(){

   var token = $("#_token").val();
     
$.ajax({

    url:'wishlist_page_ajax',

    type:'POST',

    data:{_token:token},
   
  success:function(response)
    {
    
   
        $("#wishlist_poduct_view").html(response);
      
        sub_total();

  
    }

    });
}

	
</script>
@endsection