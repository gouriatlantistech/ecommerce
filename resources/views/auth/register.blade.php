
@extends('layouts.menu')
@section('title')
User Registration : SSJ Jewellery
@endsection
@section('content')
<div id="content" class="site-content" tabindex="-1">
  <!-- breadcrumb -->
  <div class="bg-gray-13 bg-md-transparent">
                <div class="container">
                    <!-- breadcrumb -->
                    <div class="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="javascript.void(0)">Register</a></li>
                               
                            </ol>
                        </nav>
                    </div>
                    <!-- End breadcrumb -->
                </div>
            </div>
            <!-- End breadcrumb -->
<div class="container">
                <div class="mb-4">
                    <h1 class="text-center">Register</h1>
                </div>
                <div class="my-4 my-xl-8">
                    <div class="row">
                        <div class="col-md-5 ml-xl-auto mr-md-auto mr-xl-0 mb-8 mb-md-0">
                            <!-- Title -->
                            <div class="border-bottom border-color-1 mb-6">
                                <h3 class="d-inline-block section-title mb-0 pb-2 font-size-26"> Register</h3>
                            </div>
                            <p class="text-gray-90 mb-4">Create new account today to reap the benefits of a personalized shopping experience.</p>
                            <!-- End Title -->
                            <form class="" novalidate="novalidate"  method="POST" action="{{ route('register') }}">
                            @csrf
                                <!-- Form Group -->
                                <div class="js-form-message form-group mb-5">
                                    <label class="form-label" for="RegisterSrEmailExample3">Enter name
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Enter Name" aria-label="Email address" required="" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                   @enderror
                                </div>
                                <div class="js-form-message form-group mb-5">
                                    <label class="form-label" for="RegisterSrEmailExample3">Email address
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email address" aria-label="Email address" required="" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                   @enderror
                                </div>
                               
                            <input type="hidden" name="role" value="USER">
                                <div class="js-form-message form-group mb-5">
                                    <label class="form-label" for="RegisterSrEmailExample3">Phone No
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input id="mobile" type="number" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" value="{{ old('mobile') }}" required placeholder="Phone No" aria-label="Email address" required="" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success">
                                    @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                   @enderror
                             
                                </div>

                                <div class="js-form-message form-group mb-5">
                                    <label class="form-label" for="RegisterSrEmailExample3">Password
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="password" aria-label="Email address" required="" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                   @enderror
                     
                                </div>

                                <div class="js-form-message form-group mb-5">
                                    <label class="form-label" for="RegisterSrEmailExample3">Confirm Password
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password" aria-label="Email address" required="" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success">
                                </div>
                                @if(isset($_GET['ref_code']))
                                <div class="js-form-message form-group mb-5">
                                    <label class="form-label" for="RegisterSrEmailExample3">Refer Code
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input class="form-control" id="ref_code" type="text" name="ref_code" value="{{$_GET['ref_code']}}" required placeholder="Enter Referral Code " readonly>
                                </div>
                                @endif

                                <div class="js-form-message form-group mb-5" id="otp2">
                                    <label class="form-label" for="RegisterSrEmailExample3">{{ __('OTP') }}
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="number" onkeyup="matchotp(this.value);" onchange="matchotp(this.value);" class="form-control" name="otp" id="otp" value="" placeholder="Enter OTP" aria-label="Email address" required="" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success" required autofocus>
                                    <span id="ps" style="color:red"></span>
                               
                                </div>


                                <input type="hidden" value="" id="otp_value">
                                <div class="mb-6">
                                    <div class="mb-3">
                                        
                                        <button type="submit" class="btn btn-primary-dark-w px-5"  id="as">
                                         {{ __('Register') }}
                              
                                          </button>

                                    </div>
                                </div>
                                <!-- End Button -->
                            </form>

                            <div class="mb-6" id="send-otp">
                                    <div class="mb-3">
                                <button class="btn btn-primary-dark-w px-6" onclick="sendOtp()">Send OTP</button>
                                   </div>    
                             </div>
                                    <div class="mb-3" id="resend-otp">
                                    Not received your code? <a href="javascript:void(0)" onclick="sendOtp()" >Resend code</a>
                                   </div>    
                        </div>
                        <div class="col-md-1 d-none d-md-block">
                            <div class="flex-content-center h-100">
                                <div class="width-1 bg-1 h-100"></div>
                                <div class="width-50 height-50 border border-color-1 rounded-circle flex-content-center font-italic bg-white position-absolute">or</div>
                            </div>
                        </div>
        <div class="col-md-5 ml-md-auto ml-xl-0 mr-xl-auto">
                            <!-- Title -->
                            <div class="border-bottom border-color-1 mb-6">
                                <h3 class="d-inline-block section-title mb-0 pb-2 font-size-26"> Mediatary Register</h3>
                            </div>
                            <p class="text-gray-90 mb-4">Create new account today to reap the benefits of a personalized shopping experience.</p>
                            <!-- End Title -->
                            <!-- Form Group -->
                            <form class="" novalidate="novalidate" method="POST" action="{{ route('mediator_register') }}">
                            @csrf
                                <div class="js-form-message form-group mb-5">
                                    <label class="form-label" for="RegisterSrEmailExample3">Enter name
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Enter name" aria-label="Email address" required="" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                   @enderror
                                </div>

                                <input type="hidden" name="status" value="NO">
                            <input type="hidden" name="usertype" value="MEDIATOR">

                            <input type="hidden" name="role" value="USER">
                                <div class="js-form-message form-group mb-5">
                                    <label class="form-label" for="RegisterSrEmailExample3">Email address
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email address" aria-label="Email address" required="" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="js-form-message form-group mb-5">
                                    <label class="form-label" for="RegisterSrEmailExample3">Phone No
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input id="mobile1" type="number" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" value="{{ old('mobile') }}" required placeholder="Phone No" aria-label="Email address" required="" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success">
                                    @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                   @enderror
                                </div>

                                <div class="js-form-message form-group mb-5">
                                    <label class="form-label" for="RegisterSrEmailExample3">Password
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password" aria-label="Email address" required="" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="js-form-message form-group mb-5">
                                    <label class="form-label" for="RegisterSrEmailExample3">Confirm Password
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password" aria-label="Email address" required="" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success">
                                </div>


                                @if(isset($_GET['ref_code']))
                                <div class="js-form-message form-group mb-5">
                                    <label class="form-label" for="RegisterSrEmailExample3">Refer Code
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input class="form-control" id="ref_code" type="text" name="ref_code" value="{{$_GET['ref_code']}}" required placeholder="Enter Referral Code " readonly>
                                </div>
                                @endif


                                <div class="js-form-message form-group mb-5" id="otp5">
                                    <label class="form-label" for="RegisterSrEmailExample3">{{ __('OTP') }}
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="number" onkeyup="matchotp1(this.value);" onchange="matchotp1(this.value);" class="form-control" name="otp" id="otp" value="" placeholder="Enter OTP" aria-label="Email address" required="" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success" required autofocus>
                                    <span id="ps1" style="color:red"></span>
                               
                                </div>


                                <input type="hidden" value="" id="otp_value1">



                                   <!-- Button -->
                                <div class="mb-6">
                                    <div class="mb-3">
                                        <button type="submit" class="btn btn-primary-dark-w px-5" id="as1">Register</button>
                                    </div>
                                </div>
                                <!-- End Button -->
                            </form>

                            <div class="mb-6" id="send-otp1">
                                    <div class="mb-3">
                                <button class="btn btn-primary-dark-w px-6" onclick="sendOtp1()">Send OTP</button>
                                   </div>    
                             </div>
                                    <div class="mb-3" id="resend-otp1">
                                    Not received your code? <a href="javascript:void(0)" onclick="sendOtp1()" >Resend code</a>
                                   </div> 
                           
                        </div>                
                    </div>
                </div>
            </div>
            </div><!-- #content -->



            <script>
            document.getElementById("as").style.display = "none";
            document.getElementById("as1").style.display = "none";
            document.getElementById("otp2").style.display = "none";
            document.getElementById("otp5").style.display = "none";
            document.getElementById("resend-otp").style.display = "none";
            document.getElementById("resend-otp1").style.display = "none";

        function sendOtp() {
       
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

          var mobile=($('#mobile').val());

            var len1=mobile.length;
            if(len1==10)
            {
            $.ajax( {
                url:'sendOtp1',
                type:'post',
                data: {'mobile': $('#mobile').val()},
                success:function(data) {
                   
                        document.getElementById("send-otp").style.display = "none";
                        document.getElementById("resend-otp").style.display = "block";
                        document.getElementById("otp2").style.display = "block";
                        document.getElementById("otp_value").value=data;
                        document.getElementById("as").style.display = "block";
                        document.getElementById("as").disabled = true;      
                },
                error:function () {
                    console.log('error');
                }
            });
            }
            else
            {

                swal("Please enter a valid mobile number!");
            }
        }

    function matchotp(otp){ 
    var orotp=document.getElementById("otp_value").value
    var len=otp.length;
    if(len==4)
    {
       if(otp==orotp)
       {
         document.getElementById("as").disabled = false;
       }else
       {
        document.getElementById("ps").innerHTML = "Please enter valid OTP!";
        document.getElementById("as").disabled = true;
       }
    }
     else if(len>6)
     {
         document.getElementById("ps").innerHTML = "Please enter valid OTP!";  
         document.getElementById("as").disabled = true;   
     }
     else
     {
    
     }
}






function sendOtp1() {
       
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       });

     var mobile=($('#mobile1').val());

       var len1=mobile.length;
       if(len1==10)
       {
       $.ajax( {
           url:'sendOtp11',
           type:'post',
           data: {'mobile': $('#mobile1').val()},
           success:function(data) {
              
                   document.getElementById("send-otp1").style.display = "none";
                   document.getElementById("resend-otp1").style.display = "block";
                   document.getElementById("otp5").style.display = "block";
                   document.getElementById("otp_value1").value=data;
                   document.getElementById("as1").style.display = "block";
                   document.getElementById("as1").disabled = true;      
           },
           error:function () {
               console.log('error');
           }
       });
       }
       else
       {

        swal("Please enter a valid mobile number!");
       }
   }

function matchotp1(otp){ 
var orotp=document.getElementById("otp_value1").value
var len=otp.length;
if(len==4)
{
  if(otp==orotp)
  {
    document.getElementById("as1").disabled = false;
    
  }else
  {
   document.getElementById("ps1").innerHTML = "Please enter valid OTP!";
   document.getElementById("as").disabled = true;   
  }
}
else if(len>6)
{
    document.getElementById("ps1").innerHTML = "Please enter valid OTP!";  
    document.getElementById("as").disabled = true;      
}
else
{

}
}




    </script>
@endsection
