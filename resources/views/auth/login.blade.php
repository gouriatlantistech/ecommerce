
@extends('layouts.menu')
@section('title')
User Login : SSJ Jewellery
@endsection

@section('content')
<div id="content" class="site-content" tabindex="-1">
  <!-- breadcrumb -->
  <div class="bg-gray-13 bg-md-transparent">
                <div class="container">
                    <!-- breadcrumb -->
                    <div class="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="javascript.void(0)">Accessories</a></li>
                               
                            </ol>
                        </nav>
                    </div>
                    <!-- End breadcrumb -->
                </div>
            </div>
            <!-- End breadcrumb -->
<div class="container">
                <div class="mb-4">
                    <h1 class="text-center">Login </h1>
                                @if(Session::has('message'))
                                <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>	
                                        <strong>{{ Session::get('message') }}</strong>
                                </div> 
                                @endif
                </div>
                <div class="my-4 my-xl-8">
                    <div class="row">
                        <div class="col-md-5 ml-xl-auto mr-md-auto mr-xl-0 mb-8 mb-md-0">
                            <!-- Title -->
                            <div class="border-bottom border-color-1 mb-6">
                                <h3 class="d-inline-block section-title mb-0 pb-2 font-size-26">Login with Email</h3>
                            </div>
                            <p class="text-gray-90 mb-4">Welcome back! Sign in to your account.</p>
                            <!-- End Title -->
                            <form class="" novalidate="novalidate"  method="POST" action="{{ route('login') }}">

                            @csrf
                                <!-- Form Group -->
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="signinSrEmailExample3">Email address
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus id="signinSrEmailExample3 email" placeholder="Email address" >
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                               
                                </div>
                                <input type="hidden" name="role" value="USER">
                                <!-- End Form Group -->

                                <!-- Form Group -->
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="signinSrPasswordExample2">Password <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" id="signinSrPasswordExample2 password" placeholder="Password" >
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                   @enderror
                                </div>
                                <!-- End Form Group -->

                                <!-- Checkbox -->
                                <div class="js-form-message mb-3">
                                    <div class="custom-control custom-checkbox d-flex align-items-center">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                   <label class="form-check-label" for="remember" style="margin-top: 10px;">
                                        {{ __('Remember Me') }}
                                     </label>
                                    </div>
                                </div>
                                <!-- End Checkbox -->

                                <!-- Button -->
                                <div class="mb-1">
                                    <div class="mb-3">
                                      

                                                <button type="submit" class="btn btn-primary-dark-w px-6">
                                                {{ __('Login') }}
                                                </button>

                                                    @if (Route::has('password.request'))
                                                            <a class="btn btn-link" href="{{ route('password.request') }}" >
                                                                {{ __('Forgot Your Password?') }}
                                                            </a>
                                                @endif
                                               
                                    </div>
                                  
                                </div>
                                <!-- End Button -->
                            </form>
                        </div>
                        <div class="col-md-1 d-none d-md-block">
                            <div class="flex-content-center h-100">
                                <div class="width-1 bg-1 h-100"></div>
                                <div class="width-50 height-50 border border-color-1 rounded-circle flex-content-center font-italic bg-white position-absolute">or</div>
                            </div>
                        </div>
                        <div class="col-md-5 ml-md-auto ml-xl-0 mr-xl-auto">
                            <!-- Title -->
                            <div class="border-bottom border-color-1 mb-6">
                                <h3 class="d-inline-block section-title mb-0 pb-2 font-size-26">Login with Otp</h3>
                            </div>
                            <p class="text-gray-90 mb-4">Welcome back! Sign in to your account.</p>
                            <!-- End Title -->
                            <!-- Form Group -->
                            <form class=""  method="POST" action="{{ route('loginWithOtp') }}">
                            @csrf
                                <div class="js-form-message form-group mb-5">
                                    <label class="form-label" for="RegisterSrEmailExample3">{{ __('Mobile No') }}
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="number" class="form-control" name="mobile" id="mobile" placeholder="Mobile No" aria-label="Email address" required="" autofocus>
                                </div>
                                <input type="hidden" name="role" value="USER">


                                <div class="js-form-message form-group mb-5" id="otp2">
                                    <label class="form-label" for="RegisterSrEmailExample3">{{ __('OTP') }}
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="number" onkeyup="matchotp(this.value);" onchange="matchotp(this.value);" class="form-control" name="otp" id="otp" value="" placeholder="Enter OTP"  required=""   autofocus>
                                    <span id="ps" style="color:red"></span>
                               
                                </div>

                                <input type="hidden" value="" id="otp_value">


                               <!-- Button -->
                                <div class="mb-6" id="otp1">
                                    <div class="mb-3">
                                        <button type="submit" class="btn btn-primary-dark-w px-6" id="as" >Login</button>
                                               
                                    </div>
                                </div>
                                <!-- End Button -->
                            </form>


                         
                           

                              <div class="mb-6" id="send-otp">
                                    <div class="mb-3">
                                <button class="btn btn-primary-dark-w px-6" onclick="sendOtp()">Send OTP</button>
                                             
                                                            <a class="btn btn-link" href="/reset_link" >
                                                                {{ __('Forgot Your Password?') }}
                                                            </a>
                                             
                                   </div>    
                             </div>
                                    <div class="mb-3" id="resend-otp">
                                    Not received your code? <a href="javascript:void(0)" onclick="sendOtp()" >Resend code</a>
                                   </div>    
                          


                           
                           
                            <!-- <h3 class="font-size-18 mb-3">Sign up today and you will be able to :</h3>
                            <ul class="list-group list-group-borderless">
                                <li class="list-group-item px-0"><i class="fas fa-check mr-2 text-green font-size-16"></i> Speed your way through checkout</li>
                                <li class="list-group-item px-0"><i class="fas fa-check mr-2 text-green font-size-16"></i> Track your orders easily</li>
                                <li class="list-group-item px-0"><i class="fas fa-check mr-2 text-green font-size-16"></i> Keep a record of all your purchases</li>
                            </ul> -->
                         </div>
                    </div>
                </div>
            </div>
            </div><!-- #content -->


            <script>
            document.getElementById("as").style.display = "none";
            document.getElementById("otp2").style.display = "none";
            document.getElementById("resend-otp").style.display = "none";
        function sendOtp() {
       
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // alert($('#mobile').val());
            $.ajax( {
                url:'sendOtp',
                type:'post',
                data: {'mobile': $('#mobile').val()},
                success:function(data) {
                    var data1 = data.split("|");
                    if(data1[0] != 0){
                        document.getElementById("send-otp").style.display = "none";
                        document.getElementById("resend-otp").style.display = "block";
                        document.getElementById("otp2").style.display = "block";
                        document.getElementById("otp_value").value=data1[1];
                        document.getElementById("as").style.display = "block";
                        document.getElementById("as").disabled = true;
                       
                       
                       
                    }else{
                      
                        swal("Mobile No not found!");
                    }

                },
                error:function () {
                    console.log('error');
                }
            });
        }




    function matchotp(otp){ 
    var orotp=document.getElementById("otp_value").value
    var len=otp.length;
    if(len==4)
    {
       if(otp==orotp)
       {
         document.getElementById("as").disabled = false;
       }else
       {
        document.getElementById("ps").innerHTML = "Please enter valid OTP!";
        document.getElementById("as").disabled = true;
       }
    }
     else if(len>6)
     {
         document.getElementById("ps").innerHTML = "Please enter valid OTP!";  
         document.getElementById("as").disabled = true;   
     }
     else
     {
    
     }
}



    </script>

@endsection
