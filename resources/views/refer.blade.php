@extends('layouts.menu')

@section('title')
Refer and Earn : SSJ Jewellery
@endsection

@section('content')
 <!-- ========== MAIN CONTENT ========== -->
 <main id="content" role="main">
            <!-- breadcrumb -->
            <div class="bg-gray-13 bg-md-transparent">
                <div class="container">
                    <!-- breadcrumb -->
                    <div class="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Refer and Earn</li>
                            </ol>
                        </nav>
                    </div>
                    <!-- End breadcrumb -->
                </div>
            </div>
            <!-- End breadcrumb -->

            <div class="container">
                <div class="mb-5 text-center pb-3 border-bottom border-color-1">
                   <img src="http://auveraa.com/assets/images/earn-banner.png" style="width:50%">
                    <p class="text-gray-90 font-size-20 mb-0 font-weight-light"><span style="width:55%">Join me on Siddharth Shah Jewelers a Secure website For Jewellery Shopping. Enter my Code <b style="color:red">{{$refer_code->refer_code}}</b> to earn exclusive offers and your friends get cashback on your first order.</span></p>
                </div>
                <div class="d-flex mb-6">
                    <!-- Search Jobs Form -->
                    <span class="d-block d-md-flex flex-horizontal-center w-100 w-lg-80 w-xl-50 mx-md-auto">
                        <div class="mb-3 mb-md-0 col px-md-2 px-0" >
                            <!-- Input -->
                            <div class="js-focus-state">
                                <input type="number" id="phone_number" class="form-control" placeholder="Enter friends mobile number" aria-label="Search products…" aria-describedby="keywordInputAddon">
                            </div>
                            <!-- End Input -->
                        </div>
                       
                        <div class="">
                            <button type="submit" id="send" class="btn btn-block btn-primary-dark-w px-5">Send SMS</button>
                        </div>
                        <!-- End Checkbox -->
                    </span>
                 
                    <!-- End Search Jobs Form -->
                </div>
            <center>    <ul style="display: inline-flex;list-style:none;padding:0">
                                        <li class="facebook" style="padding: 10px;font-size: 45px;">
                                            <a href="fb-messenger://share/?link= https://auveraa.com/register?ref_code={{$refer_code->refer_code}}&text=oin me on Siddharth Shah Jewelers a Secure website For Jewellery Shopping. Enter my Code  {{$refer_code->refer_code}} or click the link to earn exclusive offers and your friends get cashback on your first order. https://auveraa.com/register?ref_code={{$refer_code->refer_code}}" data-toggle="tooltip" target="_blank" title="Facebook">
                                                <i class="fab fa-facebook-messenger"></i>
                                            </a>
                                        </li>
                                        <li class="twitter" style="padding: 10px;font-size: 45px;">
                                            <a href="https://twitter.com/intent/tweet?url=https://auveraa.com/register?ref_code={{$refer_code->refer_code}} &text=Join me on Siddharth Shah Jewelers a Secure website For Jewellery Shopping. Enter my Code  {{$refer_code->refer_code}} or click the link to earn exclusive offers and your friends get cashback on your first order. https://auveraa.com/register?ref_code={{$refer_code->refer_code}}" data-toggle="tooltip" target="_blank" title="Twitter">
                                                <i class="fab fa-twitter-square"></i>
                                            </a>
                                        </li>
                                        <li class="youtube" style="padding: 10px;font-size: 45px;">
                                            <a href="whatsapp://send?text=Join me on Siddharth Shah Jewelers a Secure website For Jewellery Shopping. Enter my Code  {{$refer_code->refer_code}} or click the link to earn exclusive offers and your friends get cashback on your first order. https://auveraa.com/register?ref_code={{$refer_code->refer_code}}" data-toggle="tooltip" target="_blank" title="Whatsapp">
                                            <i class="fab fa-whatsapp-square"></i>
                                            </a>
                                        </li>
                                        
                                        <li class="instagram" style="padding: 10px;font-size: 45px;">
                                            <a href="mailto:?subject=Refer and Earn &amp;body=Join me on Siddharth Shah Jewelers a Secure website For Jewellery Shopping. Enter my Code  {{$refer_code->refer_code}} or click the link to earn exclusive offers and your friends get cashback on your first order. https://auveraa.com/register?ref_code={{$refer_code->refer_code}}" data-toggle="tooltip" target="_blank" title="Gmail">
                                            <i class="fas fa-envelope"></i>
                                            </a>
                                        </li>
                                        <li class="instagram" style="padding: 10px;font-size: 45px;">
                                           
                                            <a href="javascript:void(0)" onclick="copytoclipboard();" data-toggle="tooltip"  title="Copy to Clipboard">
                                            <i class="far fa-copy"></i>
                                            </a>
                                        </li>
                                       
                          </ul> </center> 
             
                <!-- End Brand Carousel -->
            </div>
        </main>
        <!-- ========== END MAIN CONTENT ========== -->
        <input type="text" id="code" style="color:white;border:0" value="{{$refer_code->refer_code}}">
        <input type="text" id="copytextrrrr" style="color:white;border:0" readonly value="Join me on Siddharth Shah Jewelers a Secure website For Jewellery Shopping. Enter my Code  {{$refer_code->refer_code}} or click the link to earn exclusive offers and your friends get cashback on your first order. https://auveraa.com/register?ref_code=gfsgtgtgrt">
        <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>

        <script>
$(document).ready(function(){
    $('#send').click(function () {
       
var token=$("#_token").val();
var code=$("#code").val();
var phone=$("#phone_number").val();

if(phone.length==0){
   mdtoast('Please enter your friends phone number', { 
  type: 'error',
  duration: 3000
});
}else{
if(phone.length==10){
 
$.ajax({

url:'/refersend',

type:'POST',

data:{phone:phone,code:code,_token:token},


success:function(response)
{
    mdtoast('Link send successfully on your friends phone number', { 
  type: 'success',
  duration: 3000
});
}

})
}else{
    mdtoast('Please enter a valid phone number', { 
  type: 'warning',
  duration: 3000
});
}
}
});
//expair();
//alert();
});
</script>



<script>
function copytoclipboard(){

  /* Get the text field */
  var copyText = document.getElementById("code");

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  //alert("Copied the text: " + copyText.value);

    mdtoast('Link send successfully copied.', { 
  type: 'success',
  duration: 3000
});
}
</script>

@endsection