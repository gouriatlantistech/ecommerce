<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="/img/fevicon/favicon48.png" sizes="48x48">
<title>500 Internal Server Error : Auveraa Life Style</title>

<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900" rel="stylesheet">

<link type="text/css" rel="stylesheet" href="404\css\style.css">



</head>
<body>
<div id="notfound">
<div class="notfound">
<div class="notfound-404">
<h1>Oops!</h1>
</div>
<h2>500 - Internal Server Error</h2>
<p>The page you are looking for might have been removed had its name changed or is temporarily unavailable.</p>
<a href="/">Go To Homepage</a>
</div>
</div>
</html>
