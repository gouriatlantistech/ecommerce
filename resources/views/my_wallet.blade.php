@extends('layouts.menu')

@section('title')
My Wallet : SSJ Jewellery
@endsection

@section('content')

 <!-- ========== MAIN CONTENT ========== -->
 <main id="content" role="main" class="cart-page">
            <!-- breadcrumb -->
            <div class="bg-gray-13 bg-md-transparent">
                <div class="container">
                    <!-- breadcrumb -->
                    <div class="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">My wallet</li>
                            </ol>
                        </nav>
                    </div>
                    <!-- End breadcrumb -->
                </div>
            </div>
            <!-- End breadcrumb -->

            <div class="container" style="    min-height: 348px;">
                <div class="mb-4">
                    <h1 class="text-center">My Wallet</h1>
                    <br>
                </div>
                <div class="input-group input-group-pill">
                                                                                                                            <input type="email" readonly class="btn btn-dark btn-sm-wide height-40 py-2 "  name="email" value="Wallet Amount"  style="width: 148px;border-bottom-right-radius: 0px;border-top-right-radius: 0px;">
                                                                <div class="input-group-append">
                                        <button type="submit" class="border-0 height-40" style="border-bottom-right-radius: 30px;border-top-right-radius: 30px;Width:135px">&#8377;{{$wallet_amount->wallet_ammount}}</button>
                                       </div>
                                                                                                
                                </div><br>       
                    <table class="table" cellspacing="0">
                        <thead>
                            <th>Date & Time</th>
                            <th>Remarks</th>
                            <th>Transaction Amount</th>
                            <th>Transaction Type</th>
                            <th>After Transaction Amount</th>
                        </thead>
                        <tbody>
                            @foreach($user_wallet_transactions as $user_wallet_transactions)
                                <tr>
                                    <td>{{date('d M, Y h:i A',strtotime($user_wallet_transactions->created_at))}}</td>
                                    <td>{{$user_wallet_transactions->remarks}}@if($user_wallet_transactions->order_id!=null)({{$user_wallet_transactions->order_id}}) @endif</td>
                                    <td>{{$user_wallet_transactions->transaction_amount}}</td>
                                    <td>{{$user_wallet_transactions->tranasaction_type}}</td>
                                    <td>{{$user_wallet_transactions->after_transaction_amount}}</td>
                                </tr>
                            @endforeach
                        </tbody>

                    </table>
<br><br>
<br>



            </div>
        </main>
        <!-- ========== END MAIN CONTENT ========== -->
           
        <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
        </script>
   
    @endsection