
@extends('admin.layouts.menu')
@section('body')


<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">{{$product2->product_name}}</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">{{$product2->product_name}}</li>
								</ol>
							</div>

							
							<div class="row">
							<?php $product_id=$_GET['product_id']  ?>


							<div class="col-md-6 col-xl-6">
								<div class="card shadow">
									<div class="card-header">
										<h2 class="mb-0">Product Details</h2>
									</div>
									<div class="card-body text-left">
										
											Name : <b style="font-size:18px">{{$product2->product_name}}</b><br>
											Product Code : <b>{{$product2->product_code}}</b><br>
											HSN Code : <b>{{$product2->hsn_code}}</b><br>
											Category Name :
											@if($product2->cat_id!=0)
												<b>{{$cat->cat_name }}</b>
											@else
												<b>----</b>
											@endif
											<br>
											Sub-category Name :
											@if($product2->sub_cat_id!=0)
												<b>{{$sub_cat->sub_cat_name}}</b>
											@else
												<b>----</b>
											@endif
											<br>
											Sub-sub-category Name :
											@if($product2->sub_sub_cat_id!=0)
												<b>{{$sub_sub_cat->sub_sub_cat_name}}</b>
											@else
												<b>----</b>
											@endif
											<br>

										
											Active Status : <b>{{$product2->active_status}}</b><br>
											Review : <b>{{$product2->review}}</b><br>
											Total Review : <b>{{$product2->total_review}}</b><br>
											

											<a href="update_product_details?product_id={{$product2->product_id}}" class="btn btn-icon btn-default mt-1 mb-1">
																											<span class="btn-inner--icon"><i class="fe fe-edit"></i></span>
																											<span class="btn-inner--text">Update</span>
																								</a>

									</div>
								</div>
							

							</div>
							<div class="col-md-6 col-xl-6">
								<div class="card shadow">
									<div class="card-header">
										<h2 class="mb-0">Product Description</h2>
									</div>
									<div class="card-body text-center">
										

										<?php echo $product2->description; ?>


										<br>

<a href="update_product_description?product_id={{$product2->product_id}}" class="btn btn-icon btn-default mt-1 mb-1">
																<span class="btn-inner--icon"><i class="fe fe-edit"></i></span>
																<span class="btn-inner--text">Update</span>
															</a>


									</div>
								</div>
							</div>
						</div>

						<div class="row">
							
							
							<div class="col-md-6 col-xl-6">
								<div class="card shadow">
									<div class="card-header">
										<h2 class="mb-0">Features</h2>
									</div>
									<div class="card-body text-center" >
										<table width="100%">
										 
									@foreach($feature as $key => $feature1)
									<tr>
									 <td width="45%">	
									  <p style="text-align: left">{{$key+1}}.{{$feature1->features}}  </p> 
									  </td>
                                          <td width="15%">
											<a href="delete_feature?id={{$feature1->product_feature_id }}&product_id={{$feature1->product_id}}"><i class="fa fa-trash"></i></a></td>
									  </tr>
									 @endforeach

									 </table>

	                               <a href="add_features?id={{$_GET['product_id']}}" class="btn btn-primary btn-pill mt-1 mb-1">Add Features</a>

									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-6">
								<div class="card shadow">
									<div class="card-header">
										<h2 class="mb-0">Product Specification</h2>
									</div>
									<div class="card-body text-left">
										
								<table width="100%">
									
									 @foreach($specification as $specification1)
									 <tr>
									 <td width="30%%" class="text-left"><b>{{$specification1->title}}</b></td>
										 <td width="60%" class="text-center">{{$specification1->description}}</td>	
										 <td width="10%">
										
											<a href="delete_specification?id={{$specification1->product_spacifications_id}}&product_id={{$specification1->product_id}}"><i class="fa fa-trash"></i></a>
										
										</td>
									 </tr>
									 @endforeach
									
								</table>
								<br>
								<center>	<a href="add_specification?id={{$product2->product_id}}" class="btn btn-icon  btn-primary mt-1 mb-1" >
									<span class="btn-inner--icon"><i class="fe fe-check-square"></i></span>
									<span class="btn-inner--text">Add Specification</span>
								</a></center>



									</div>
								</div>
							</div>
						</div>









							<div class="row">
							<div class="col-md-12 col-xl-12">
								<div class="card shadow">
									<div class="card-header">
										<h2 class="mb-0">Price Chart</h2>
									</div>
									<div class="card-body">
										<div class="row">
										@foreach($price as $key=>$price1)
											<div class="col-lg-4">
												<div class="tile pt-inner text-center p-0 overflow-hidden">
													<div class="pti-header bg-success">
														<h2>
														{{$price1->size}}
														</h2>
														<div class="ptih-title">Product Size</div>
													</div>
			

													<div class="pti-body">
											
													<div class="ptib-item"  style="border: solid 1px;border-top: none;">
												Product MRP<font size="2px">(RS)</font> : <b>&#8377;{{$price1->mrp}}/-</b>
											</div>
											
											<div class="ptib-item"  style="border: solid 1px;border-top: none;">
												Selling Price<font size="2px">(RS)</font> : <b>&#8377;{{$price1->selling_price}}/-</b>
											</div>

											<div class="ptib-item"  style="border: solid 1px;border-top: none;">
												Mediator Price<font size="2px">(RS)</font> : <b>&#8377;{{$price1->mediator_price}}/-</b>
											</div>
											
										
											<div class="ptib-item" style="border: solid 1px;border-top: none;">
												Total Stock<font size="2px">(Qty)</font>: <b>{{round($price1->total_stock)}}</b>
											</div>
											<div class="ptib-item" style="border: solid 1px;border-top: none;">
												Out Stock<font size="2px">(Qty)</font>: <b>{{round($price1->total_stock-$price1->available_stock)}}</b>
											</div>
											<div class="ptib-item" style="border: solid 1px;border-top: none;">
												Available Stock<font size="2px">(Qty)</font>: <b>{{round($price1->available_stock)}}</b> @if($price1->available_stock<6) <span class="badge badge-pill badge-danger">Low Stock</span>
												@elseif($price1->available_stock<1) <span class="badge badge-pill badge-danger">Out of Stock</span>@endif
											</div>
										</div>

												
										<div class="pti-body">
														
													<div class="ptib-item" style="border: solid 1px;border-top: none;border-bottom-left-radius: 7px;border-bottom-right-radius: 7px;">
															<a href="update_price?product_prices_id={{$price1->product_prices_id }}" class="btn btn-icon btn-default mt-1 mb-1" >
																<span class="btn-inner--icon"><i class="fe fe-edit"></i></span>
																<span class="btn-inner--text">Update</span>
															</a>
														<a href="#" class="btn btn-icon btn-danger mt-1 mb-1">
																<span class="btn-inner--icon"><i class="fe fe-eye"></i></span>
																<span class="btn-inner--text">Inventory</span>
															</a>
														</div>
														
														
													</div>
			
													
												</div>
											</div>
											@endforeach	
										

											
											
									</div>
									<!--	<center>
										<a href="add_color?id={{$product2->product_id}}"><button class="btn btn-icon btn-primary mt-1 mb-1" type="button">
												<span class="btn-inner--icon"><i class="fe fe-check-square"></i></span>
												<span class="btn-inner--text">Add Size</span>
											</button></a>
										</center>-->

									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12 col-xl-12">
								<div class="card shadow">
									<div class="card-header">
										<h2 class="mb-0">Product image</h2>
									</div>
									<div class="card-body">
										<div class="row">
											@foreach($image as $image)
										
										
									<div class="" style="padding:5px">
										<img src="/product_image/{{$image->image}}" height="160px" style="padding:10px">	
										<br><center>
											<a href="delete_product_image?id={{$image->product_image_id }}" class="btn btn-icon btn-pill btn-danger mt-1 mb-1 btn-sm" >
											<span class="btn-inner--icon">Delete</span>
										</a>
										<br><br>
									
									</div>
								
											@endforeach
										</div>	
											<form action="add_product_image" method="post" enctype="multipart/form-data">
												<input type="hidden" value="{{$product2->product_id}}" name="product_id">
												<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>	
												<div class="row" style="padding-left:70px;padding-top: 27px;border: solid 1px;">


											<div class="col-md-8">
													<input type="file" required onchange="validateImage(this.id)" id="img" name="image[]" multiple>
											</div>

											<div class="col-md-4">


												<input type="submit" class="btn btn-primary">
												
											
											</div>
												
												</div>
											</form>
										
									</div>
								</div>
							</div>
						</div>
						<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>	
						
						
<script type="text/javascript">
function validateImage(id) {
    var formData = new FormData();
 
    var file = document.getElementById(id).files[0];
 
    formData.append("Filedata", file);
    var t = file.type.split('/').pop().toLowerCase();
    if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        alert('Please select a valid image file');
        document.getElementById(id).value = '';
        return false;
    }
    if (file.size > 1024000) {
        alert('Max Upload size is 1MB only');
        document.getElementById(id).value = '';
        return false;
    }
    return true;
}
</script>
					
                        @endsection							