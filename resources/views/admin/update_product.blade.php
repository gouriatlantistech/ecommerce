
@extends('vendor.layouts.vendor_menu')
@section('body')
			<!-- app-content-->
			
           @foreach($product as $product)
						<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Update Product</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Update Product</li>
								</ol>
							</div>
							
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
											<h2 class="mb-0">Update Product</h2>
										</div>
										<div class="card-body">
											<div class="nav-wrapper">
												<ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
													<li class="nav-item">
														<a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="fe fe-home mr-2"></i>Update Details</a>
													</li>
													<li class="nav-item">
														<a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="fe fe-user mr-2"></i>Update Image</a>
													</li>
													
												</ul>
											</div>
											<div class="card shadow mb-0">
												<div class="card-body">
													<div class="tab-content" id="myTabContent">
														<div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
        <!-- Body -->
        <div class="row">
                                        <div class="col-md-6">
													<div class="form-group">
														<input type="text" class="form-control" name="code" value="" readonly required placeholder="Product Code" value="Auveraa-{{$product->product_code}}">
													</div>
													
											</div>
											<div class="col-md-6">
													<div class="form-group">
														<input type="text" class="form-control" name="product_name" required placeholder="Product name " value="{{$product->product_name}}">
													</div>
													
											</div>
                                        </div>
                                        <input type="hidden" class="form-control" name="product_name" required placeholder="Product name " value="{{$product_id=$product->product_id}}">
                                        <div class="row">
                                        <div class="col-md-4">
                                       @php($category=DB::table('cats')->get())
													<div class="form-group">
														<select class="form-control" id="cat" required>
                                                         @foreach($category as $category)
                                                        <option  value="{{$category->cat_id}}">{{$category->cat_name}}</option>
                                                       @endforeach
                                                        </select>
                                                       
													</div>
													
											</div>
											<div class="col-md-4">
													<div class="form-group">
                                                    <select class="form-control" id="sub_cat" required>
                                                        <option>--Select sub Category-- </option>
                                                       
                                                        </select>
													</div>
													
											</div>
                                            <div class="col-md-4">
													<div class="form-group">
                                                    <select class="form-control" id="sub_sub_cat" name="product_sub_sub_cat_id" required>
                                                    <option>--Select Sub Sub Category-- </option>
                                                        </select>
													</div>
													
											</div>
                                        </div>
                                        <div class="row">
                                        
                                        <div class="col-md-6">
                                        <div class="form-group">
                                        
                                        <textarea class="form-control" name="product_description" placeholder="Product Description" id="" value="{{$product->description}}"></textarea>
                                        </div></div>

                                        <div class="col-md-6">
                                    Product Brand
                                        <div class="form-group">
                                        <input type="text" class="form-control" name="brand" required placeholder="Brand" value="{{$product->brand}}">
                                       
                                        </div></div>
										
                                        </div>
                                        
                                        <h3>Product specification</h3>
                                        <div class="row">
                                       
                                        <div class="col-md-12">
                                        <div class="form-group">
                                        @php($specification=DB::table('specifications')->where('product_id',$product_id)->get())
                                          @foreach($specification as $specification)
                                        <input type="text" value="" class="form-control" name="specification1[]"  placeholder="Product specification Type" value="{{$specification->specification_type}}"><br>
                                        <input type="text" value="" class="form-control" name="specification[]"  placeholder="Product specification " value="{{$specification->specification_name}}">
                                       <br>
                                        @endforeach
                                        <br><span id ="view1"></span>&nbsp;&nbsp;&nbsp;<a class="btn btn-primary" id="click1">+</a>
                                        
                                        </div></div>
										
                                        </div>
                                     @php($feature=DB::table('features')->where('product_id',$product_id)->get())

                                        <h3>Product Feature</h3>
                                        <div class="row">
                                       
                                        <div class="col-md-12">
                                        <div class="form-group">
                                        @foreach($feature as $feature)
                                        <input type="text" value="" class="form-control" name="fetuars[]"  placeholder="Product Feature " value="{{$feature->feature_name}}"><br> @endforeach<br><span id ="view"></span>&nbsp;&nbsp;&nbsp;<a class="btn btn-primary" id="click">+</a>
                                       
                                        </div></div>
										
                                        </div>

                                        <div class="row">
                                        <div class="col-md-6">
                                        <h3>Product Quantity</h3>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="product_quantity" required placeholder="Quantity" value="{{$product->qty}}">
													</div>
													
											</div>
											
                                            <div class="col-md-6">
													<h3>product Type</h3>
                                                    <div class="custom-control custom-radio mb-3">
														<input name="custom-radio-1" class="custom-control-input" id="customRadio1" checked="" type="radio" value="none">
														<label class="custom-control-label" for="customRadio1">None</label>
													</div>
													<div class="custom-control custom-radio mb-3">
														<input name="custom-radio-1" class="custom-control-input" id="customRadio2"  type="radio" value="top">
														<label class="custom-control-label" for="customRadio2">Top Deals</label>
													</div>
													<div class="custom-control custom-radio mb-3">
														<input name="custom-radio-1" class="custom-control-input" id="customRadio3"  type="radio" value="new">
														<label class="custom-control-label" for="customRadio3">New Arrival</label>
													</div>
													<div class="custom-control custom-radio mb-3">
														<input name="custom-radio-1" class="custom-control-input" id="customRadio4"  type="radio" value="best">
														<label class="custom-control-label" for="customRadio4">Best Deals</label>
													</div>
													
												</div>

                                                <div class="col-md-6">
                                            <h3>Product Size Require</h3>
													<div class="form-group">
                                                    <select class="form-control" id="" name="size_require">
                                                        
                                                       <option value="yes">Yes</option>
                                                       <option value="no">No</option>
                                                       
                                                        </select>
													</div>
													
											</div>







                                        </div>
                                        
                                        <div class="row">
                                        <div class="col-md-6">
                                        <h3>Weight</h3>
                                        <div class="form-group">
                                        <input type="text" class="form-control" name="p_weight" id="p_weight" placeholder=" Product Weight" value="{{$product->product_weight}}">
                                        </div>
                                        </div>
										<div class="col-md-6">
                                        <h3>Charge</h3>
                                        <div class="form-group">
                                        <input type="text" class="form-control" name="delevery_charge" placeholder="Charge" id="weight_view" value="{{$product->delivary_charge}}">
                                        </div>
                                        </div>
                                        </div>
                                        <h2> Calculate Price here</h2>
                                        <div style="background-color:#A4B0BD;padding-top:10px;padding-left:10px;padding-right:10px;box-shadow: 10px 5px 5px #264d73;">
                                        <div class="row">
                                        <div class="col-md-4">
                                        <label>Product Price</label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="product_price[]" placeholder="Price(1 and 2)" id="price1" value="">
													</div>
													
											</div>
											<div class="col-md-4">
                                            <label>Product Price</label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="product_price[]" placeholder="Price(3 to 20)" id="price2" value="">
													</div>
													
											</div>
                                            <div class="col-md-4">
                                            <label>Product Price</label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="product_price[]" placeholder="Price(21 to 50)" id="price3" value="">
													</div>
													
											</div>
                                        </div>
                                           
                                        <div class="row">
                                        <div class="col-md-4">
                                        <label>Admin Commission(<b id="admin_dis1"></b>)</label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="commission[]" placeholder="Admin Commission" id="discount1" value="">
													</div>
													
											</div>
											<div class="col-md-4">
                                            <label>Admin Commission(<b id="admin_dis2"></b>)</label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="commission[]" placeholder="Admin Commission" id="discount2" value="">
													</div>
													
											</div>
                                            <div class="col-md-4">
                                            <label>Admin Commission (<b id="admin_dis3"></b>)</label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="commission[]" placeholder="Admin Commission" id="discount3" value="">
													</div>
													
											</div>
                                        </div>

										<div class="row">
                                        <div class="col-md-4">
                                        <label>Service Tax </label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="service_tax[]" placeholder="Service Tax for(1 to 5)" id="gst1" value="">
													</div>
													
											</div>
											<div class="col-md-4">
                                            <label>Service Tax </label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="service_tax[]" placeholder="Service tax for(6 to 20)" id="gst2" value="">
													</div>
													
											</div>
                                            <div class="col-md-4">
                                            <label>Service Tax </label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="service_tax[]" placeholder="Service tax for(21 to 50)" id="gst3" value="">
													</div>
													
											</div>
                                        </div>

                                        <div class="row">
                                        <div class="col-md-4">
                                        <label>Delivery Charge</label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="delivery_charge[]" placeholder="Delivery Charge" id="dc1">
													</div>
													
											</div>
											<div class="col-md-4">
                                            <label>Delivery Charge</label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="delivery_charge[]" placeholder="Delivery Charge" id="dc2">
													</div>
													
											</div>
                                            <div class="col-md-4">
                                            <label>Delivery Charge</label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="delivery_charge[]" placeholder="Delivery Charge" id="dc3">
													</div>
													
											</div>
                                        </div>


                                        <div class="row">
                                        <div class="col-md-4">
                                        <label>Closing Fees</label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="closing_fees[]" placeholder="Closing fees" id="cf1">
													</div>
													
											</div>
											<div class="col-md-4">
                                            <label>Closing Fees</label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="closing_fees[]" placeholder="Closing fees" id="cf2">
													</div>
													
											</div>
                                            <div class="col-md-4">
                                            <label>Closing Fees</label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="closing_fees[]" placeholder="Closing fees" id="cf3">
													</div>
													
											</div>
                                        </div>
                                        
                                        <div class="row">
                                       
                                        <div class="col-md-4">
                                        <label>Total Amount To Get</label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="vendor_amount[]" placeholder="My Amount" id="vendor1" value="">
													</div>
													
											</div>
											<div class="col-md-4">
                                            <label>Total Amount To Get</label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="vendor_amount[]" placeholder="My Amount" id="vendor2" value="">
													</div>
													
											</div>
                                            <div class="col-md-4">
                                            <label>Total Amount To Get</label>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="vendor_amount[]" placeholder="My Amount" id="vendor3" value="">
													</div>
													
											</div>
                                        </div>
                                   </div>









														</div>




                                                        <!-- 2nd tab--> 
														<div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
														
                             <div class="row">
													
								<div class="col-md-12">
									<div class="card card-profile  overflow-hidden">
										<div class="card-body text-center cover-image" data-image-src="assets/img/profile-bg.jpg">
											
												
																
												
													
											<input type="file" class="dropify" data-height="" name="m_pic"  />
										           
											
											
											
											<p class="mb-4 text-white">Add Banner</p>
											
											<input type="submit" class="btn btn-success btn-sm" value="submit" name="submit">
										</div></div></div></div>

                                                        <div class="row">
													
														<div class="col-lg-4 profile-image">
															<div class="card shadow"><a href=""><i class="side-menu__icon ion-close-round"></i></a>
																<img src="" alt="gallery">
															</div>
														</div>
														
														
													</div>
														</div>
														
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
						<!-- file uploads js -->
   	@endforeach
						
       <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
	
</script>
	<script type="text/javascript">

$(document).ready(function() {
//sub category view
$('#cat').change(function(){
    var cat = $("#cat").val();
    var token = $("#_token").val();
 
$.ajax({

    url:'sub_cat_ajax',

    type:'POST',

    data:{cat:cat,_token:token},
    dataType:"text",
  success:function(response)
    {
    
   
        $("#sub_cat").html(response);


  
    }

    });
});


//sub sub category view
$('#sub_cat').change(function(){
   
    var sub_cat = $("#sub_cat").val();
    var token = $("#_token").val();
 
$.ajax({

    url:'sub_sub_cat_ajax',

    type:'POST',

    data:{sub_cat:sub_cat,_token:token},
    dataType:"text",
  success:function(response)
    {
       
    $("#sub_sub_cat").html(response);
    }

    });
});

//commission view

$('#sub_sub_cat').change(function(){
  
   var scat = $("#cat").val();
  
   var token = $("#_token").val();

$.ajax({

   url:'commission_ajax',

   type:'POST',

   data:{scat:scat,_token:token},
   dataType:"text",
 success:function(response)
   {
     
   $("#commission").html(response);
   var com = $("#com").val();
   
  $("#admin_dis1").val(com);
  $("#admin_dis2").val(com);
  $("#admin_dis3").val(com);
   }

   });
});

//size

$('#size_get').change(function(){
    var size_get = $("#size_get").val();
    var token = $("#_token").val();
     
$.ajax({

    url:'size_ajax',

    type:'POST',

    data:{size_get:size_get,_token:token},
    dataType:"text",
  success:function(response)
    {
    
    $("#size_view").html(response);
  
    }

    });
});
  
//price related

$('#p_weight').keyup(function(){
    var cat = $("#p_weight").val();
    
    var token = $("#_token").val();
   if(cat<=500)
   {
    $('#weight_view').val(35);
    $('#dc1').val(35);
    $('#dc2').val(35);
    $('#dc3').val(35);
   }
   else
   {
    $('#weight_view').val(70); 
    $('#dc1').val(70);
    $('#dc2').val(70);
    $('#dc3').val(70);
   }
   
    
});

$('#price1').keyup(function(){
    var com = $("#com").val();
   
    var dc1 = $("#dc1").val();
    var price1 = $("#price1").val();
    var token = $("#_token").val();
   
    var result1= (Number(price1)*(Number(com)/100)) ;
	var gst1= (Number(price1)*(Number(14)/100)) ;
    if(price1<=1000)
    {
        $('#cf1').val(20);   
        var vendor1= Number(price1)-Number(result1)-Number(gst1)-Number(dc1)-20;
    }
    else{
        $('#cf1').val(40); 
        var vendor1= Number(price1)-Number(result1)-Number(gst1)-Number(dc1)-40;
    }
    
	$('#gst1').val(gst1);
    $('#discount1').val(result1);
    $('#vendor1').val(vendor1);


});
$('#price2').keyup(function(){
   
    var com = $("#com").val();
    var dc2 = $("#dc2").val();
    var price2 = $("#price2").val();
    var token = $("#_token").val();
    var result2= (Number(price2)*(Number(com)/100)) ;
	var gst2= (Number(price2)*(Number(14)/100)) ;
    if(price2<=1000)
    {
        $('#cf2').val(20); 
    var vendor2= Number(price2)-Number(result2)-Number(gst2)-Number(dc2)-20;
    }
    else{
        $('#cf2').val(40); 
        var vendor2= Number(price2)-Number(result2)-Number(gst2)-Number(dc2)-40;
    }
	$('#gst2').val(gst2);
    $('#discount2').val(result2);
    $('#vendor2').val(vendor2);


});
$('#price3').keyup(function(){
    var com = $("#com").val();
    var dc3 = $("#dc3").val();
    var price3 = $("#price3").val();
    var token = $("#_token").val();
    var result3= (Number(price3)*(Number(com)/100)) ;
	var gst3= (Number(price3)*(Number(14)/100)) ;
    if(price3<=1000)
    {
        $('#cf3').val(20); 
        var vendor3= Number(price3)-Number(result3)-Number(gst3)-Number(dc3)-20;
    }
    else{
        $('#cf3').val(40); 
        var vendor3= Number(price3)-Number(result3)-Number(gst3)-Number(dc3)-40;
    }
    
	$('#gst3').val(gst3);
    $('#discount3').val(result3);
    $('#vendor3').val(vendor3);


});

});

</script>

<script>
							
							function price1(abc){
							
								
                                alert(abc);
                                
								var token = $("#_token").val();
							//alert(result1);
							$.ajax({
								   type:'POST',
								   url:'shop_ajax',
								   data:{
                                    result1:result1,
									   
									   _token : token
									  
								   },
                                   success:function(response) {
                                       
                                       $("#sub_cat").html(response);
                                      }
                                  
								});
							}
							</script>





<script type="text/javascript">

$(document).ready(function() {

$('#click').click(function(){

$.ajax({
   url:'add_product',



    type:"GET",



    data:{},



    dataType:"text",



    success:function(data)



    {

       

    $('#view').append(' <input type="text" value="" name="fetuars[]" class="form-control"  placeholder="Product Fetuars "> <br>'
										
    );


	
    }
});

a++;

});
});

</script>	





<script type="text/javascript">

$(document).ready(function() {

$('#click1').click(function(){

$.ajax({
   url:'add_product',



    type:"GET",



    data:{},



    dataType:"text",



    success:function(data)



    {

       

    $('#view1').append(' <input type="text" value="" class="form-control" name="specification1[]"  placeholder="Product specification Type"><br> <input type="text" value="" name="specification[]" class="form-control"  placeholder="Product Specification "> <br>'
										
    );


	
    }
});

a++;

});
});

</script>	
                    
                        
                        	@endsection