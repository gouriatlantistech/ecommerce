@extends('admin.layouts.menu')
@section('body')
<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Add Category</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
											<h2 class="mb-0">Add category </h2>
										</div>
										<form method="POST" class="appointment-form" id="" action="category_action" role="form" name="frm" enctype="multipart/form-data">
										<div class="card-body">
											<div class="row" id="view">
											<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>	
											<div class="col-md-6"><label>Category Name</label>
												<div class="form-group">
														<input type="text" class="form-control" name="cat_name[]" placeholder="Add Category" value="">
													</div>
													
											</div>
											<div class="col-md-6"><label>Category Background Image</label>
												<div class="form-group">
														<input type="file" class="form-control" name="cat_image[]" placeholder="" value="">
													</div>
													
											</div>
										</div>
										<a style="color:white" class="btn btn-success mt-1 mb-1" id="add_cat">+</a>
									</div>
									<center><input type='submit' name='submit' value='Final Submission' class='btn btn-primary mt-1 mb-1'></center>
								</div>

								</form>
											
								</div>
							</div>

							<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
							<script type="text/javascript">

$(document).ready(function() {

	var a=0;

$('#add_cat').click(function(){



  

$.ajax({



    url:'add_category',



    type:"GET",



    data:{},



    dataType:"text",



    success:function(data)



    {



    $('#view').append('<div class="col-md-6"><label>Category Name</label>'+
													'<div class="form-group">'+
														'<input type="text" class="form-control" name="cat_name[]" placeholder="Add Category" value="">'+
													'</div>'+
													'</div>'+

													'<div class="col-md-6"><label>Category Background Image</label>'+
												'<div class="form-group">'+
														'<input type="file" class="form-control" name="cat_image[]" placeholder="" value="">'+
													'</div>'+
													
											'</div>'+
													
											'</div>'
										
    );


	  a=a+1;
    }



    



    });







});











});

</script>	
</script>	


<script
src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous">
</script>

<script type="text/javascript">
function validateImage(id) {


var formData = new FormData();

var file = document.getElementById(id).files[0];

formData.append("Filedata", file);
var t = file.type.split('/').pop().toLowerCase();
if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
alert('Please select a valid image file');
document.getElementById(id).value = '';
return false;
}
if (file.size > 1024000) {
alert('Max Upload size is 1MB only');
document.getElementById(id).value = '';
return false;
}
return true;
}
</script>


							@endsection