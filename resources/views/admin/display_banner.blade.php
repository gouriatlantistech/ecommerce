<!-- Page content -->
@extends('admin.layouts.menu')
@section('body')
<div class="container-fluid pt-8">
							<div class="page-header mt-0 shadow p-3">
								<h3 class="mb-sm-0">Display Banner</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
								</ol>
							</div>
							 <form role="form" name="frm" method="post" enctype="multipart/form-data" action="add_display_banner">
							<div class="row">
													
								<div class="col-md-12">
									<div class="card card-profile  overflow-hidden">
										<div class="card-body text-center cover-image" data-image-src="assets/img/profile-bg.jpg">
											
												
																
												
									
											<input type="file" class="dropify" id="logo_image" onchange="validateImage(this.id);" data-height="" name="m_pic" required />
											
											
											 

											  
											
											<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>
											
											
											<p class="mb-4 text-white">Add Banner</p>
											
											<input type="submit" class="btn btn-success btn-sm" value="submit" name="submit">
										</div>
										<center>(size : 416px x 420px)</center>
										<div class="card-body">
											<div class="nav-wrapper p-0">
												<ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
													
													
													<li class="nav-item">
														<a class="nav-link mb-sm-3 mb-md-0 active show mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="true"><i class="far fa-images mr-2"></i>Banner</a>
													</li>
													
												</ul>
											</div>
										</div>
									</div>
									</form>
									<div class="card shadow ">
										<div class="card-body pb-0">
											<div class="tab-content" id="myTabContent">
												
													
																				
																				
																				
												<div class="tab-pane fade show active" id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
													<div class="row">
													@foreach($banner as $banner)
														<div class="col-lg-4 profile-image">
															<div class="card shadow"><a href="delete_display_banner?id={{$banner->dis_id}}"><i class="side-menu__icon ion-close-round"></i></a>
                                                          
																<img src="../banner/{{$banner->display_banner}}" alt="gallery">
															</div>
														</div>
														@endforeach
														
													</div>
												</div>
												
												
											</div>
										</div>
									</div>
								</div>
							</div>
							<script type="text/javascript">
function validateImage(id) {
    var formData = new FormData();
 
    var file = document.getElementById(id).files[0];
 
    formData.append("Filedata", file);
    var t = file.type.split('/').pop().toLowerCase();
    if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        alert('Please select a valid image file');
        document.getElementById(id).value = '';
        return false;
    }
    if (file.size > 1024000) {
        alert('Max Upload size is 1MB only');
        document.getElementById(id).value = '';
        return false;
    }
    return true;
}
</script>
                            @endsection