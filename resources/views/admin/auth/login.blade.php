<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta content="SSJ Jewellers Admin Dashboard" name="description">
	<meta content="Quantex Consulting Services Pvt. Ltd." name="author">

<title>Admin Dashboard : SSJ Jewellery</title>

<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">


	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800" rel="stylesheet">

	<!-- Icons -->
	<link href="admin_assets/css/icons.css" rel="stylesheet">

	<!--Bootstrap.min css-->
	<link rel="stylesheet" href="admin_assets/plugins/bootstrap/css/bootstrap.min.css">

	<!-- Adon CSS -->
	<link href="admin_assets/css/dashboard.css" rel="stylesheet" type="text/css">

	<!-- Single-page CSS -->
	<link href="admin_assets/plugins/single-page/css/main.css" rel="stylesheet" type="text/css">
</head>

<body class="bg-gradient-primary">
	<div class="page">
		<div class="page-main">
			<div class="limiter">
				<div class="container-login100">
					<div class="wrap-login100 p-5">
						<form class="login100-form validate-form " method="POST" action="{{ route('login') }}">
                        @csrf
                        <input id="email" type="hidden" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$admin->email}}" required autocomplete="email" autofocus>

                     

							<div class="text-center mb-4">
								<img src="logo/SSJ Jewellery Logo PNG.png" alt="SSJ Jewellery" class="" style="height: 50px;">
							<br><br>
								<h1>Administrator Login</h1>

								<h2></h2>
							</div>
						
                            <input type="hidden" name="role" value="ADMIN">
							<div class="wrap-input100 validate-input" data-validate = "Password is required">
								<input class="input100" type="password" name="password" required placeholder="Password">
								<span class="focus-input100"></span>
								<span class="symbol-input100">
									<i class="fa fa-lock" aria-hidden="true"></i>
								</span>
							</div>
                            @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							<div class="container-login100-form-btn">
								
                                <button type="submit" class="login100-form-btn btn-primary">
                                    {{ __('Login') }}
                                </button>
							</div>

							
                            
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Adon Scripts -->
	<!-- Core -->
	<script src="admin_assets/plugins/jquery/dist/jquery.min.js"></script>
	<script src="admin_assets/js/popper.js"></script>
	<script src="admin_assets/plugins/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>