<!-- Page content -->
@extends('admin.layouts.menu')
@section('body')
<div class="container-fluid pt-8">
							<div class="page-header mt-0 shadow p-3">
								<h3 class="mb-sm-0">Update Banner</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Update Banner</li>
								</ol>
							</div>
							


<div class="row">
@foreach($banner1 as $banner1)
	<div class="col-lg-3">
	<div class="card shadow">
	<form role="form" name="frm" method="post" enctype="multipart/form-data" action="add_promotion_banner">
		<div class="card-body">
			<input type="file" name="img_prom" class="dropify" data-default-file="../promotion_banner_image/{{$banner1->image}}" id="dsavcdsfs{{$banner1->dis_id}}" onchange="validateImage(this.id);" data-height="300" required />
			<input type="hidden" name="dis_id" value="{{$banner1->dis_id}}">
		</div>
		<center>
			@csrf
		<input class="btn btn-icon btn-default mt-1 mb-1" type="submit" value="Update">
			

		</center><br>
	</form>
	</div>
	</div>
@endforeach
	
</div>

<div class="row">
@foreach($banner2 as $banner2)
	<div class="col-lg-6">
	<div class="card shadow">
	<form role="form" name="frm" method="post" enctype="multipart/form-data" action="add_promotion_banner">
	
		<div class="card-body">
		<input type="file" name="img_prom" class="dropify" data-default-file="../promotion_banner_image/{{$banner2->image}}" id="dsavcdsfs{{$banner2->dis_id}}" onchange="validateImage(this.id);" data-height="300" required />
			<input type="hidden" name="dis_id" value="{{$banner2->dis_id}}">
		</div>
		@csrf
		<center>	<input class="btn btn-icon btn-default mt-1 mb-1" type="submit" value="Update">
		
		</center><br>
</form>
	</div>
	</div>

	@endforeach
</div>


</div>
								
							<script type="text/javascript">
function validateImage(id) {
    var formData = new FormData();
 
    var file = document.getElementById(id).files[0];
 
    formData.append("Filedata", file);
    var t = file.type.split('/').pop().toLowerCase();
    if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        alert('Please select a valid image file');
        document.getElementById(id).value = '';
        return false;
    }
    if (file.size > 1024000) {
        alert('Max Upload size is 1MB only');
        document.getElementById(id).value = '';
        return false;
    }
    return true;
}
</script>
                            @endsection