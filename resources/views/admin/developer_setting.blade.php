@extends('admin.layouts.menu')


@section('body')
<style>
.borderstyle{
    padding: 10px;
    border-radius: 15px;
    border: solid 1px;
    margin: 10px 0px;
}

</style>
<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Developer Setting</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Developer Area</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<form method="post" action="vendor_contact_mail" enctype="multipart/form-data">
									<div class="card shadow">
										<div class="card-header">
										@csrf
										<div class="card-body">
                                        @if(session()->has('success'))
                                
                                <div class="alert alert-success">
                                  <h3 style="margin: 0;"> <center> {{ session()->get('success') }}</center></h3>
                                </div>
                                
                            @endif
                            @if(session()->has('error'))
                        
                                <div class="alert alert-danger">
                                <h3 style="margin: 0;"> <center>    {{ session()->get('error') }}</center></h3>
                                    </div>
                                    
                            @endif
											<div class="row">
                                            
                                  
												<div class="row col-md-6">
                                                
                                          
													<div class="col-md-6 ">
													<center class="borderstyle">	<label class="form-label">Cache Clear</label><br>
                                                        <a href="clear-cache" value="Send Mail" class="btn btn-icon  btn-primary mt-1 mb-1">Cache Clear</a></center>
                                                        
													</div>
													<div class="col-md-6 ">
													<center class="borderstyle">	<label class="form-label">Config Clear</label><br>
                                                        <a href="config-clear" value="Send Mail" class="btn btn-icon  btn-primary mt-1 mb-1">Config Clear</a></center>
                                                        
													</div>
                                                    <div class="col-md-6 " >
													<center class="borderstyle">	<label class="form-label">Route Cache</label><br>
                                                        <a href="route-cache" value="Send Mail" class="btn btn-icon  btn-primary mt-1 mb-1">Route Cache</a></center>
                                                        
													</div>
                                                    <div class="col-md-6 ">
													<center class="borderstyle">	<label class="form-label">Route Clear</label><br>
                                                        <a href="route-clear" value="Send Mail" class="btn btn-icon  btn-primary mt-1 mb-1">Route Clear</a></center>
                                                        
													</div>
													
                                                    <div class="col-md-6 ">
													<center class="borderstyle">	<label class="form-label">View Clear</label><br>
                                                        <a href="view-clear" value="Send Mail" class="btn btn-icon  btn-primary mt-1 mb-1">View Clear</a></center>
                                                        
													</div>
                                                    
                                                    <div class="col-md-6 " >
													<center class="borderstyle">	<label class="form-label">Config Cache</label><br>
                                                        <a href="config-cache" value="Send Mail" class="btn btn-icon  btn-primary mt-1 mb-1">Config Cache</a></center>
                                                        
													</div>
												</div>
                                                
                                                </form>
                                                <div class="col-md-6">
													<center class="borderstyle" style="padding:17%">	
                                                    <i class="fa fa-download" style="    font-size: 100px;"></i><br>
                                                    
                                                    <label class="form-label" style="    font-size: 30px;">Download Database</label><br>
                                                        <a href="db_backup" value="Send Mail" class="btn btn-icon  btn-primary mt-1 mb-1">Download</a></center>
                                                        
													</div>
                                           
											</div>
										</div>
									</div>
                                    
								</div>

								</div>
                                </div>
							</div>

@endsection