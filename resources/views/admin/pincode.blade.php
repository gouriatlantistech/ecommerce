@extends('admin.layouts.menu')


@section('body')

		
<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0"></h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
											<h2 class="mb-0">All Product</h2>
										</div>
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
															<th class="wd-15p">Pin </th>
                                                         
                                                            
                                                            <th class="wd-15p">Minimum<br>Price</th>
                                                            <th class="wd-15p">Deliver<br>Charge</th>
                                                          

                                                           <th>Active/<br>Non-<br>Active</th>
															<th class="wd-20p">Auction</th>
															
														</tr>
													</thead>
													<tbody>

                                                        @foreach($pin as $pin)
                                                        <tr>
                                                        <td>{{$pin->pincode}}
														
														@php($pin_count=DB::table('pincodes')->where('pincode',$pin->pincode)->where('id','<',$pin->id)->Count())
														@if($pin_count>0)
														<br>	<span class="badge badge-danger">Duplicate</span>
														@endif
														
														</td>
                                   
                                                        
                                                        <td>{{$pin->min_price}}</td>
                                                        <td>{{$pin->delivery_charge}}</td>
                                                        <td>
                                                        @php($var=$pin->active_status)
                                                        @if($var=='YES')
                                                        <a href="pincode_status?id={{$pin->id}}&s=y" type="button" class="btn btn-primary mt-1 mb-1 bt-sm" style="width:150px">Active</a>
														
																@else
																<a href="pincode_status?id={{$pin->id}}&s=n" type="button" class="btn btn-danger mt-1 mb-1 bt-sm" style="width:150px">Non-active</a>
														
																@endif

                                                        </td>
                                                        <td><a href="update_pincode?id={{$pin->id}}" type="button" class="btn btn-info mt-1 mb-1 bt-sm">Edit</a>
														<a href="delete_pincode?id={{$pin->id}}" type="button" class="btn btn-warning mt-1 mb-1 bt-sm">Delete</a>

                                                        </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
</div>

@endsection