


           {{$text}}|malaysalaesreport|                                         <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

@if($booking_count>0)
<style>
body { background-color: #fafafa; }
.container { margin: 150px auto; }
</style>

    <table class="table" id="example">
  <thead class="thead-dark " >
  <tr>
                                                            <th class="wd-15p " >Date & <br>Time</th>
															                              <th class="wd-15p" >Order <br>ID</th>
                                                            <th class="wd-15p" >Name</th>
                                                            
                                                            <th class="wd-15p" >Billing Amount</th>
                                                           
															
														</tr>
                                                        </thead>
													<tbody>
                                                    @php($sum=0)
                                                    @foreach($booking as $booking)
													<tr>

                                                    <td>{{$booking->created_at}}</td>
                                                    <td>{{$booking->order_id}}-{{$booking->multi_id}}</td>
                                                    <td>{{$booking->name}}</td>
                                                    @php($x=($booking->product_price)*($booking->quantity1))
                                                    <td>{{$x}}</td>
@php($sum=$sum+$x)
                                                    </tr>
													@endforeach	
                                                    <tr>
                                                    <td colspan=3><b style="font-size:20px">Grant Total</b></td>
                                                    <td><b style="font-size:20px">{{$sum}}</b></td>
                                                    </tr>
													</tbody>
                                                    </table>
<p class="lead"><!--<button id="json" class="btn btn-primary">TO JSON</button>--> <a id="csv" class="btn btn-info" style="color:white">PRINT EXCEL</a>  <a id="pdf" class="btn btn-danger" style="color:white">PRINT PDF</a></p>
  </div>

@else
<br>
<h2 style="font-size: 37px;"><center>No Record Found</center></h2><br>
@endif
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.5/jspdf.plugin.autotable.min.js"></script>
<script src="https://www.jqueryscript.net/demo/export-table-json-csv-txt-pdf/src/tableHTMLExport.js"></script>

<script type="text/javascript" src="libs/jsPDF/jspdf.min.js"></script>
<script>
  $('#json').on('click',function(){
    $("#example").tableHTMLExport({type:'json',filename:'sample.json'});
  })
  $('#csv').on('click',function(){
    $("#example").tableHTMLExport({type:'csv',filename:'sales_report.csv'});
  })
  $('#pdf').on('click',function(){
    $("#example").tableHTMLExport({type:'pdf',escape:'false',filename:'sales_report.pdf'});
  })
  </script>