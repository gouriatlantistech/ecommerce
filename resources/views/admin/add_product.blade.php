
@extends('admin.layouts.menu')
@section('body')
<style>
label
{
    color:black;
}
</style>
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Add product</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Add product</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
											<h2 class="mb-0">Add Product </h2>
										</div>
										<form method="POST" class="appointment-form" id="" action="product_action" role="form" name="frm" enctype="multipart/form-data">
										<div class="card-body">
                                        <div class="row">
                                        <div class="col-md-4">
                                        <h3>Product Code</h3>
													<div class="form-group">
														<input type="text" class="form-control" name="product_code" value="{{$product_code}}" readonly  placeholder="Product Code" value="">
													</div>
													
											</div>
                                            <div class="col-md-4">
                                            <h3>HSN No</h3>
													<div class="form-group {{ $errors->has('hsn') ? 'has-error' : '' }}" >
														<input type="text" class="form-control" name="hsn"  placeholder="Product HSN no " value="{{ old('hsn') }}">
                                                                      <span class="text-danger">{{ $errors->first('hsn') }}</span>
                                                                 </div>
													
											</div>
											<div class="col-md-4">
                                            <h3>Product Name</h3>
													<div class="form-group {{ $errors->has('product_name') ? 'has-error' : '' }}">
														<input type="text" class="form-control" name="product_name"  placeholder="Product name " value="{{ old('product_name') }}">
                                                                      <span class="text-danger">{{ $errors->first('product_name') }}</span>
                                                                 </div>
													
											</div>
                                        </div>
                                       
                                        <div class="row">
                                        <div class="col-md-4">
													<div class="form-group {{ $errors->has('product_cat_id') ? 'has-error' : '' }}">
														<select class="form-control" id="cat" name="product_cat_id" >

                                                        <option disabled selected>--Select Category-- </option>
                                                        
                                                        @foreach($cat as $cat)
                                                        <option value="{{$cat->cat_id}}">{{$cat->cat_name}}</option>
                                                        @endforeach
                                                     
                                                        </select>
                                                        <span class="text-danger">{{ $errors->first('product_cat_id') }}</span>
                                                       
													</div>
													
											</div>
											<div class="col-md-4">
													<div class="form-group">
                                                    <select class="form-control" id="sub_cat" name="product_sub_cat_id" >
                                                        <option>--Select sub Category-- </option>
                                                       
                                                        </select>
													</div>
													
											</div>
                                            <div class="col-md-4">
													<div class="form-group">
                                                    <select class="form-control" id="sub_sub_cat" name="product_sub_sub_cat_id" >
                                                    <option value="0">--Select Sub Sub Category-- </option>
                                                        </select>
													</div>
													
											</div>
                                        </div>
                                        
                                        <div class="row">
                        
               
                            <div class="col-md-6">
                                    Product Brand
                                        <div class="form-group {{ $errors->has('brand') ? 'has-error' : '' }}">
                                        <select class="form-control"  name="brand" >

<option value="0"  selected >--Select Product Brand Name-- </option>

@foreach($brand as $brand)
<option value="{{$brand->brand_id}}" @if( old('brand')  == $brand->brand_id) selected   @endif > {{$brand->brand_name}}</option>
@endforeach

</select>
<span class="text-danger">{{ $errors->first('brand') }}</span>             
                                        </div></div>

                            <div class="col-md-6">
                            Upload Image (1000x1000)
                        <div class="form-group {{ $errors->has('image_name') ? 'has-error' : '' }}">
                        <input type="file" accept="image/*"  name="image_name[]" onchange="validateImage(this.id)" id="image_name" class="form-control" multiple/>
                        <span class="text-danger">{{ $errors->first('image_name') }}</span>
                       <p class="vi"><p> 
                    <p id="image_print"></p>
                    </div>
                        </div>
                        </div>
                        <div class="row">

                                      
                                        <div class="col-md-12">
                                        Product Description
                                        <div class="form-group {{ $errors->has('editor1') ? 'has-error' : '' }}">
                     
                                        <textarea class="form-control" name="editor1"  placeholder="Product Description" id="">{{ old('editor1') }}</textarea>
                                        <span class="text-danger">{{ $errors->first('editor1') }}</span>
                                        </div></div>

                                        
										
                                        </div>
                                        
                                  

                                        <div class="row">
                                       
                                       <div class="col-md-6">
                                      

                                       <div class="">  
              
                
               <div class="form-group">  
                  
                         <div class="table-responsive">  
                              <table class="table table-bordered" id="dynamic_field1">  
                                   <tr>  
                                        
                                        <td align="center"> <h3>Specification<br>Title</h3></td>
                                        <td align="center"> <h3>Product <br>Specification </h3> </td>

                                       
                                        <td><a style="color:white" name="add1" id="add" class="btn btn-success">Add More</a></td>  
                                   </tr>  
                              </table>  
                            
                         </div>  
                   
               </div>  
          </div>  






                                   
                                   
                                   </div>
                                       
                                      


                                       
                                      
                                      
                                       <div class="col-md-6">
                                      
                                       
                                       
                                       

                                       <div class="">  
               
                
               <div class="form-group">  
                  
                         <div class="table-responsive">  
                              <table class="table table-bordered" id="dynamic_field2">  
                                   <tr>  
                                        
                                     
                                        <td align="center"><h3>Product <br>Feature</h3></td>

                                       
                                        <td><a style="color:white" name="add2" id="add2" class="btn btn-success">Add More</a></td>  
                                   </tr>  
                              </table>  
                            
                         </div>  
                   
               </div>  
          </div>  


          


                                       
                                       
                                       </div>
                                       
                                       </div>




                                <!--    <div class="col-md-12">
                                            <h3>Product Size(optional)</h3>
													<div class="form-group">
                                                    <input type="text" class="form-control" name="size" placeholder="Product Size" value="">
												
													</div>
													
											</div>
                                    </div>-->

                                          <!-- <div class="row">

                                           <div class="col-md-6">
                                            <h3>Product Color Require</h3>
													<div class="form-group">
                                                    <select class="form-control" id="color_require" name="color_require">
                                                        <option disabled selected>Select</option>
                                                       <option value="yes">Yes</option>
                                                       <option value="no">No</option>
                                                       
                                                        </select>
													</div>
													
											</div>

                                            <div class="col-md-6" id="colorview">
													
                                                    
													
												</div>




                                        </div>-->
                                        
                                     <div class="row">
                                        <div class="col-md-12">
                                        <div class="form-group">  
                   
                                                    <div class="table-responsive">  
                                                            <table class="table table-bordered" id="dynamic_field">  
                                                            
                                                                <tr>  
                                                                    
                                                                    <td align="center"><h3>Product Size</h3></td>
                                                                    <td align="center"><h3>Product MRP</h3> </td>
                                                                    <td align="center"><h3>Selling Price</h3> </td>
                                                                    <td align="center"><h3>Mediator Price</h3> </td>
                                                                    <td align="center"><h3>Opening Stock</h3> </td>
                                                                    <td align="center"><h3>Add More</h3></td>  
                                                                </tr>  
                                                                <tr>  
                                                                    
                                                                    <td><input type="text" name="size[]" placeholder="Product Size" required class="form-control name_list" value="" />
                                                                    </td>
                                                                    <td><input type="number" name="mrp[]" placeholder="Product MRP" required class="form-control name_list" value="" />
                                                                   
                                                                    </td>
                                                                    <td><input type="number" name="selling_price[]" placeholder="Selling Price" required class="form-control name_list"  value="" />
                                                                   
                                                                    </td>
                                                                    <td><input type="number" name="mediator_price[]" placeholder="Mediator Price" required class="form-control name_list"  value="" />
                                                                  
                                                                    
                                                                    </td>
                                                                    <td><input type="number" name="qty[]" placeholder="Opening Stock"  required class="form-control name_list" />
                                                                   
                                                                      
                                                                    </td>
                                                                    
                                                                    <td><a  name="add1" id="add1" style="color:white" class="btn btn-success">Add More</a></td>  
                                                                </tr>  
                                                            </table>  

                                                         
                                                        
                                                    </div>  
                                                
                                            </div>
                                        </div>
                                     </div>





                                        <div class="row">
                              




                                        <div class="col-md-12">
											<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>	
												<center><input type="submit" class="btn btn-success mt-1 mb-1" value="Add Product"></center>


										</div>
                                        </div>
									
									
								
							

								</form>
											
								</div>
							</div>
               
                      
                            <script>
                        CKEDITOR.replace( 'editor1' );
                </script>
							<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
	
</script>
	<script type="text/javascript">

$(document).ready(function() {
//sub category view
$('#cat').change(function(){
    var cat = $("#cat").val();
    var token = $("#_token").val();
 
$.ajax({

    url:'sub_cat_ajax',

    type:'POST',

    data:{cat:cat,_token:token},
    dataType:"text",
  success:function(response)
    {
    
   
        $("#sub_cat").html(response);


  
    }

    });
});


//sub sub category view
$('#sub_cat').change(function(){
   
    var sub_cat = $("#sub_cat").val();
    var token = $("#_token").val();
 
$.ajax({

    url:'sub_sub_cat_ajax',

    type:'POST',

    data:{sub_cat:sub_cat,_token:token},
    dataType:"text",
  success:function(response)
    {
       
    $("#sub_sub_cat").html(response);
    }

    });
});

//commission view


});
</script>
<script>  
 $(document).ready(function(){  
      var i=1;  
      $('#add1').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'"> <td><input type="text" name="size[]" placeholder="Product Size" class="form-control name_list" /></td><td><input type="number" name="mrp[]" placeholder="Product MRP" class="form-control name_list" /></td><td><input type="number" name="selling_price[]" placeholder="Selling Price" class="form-control name_list" /></td><td><input type="number" name="mediator_price[]" placeholder="Mediator Price" class="form-control name_list" /></td> <td><input type="number" name="qty[]" placeholder="Opening Stock" class="form-control name_list" /></td><td><a style="color:white" name="remove" id="'+i+'" class="btn btn-danger btn_remove1">X</a></td></tr>');  
      });  
      $(document).on('click', '.btn_remove1', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
      $('#submit').click(function(){            
           $.ajax({  
                url:"name.php",  
                method:"POST",  
                data:$('#add_name').serialize(),  
                success:function(data)  
                {  
                     alert(data);  
                     $('#add_name')[0].reset();  
                }  
           });  
      });  
 });  
 </script>
<script type="text/javascript">
function validateImage(id) {
    var formData = new FormData();
 
    var file = document.getElementById(id).files[0];
 
    formData.append("Filedata", file);
    var t = file.type.split('/').pop().toLowerCase();
    if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
     alert('Please select a valid image file');


       // ('.vi').html('Please select a valid image file');
        document.getElementById(id).value = '';
        return false;
    }
    if (file.size > 1024000) {
     alert('Max Upload size is 1MB only');
       // ('#vi').val('Max Upload size is 1MB only');

        document.getElementById(id).value = '';
        return false;
    }
    return true;
}
</script>

<!---------    specification-->
<script>  
 $(document).ready(function(){  
      var i=1;  
      $('#add').click(function(){  
           i++;  
           $('#dynamic_field1').append('<tr id="row'+i+'"> <td><input type="text" name="specification1[]" placeholder="specification Type" class="form-control name_list" /></td>  <td><input type="text" name="specification[]" placeholder="specification" class="form-control name_list" /></td><td><a style="color:white" name="remove" id="'+i+'" class="btn btn-danger btn_remove1">X</a></td></tr>');  
      });  
      $(document).on('click', '.btn_remove1', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
      $('#submit').click(function(){            
           $.ajax({  
                url:"name.php",  
                method:"POST",  
                data:$('#add_name').serialize(),  
                success:function(data)  
                {  
                     alert(data);  
                     $('#add_name')[0].reset();  
                }  
           });  
      });  
 });  
 </script>

 <!------ end specification-->


 <!-- start feature -->

 <script>  
 $(document).ready(function(){  
      var i=1;  
      $('#add2').click(function(){  
           i++;  
           $('#dynamic_field2').append('<tr id="row1'+i+'"> <td> <input type="text" value="" class="form-control" name="fetuars[]"  placeholder="Product Feature "></td><td><a style="color:white" name="remove" id="'+i+'" class="btn btn-danger btn_remove2">X</a></td></tr>');  
      });  
      $(document).on('click', '.btn_remove2', function(){  
           var button_id = $(this).attr("id");   
           $('#row1'+button_id+'').remove();  
      });  
      $('#submit').click(function(){            
           $.ajax({  
                url:"name.php",  
                method:"POST",  
                data:$('#add_name').serialize(),  
                success:function(data)  
                {  
                     alert(data);  
                     $('#add_name')[0].reset();  
                }  
           });  
      });  
 });  
 </script>




 <!--  End feature  -->


							@endsection