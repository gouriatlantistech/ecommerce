@extends('admin.layouts.menu')
@section('body')
<style>
li {
    list-style:none;
    background-image:none;
    background-repeat:none;
    background-position:0; 
}
ul
{
    list-style-type:none;
    padding:0px;
    margin:0px;
}
li
{
 
    background-repeat:no-repeat;
    background-position:0px 5px; 
    padding-left:14px;
}
</style>
<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0" id="sales_date_show">Tax Report</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
									










                                        <div class="col-md-12"><br><br>
									<div class="">
									
									<div class="input-daterange datepicker row align-items-center">
												<div class="col-sm-6">
													<div class="form-group mb-sm-0 mb-4">
														<div class="input-group">
															<div class="input-group-prepend">
																<span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
															</div>
															<input class="form-control" placeholder="Start date" type="text" id="date1">
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group mb-0">
														<div class="input-group">
															<div class="input-group-prepend">
																<span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
															</div>
															<input class="form-control" placeholder="End date" type="text"   id="date2" >
														</div>
													</div>
												</div>
                                           
											</div>
										<br>
                             <center>   <input type="submit" class="btn btn-primary mt-1 mb-1" name="Search" id="search" onclick="tax_report();" value="Search" style="background-color:#ad59ff"> </center>  
                             <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>
                                                
									</div>
								</div>
							








                                  


										<div class="card-body">
											<div class="table-responsive">

                                            <span id="table_view" >
												<!--<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
															<th class="wd-15p">Product <br>hsn_no</th>
                                                            <th class="wd-15p">Product <br>Name</th>
                                                            <th class="wd-15p">Color</th>
                                                            <th class="wd-15p">Total<br>Stock</th>
															<th class="wd-15p">Remaining <br>Stock</th>
                                                            <th class="wd-15p">Sale <br>Stock</th>
                                                            
                                                            <th class="wd-15p">Add <br>Stock</th>
									
															
														</tr>
													</thead>
													<tbody>
													
													
                                                     
													
														
													</tbody>-->
												</span>
											
											</div>
												
										</div>
										
									</div>
								</div>
							</div>
						
							<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
							<script type="text/javascript">

function tax_report(){

//sub category view
//$('#search').click(function(){
	$("#overlay").fadeIn(300);　
    var date1 = $("#date1").val();
    var date2 = $("#date2").val();
    var token = $("#_token").val();
 
$.ajax({

    url:'tax_report_ajax',

    type:'POST',

    data:{date1:date1,date2:date2,_token:token},
    dataType:"text",
  success:function(response)
    {
    
  
		var aaaa=response.split("|malaysalaesreport|");

		$('#sales_date_show').html(aaaa[0]);
        $('#table_view').html(aaaa[1]);

        $("#overlay").fadeOut(300);　
  
  
    }

    });
//});


}

</script>
                      
@endsection
