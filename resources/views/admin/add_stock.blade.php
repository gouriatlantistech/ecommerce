@extends('admin.layouts.menu')
@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Add Stock</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
											<h2 class="mb-0"> <table>
											<tr><td style="padding-right: 30px;"><h3 class="mb-0">Product Name : <b>{{$product->product_name}}</b></h3> </td><td> <h3 class="mb-0">Product Size : <b>{{$product->size}}</b></h3></td></tr>
										</table></h2>
										</div>
									
						
										
										<form method="POST" class="appointment-form" id="" action="add_stock_code" role="form" name="frm">
										<div class="card-body">
											<div class="row" id="">
											<div class="col-md-6"><label>Transfer ID</label>
													<div class="form-group">
														<input type="text" class="form-control" name="transfer_id" placeholder="Enter Transfer ID" value="">
													</div>
										
												
													</div><div class="col-md-6"><label>Transfer Qty</label>
													<div class="form-group">
														<input type="number" class="form-control" name="transfer_qty" placeholder="Enter Transfer Qty" value="">
													</div>

													
                                                    
													
													
											</div>
										
											</div>
										
                                            <input type="hidden" name="product_price_id" id="product_price_id" value="{{$product->product_prices_id}}"/>
											<input type="hidden" name="product_id" id="product_id" value="{{$product->product_id}}"/>

											<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>
                  
									</div>
									<center><input type='submit' name='submit' value='Add Stock' class='btn btn-primary mt-1 mb-1'></center><br><br>
								</div>

								</form>
										
								</div>
							</div>	
                            </div>	

@endsection