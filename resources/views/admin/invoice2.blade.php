<!DOCTYPE html>
<html>
<head>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">

	<!-- Fonts -->
    <title>SnapcrowdRaftarShopping</title>


<!--
	<link href="../admin_assets/img/brand/raf_logo.png" rel="icon" type="image/png">


	<link href="../admin_assets/css/icons.css" rel="stylesheet">


	<link rel="stylesheet" href="../admin_assets/plugins/bootstrap/css/bootstrap.min.css">


	<link href="../admin_assets/css/dashboard.css" rel="stylesheet" type="text/css">

       


	<link href="../admin_assets/plugins/tabs/style.css" rel="stylesheet" type="text/css">


	<link href="../admin_assets/plugins/customscroll/jquery.mCustomScrollbar.css" rel="stylesheet" />

	
	<link href="../admin_assets/plugins/toggle-sidebar/css/sidemenu.css" rel="stylesheet">

	<link href="../admin_assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
	<link href="../admin_assets/plugins/datatable/responsivebootstrap4.min.css" rel="stylesheet" />
   
	 <link href="../admin_assets/plugins/sweet-alert/sweetalert.css" rel="stylesheet" />
	 <link href="../admin_assets/plugins/fileuploads/css/dropify.css" rel="stylesheet" type="text/css" />
-->

</head>
<body class="app sidebar-mini rtl" >
	<div id="global-loader" ></div>

						<!-- Top navbar-->

						<!-- Page content -->
						<div class="container-fluid pt-8">
                        <BR>
                            
                            <table class="table" style=" margin-bottom: 0px;"><tr>
                                <td style="width:25%"><b style="font-size:15px">Tax Invoice</b></td>
                                <td style="width:25%;font-size:10px">Order id: <B>{{$book->order_id}}</B><br>
                                Order Date:@php($view=$book->created_at)
								@php($vieww=explode(" ",$view))
								{{$vieww[0]}}
                                <td style="width:15%;font-size:10px">GSTIN:{{$book->gst_no}}<br>PAN: {{$book->pan_no}}</td>
                            </tr></table>
							<div class="row invoice">
								<div class="col-md-12">
									<div class="card-box card shadow">
										<table class="table" style=" margin-bottom: 10px;"><tr>
										@php($product_id=$book->product_id)
										@php($vendor=DB::table('products')->where('product_id',$product_id)->first())
										@php($id=$vendor->vendor_id)
										@php($user=DB::table('users')->where('id',$id)->first())
                                            <td style="width:40%;font-size:10px"><b>Slod By</b><br>{{$user->companey_name}},<br>{{$user->address}},<BR>GISTN:{{$user->gst_no}}</td>
                                            <td style="width:30%;font-size:10px"><B>Shipping Address</B><br>
                                           {{$book->name}},
										  @php($customer_id=$book->customer_id)
										  
										@php($shipping=DB::table('addres')->where('user_id',$customer_id)->first())
										
										   <br>{{$shipping->house_no}},{{$shipping->landmark}}<br>
                                       {{$shipping->area}},{{$shipping->city}},{{$shipping->state}}
									   <br>Pin Code - {{$shipping->pincode}}</td>
                                          
                                           <td style="width:30%;font-size:10px"><B>Billing Address</B><br>
										   @php($billing=DB::table('billing_adds')->where('user_id',$customer_id)->first())
										   {{$book->name}},
										   <br>{{$billing->house_no1}},{{$billing->landmark1}}<br>
                                       {{$billing->area1}},{{$billing->city1}},{{$billing->state1}}
									   <br>Pin Code - {{$billing->pincode1}}</td>
                                        </tr></table>
										<div class="card-body">
											<div class="mb-0">
										
												<div class="row mt-4">
													<div class="col-md-12">
														<div class="table-responsive">
															<table class="table table-bordered m-t-30 text-nowrap" style=";font-size:10px">
																<thead >
																	<tr>
																		<th>Product</th>
																		<th>Description</th>
																		<th>Qty</th>
                                                                        <th>Gross<br>Amount</th>
                                                                        
                                                                        <th>Taxable <br>Value</th>
                                                                        <th>GST</th>
																		<th class="text-right">Total</th>
																		
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>{{$vendor->product_name}}</td>
                                                                        <td>HSN:{{$vendor->hsn}}<br>
																		@php($sub_cat=$vendor->product_sub_cat_id)
																		@php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$sub_cat)->first())
																		GST:{{$gst=$sub_cat->gst}}%

                                                                        <br><b>Shipping Charge : {{$vendor->delivary_charge}}</b>
                                                                        </td>
																		<td>{{$quan=$book->quantity1}}</td>

																		@php($for_price=DB::table('product_prices')->where('product_id',$product_id)->get())
                                                         @foreach($for_price as $key=>$price)
                                                            @if($quan<3 && $key == 0)
                                                                    @php($price1=$price->product_price)
                                                            @elseif($quan<21 && $quan>2 && $key==1)
                                                                    @php($price1=$price->product_price)
                                                            @elseif($quan>20 && $key==2)
                                                                   @php($price1=$price->product_price)
                                                            @endif
                                                        @endforeach
																		<td>{{$price1}}<br><br>35</td>

																		@php($igst=$price1*($gst/100))
                                                                        <td>{{$Price33=$price1-$igst}}<br></td>
                                                                        <td>{{$igst}}<br></td>
																		<td class="text-right">{{$price1}}<br></td>
																	</tr>
																
																
																
																
																
																</tbody>
															</table>
														</div>
													</div>
												</div>
												<div class="row" >
                                                    <div class="col-xl-4 col-12 offset-xl-8" style="padding-right: 20px;">
                                                        <table class="table m-t-30 text-nowrap">
                                                            <tr style=";font-size:10px">
                                                                <td width=""><p class=" mt-3 font-weight-600"><b style="font-size:large;;font-size:12px">Total Qty :{{$quan}} </b></p></td>
                                                                <td></td>
                                                                <td><p class="text-right mt-3 font-weight-600">Sub-total:{{$price1}}
																 
																   <br><b>Grand Total :{{$price1}}</b></td>
                                                            
                                                            </tr>
                                                       
<tr><td style="font-size:10px">
                                                        Seller Registered Address :Snapcrowd Raftar Shopping Private Limited Near<br>
														shiv kirtan hall, Ward No. 8, P.O PALAMPUR -176061 ,<br>
                    Teh Palampur, Distt. Kangra ,<br> Himachal Pradesh , India, <BR>
                                            <B>Declaration : </b><br>
                                                
                                                The goods sold are intended for end user consumption and not for resale.<br>
                                                **Conditions Apply. Please refer to the product page for more details<br>E.& O.E
                                            </td>
                                            
                                            <td style="font-size:10px"><br><br><br>
                                                <center>
                                                Ordered Thpough<br>
                          <!--      <img src="http://127.0.0.1:8000/img/logo.png" height="20px">-->
									         </center>
                                            </td>
                                            
                                            
                                            <td class="text-right" >
                                             <!--    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQ4AAAC6CAMAAABoQ1NAAAAAflBMVEX///8AAAD6+vr29vbX19eIiIi6urrt7e3p6enGxsby8vL4+Pi/v7+ZmZmrq6vQ0NDi4uJ0dHTa2to9PT1ra2snJydiYmIXFxcwMDCSkpKysrKBgYGLi4tXV1dycnKpqalKSkoaGhpBQUGenp5QUFAhISFdXV0QEBA2NjYsLCxuexxpAAAH40lEQVR4nO2c23qiMBCAO3gAj4ACKtSKWIW+/wtuJkEMEsS62tAy/8V+W7B2mMwpk4S3N4IgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIoGI92A9c7Be770NEti2Ym08gHmWihWyRtTMItV0EczcLhaLp7P/Gf+rrl0sLCxYf3Qqt0MWLXPnSJpI9wiboYVW8YG4C9Vb3+hzFmTBf+bqK+a7Fo0qWYisrYKgyj4BNg/GPSaGaE0aFh9NcQ/4QoLYAF0KAxNFgA4U8Io5tJBkf7js+9A7xclhYQwOCuz/UAbgWXv4I9vPODAUQvFeSXEYKvW4Q20e9G8LiXXodKjzswSB0yTB2GbhlahEmxQ8aGT90itIk5bHSL0Ca8TlSldwNQ0w3pJDZsdYvQJly4d3LTBajqKLGj+azMsVO94yamFEhlMringdgVRlSgy6yhm8u0aoYQ6BahTXRrSbKJzZ1rD93Aor6PTAxT3SK0iCFVYBLUQC/hwVy3CC3ChkS3CG0iBVO3CC0iIleRoLWVEpRVZDxqF0sMu7jbuBYTVrpFaBNLaOFZBW0rgQM41N2a61qtHQFo2hw/glPNnWEKmnqFuFdcT1XYq1t1G2V4ykOLOjyAVFPij9VP7MQAgQ/xzwojYH/62NPxh7EfqAocYxfAc8agJcYuAdaalonVuxd2AFsH5zF1UeWVbAGWGv4sogwcDgsaU3FTQ1/9XZ823hKFN7Co/s7/M3h00cX+j5Yry7BHXRsqFBWHlUEqVmmrxhFmd22iY0/08AzIhG9NJp+afhQVxxyK7R0ReFefxlN0zd/a/4/0bOy/9buHZza7x5VllQkLY+cFfLNcF04CpovlHe5jwX8cl1oDzGpvGteRbMFS0KN/qUp27Qxs/L3Cbz1wpVshwH74ljY/KNPxjSe6jbG+EUYtVpvhANmXycz+mYXAAHblC67s845cJRsn/oxMJ03V0WQFDxvwBM/A1+0umfFD4VtWJ+0leWsnW9/mOnCYGSTSNOEkGQeLBks0JL9kMErY+D66085M8dh7zU08E+8nAO5lMjN9XPFVrgPHtGzjsnEc8vA6bZ5WJTfGt4EFH/6alBTzW4F4bYC4hDHqeZV8XHa76CqiuxdD+IB86dZvXN/fPn6Kf8jCYlS3ZP4JXBx7BYW6e6uHFa9gU7KFcQKnkon3irTSS87lx67RDfjoPeYqGzZhZP+oJwXMQ/iqBxYlcOSXzD08ccNnH2LpJ7sSkw7njGqlsBQmaTTmT/7ah8eCW8CtKlDnpDD3oTEGFxH/eQ2E4jyl/2Ckcul3qJrdKnedBRSVxgdkt790gCKm+Q/j4ex+SR0WgLHk2ys3VJh5VBvveehgedbgiodjGMFTtmB48rcEkFyHpFGe/u2LRfabXHVzCYXGFGuEe4+BGKhHPktS/4lALB5PjsxluBExr4WvPn//CuyfMMmU1xFMX+GDW+EXzCSLOYrfUJCyuiAWjj0e5C/PEcHYPGvFjBLFUBoH/Ci/4SgL9PyqyULnguW2Of88isVC/MczgqlZmDQf/2pEyDdFTeFSqB0akiyzjcEcv2vClZHtvnJLsc9q4dbD/9ebZed8OuJmfxKeu1Bah8sL8T5PKTuh5/VT+w5SO3CnLHMjbjBDqQqYNFTeEZr7khkHLx5dB8XnJo5jiTFH5EhUxzjijmTZu0HMr2VnHThK/0oxvePXLs61yfNKUSFh4QEuZIpKRszsQ1k4VzIoBVvUFnuahL9SCM0oEZl6m+cCNAKPaYR3HUvI71ZRPSkTZjdlQdTnxjmKk81zu6jOJUMkV1P4nBmu5pfSjXrcbE9Y2djnj8xHHXxubTYPjk5uEv2UX2fFFMa/VThMc118zksNC1cRnyZyHHoB/rnAYpWM2gFQEZuSAk7Fdoco75SxYTv3NG1RAhhc7PxuguVYiJ7ATGaWP86m+IS1huxjXnHTybsilGJtt3rZsYFiW8uiLmeHzHwGJW0szoUpDnNeR/NCCCcN73limEu5lUWOENUFO1sMLg/XWFQuvz8jj7av24hjn+vgaW2vwIdRVJ5LBblTieHlToz5gxUCswW+dIuPKarFKX7hiCbx5WA8vlzvz9p2OOR8KPQA+5rEOQT/qpFgibDosCf/2vNJicl8YG0K94Cl8P9e4J2DXH4DJ3yYIFXhuh0EuTlHkNRNtb7OT1IwgPhNmEaEVmGJF0y98eSjnEhZXBvCEgZ+e4/WhXmEDupLzCnPlaVLmEVtlj2OC/6kKfqHcOfZaqs0/0zuMrYWUzTWjPjG3BjXqePSFWYLh8vrCHk69RvKQuPwGw4JiUUmNkuprzDROK6WfUQ9UcRWF/y/seVyxtXg3Oxb+FBZbOIvNc2kPvJrpPtpFnwW0b858Q6hUo1YNfHyt8N3WY9ur3IBVJYdnaqG/gIe+kh4e1H+oOrsz90/uAM3xNza0LVgs8eObD62sJrcgH+zQIy68bbBN1wds9nTNjR/7a689CdiZbcHsW4xWsIQ1qwwpyMaAhY4TE/LtrdWsoY0VfcBu4j70mbjb6OuK9FNFqQNCaPS3Oo0MWlDYkDakAhJGxJ9iqISJtUbMsfrJYJOgyvmumVoDx7QHPbCRuOpofYxh2fuUf7tjKrLRx3GgWfu2P71jEgbJfp/ZCWVIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIB7kHxq1TnIMWGnNAAAAAElFTkSuQmCC" height="40px" width="75px">
<br>-->
<font style="font-size:10px">{{ strtoupper('Raftar Shopping Star')}}</fon><br>
    <font  style="margin-top:-5px;font-size:10px"> Authorized Signature</font>

                                            </td>
                                        </tr> 
                                            
                                            </table>
                                            </div>
												</div>
												<hr>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- end row -->

							<!-- Footer -->
							<footer class="footer">
								<div class="row align-items-center justify-content-xl-between">
									<div class="col-xl-6">
										
									</div>
									
								</div>
							</footer>
							<!-- Footer -->
						</div>
					</div>
				</div>
			</div>
			<!-- app-content -->
		</div>
	</div>
<!--
	<script src="../admin_assets/plugins/jquery/dist/jquery.min.js"></script>
	<script src="../admin_assets/js/popper.js"></script>
	<script src="../admin_assets/plugins/bootstrap/js/bootstrap.min.js"></script>


	<script src="../admin_assets/plugins/chart.js/dist/Chart.min.js"></script>
	<script src="../admin_assets/plugins/chart.js/dist/Chart.extension.js"></script>

	


    <script src="../admin_assets//plugins/datatable/jquery.dataTables.min.js"></script>
	<script src="../admin_assets//plugins/datatable/dataTables.bootstrap4.min.js"></script>
	<script src="../admin_assets//plugins/datatable/dataTables.responsive.min.js"></script>
	<script src="../admin_assets//plugins/datatable/responsive.bootstrap4.min.js"></script>


	<script src="../admin_assets/plugins/toggle-sidebar/js/sidemenu.js"></script>

	
    <script src="../admin_assets/plugins/fileuploads/js/dropify.min.js"></script>
	

	<script src="../admin_assets/plugins/jquery-ui/jquery-ui.min.js"></script>

	<script src="../admin_assets/plugins/customscroll/jquery.mCustomScrollbar.concat.min.js"></script>


	<script src="../admin_assets/js/custom.js"></script>
	<script src="../admin_assets/js/othercharts.js"></script>
	<script src="../admin_assets/js/datatable.js"></script>
	<script src="../admin_assets/js/dashboard-hr.js"></script> -->
</body>
</html>