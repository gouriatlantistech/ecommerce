
@extends('admin.layouts.menu')
@section('body')
<div class="container-fluid pt-8">
    <div class="page-header mt-0  p-3">
        <h3 class="mb-sm-0">Update product Price</h3>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page">Vendor Dashboard</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow">
                <form action="update_price" method="post">
                <div class="card-body">
                    <div class="row">
                    <div class="col-md-6">
                           
                           <div class="form-group">
                               <label class="form-label">Product Size<font size="2px"></font></label>
                               <input type="text"  class="form-control " name="size" placeholder="Product Size" id=""  required value=" {{$price1->size}}">
                           </div>
                          
                       </div>
                       <div class="col-md-6">
                           
                           <div class="form-group">
                               <label class="form-label">Product MRP<font size="2px">(RS)</font></label>
                               <input type="text"  class="form-control " name="mrp"required  placeholder="Product MRP" id="mrp"  value=" {{$price1->mrp}}">
                           </div>
                          
                       </div>
                       
                       <div class="col-md-6">
                           
                           <div class="form-group">
                               <label class="form-label">Selling Price<font size="2px">(RS)</font></label>
                               <input type="text"  class="form-control " required name="selling_price" placeholder="Selling Price" id=""  value="{{$price1->selling_price}}">
                           </div>
                          
                       </div>
                       <div class="col-md-6">
                           
                           <div class="form-group">
                           <label class="form-label">Mediator Price<font size="2px">(RS)</font></label>
                               <input type="text"  class="form-control " name="mediator_price" placeholder="Mediator Price" id="" value="{{round($price1->mediator_price)}}">
                           </div>
                          
                       </div>

             
                       <input type="hidden" name="product_id" value="{{$price1->product_id}}">
                
                        <input type="hidden" name="product_price_id" value="{{$price1->product_prices_id}}">
                        <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>
                    </div>
<center><input type="submit" class="btn btn-primary mt-1 mb-1"></center>

                </div>
            </form>
            </div>
           
        </div>

    </div>
    </div>
    
  

@endsection