@extends('admin.layouts.menu')
@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0"><table>
											<tr><td style="padding-right: 30px;"><h3 class="mb-0">Product Name : <b>{{$product->product_name}}</b></h3> </td><td> <h3 class="mb-0">Product Size : <b>{{$product->size}}</b></h3></td></tr>
										</table></h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
								</ol>
							</div>
                            <div class="row">
                            <div class="col-xl-4 col-md-6">
									<div class="card shadow text-center">
										<div class="card-body">
											<h3 class="mb-3">Total Stock</h3>
											<div class="chart-circle" data-value="1.0" data-thickness="10" data-color="#ad59ff"><canvas width="128" height="128"></canvas><canvas width="128" height="128"></canvas>
												<div class="chart-circle-value"><div class="text-xxl">  {{$product->total_stock}}</div></div>
											</div>
										</div>
									</div>
								</div>
                                <div class="col-xl-4 col-md-6">
									<div class="card shadow text-center">
										<div class="card-body">
											<h3 class="mb-3">Out Stock</h3>
											<div class="chart-circle" data-value="1.0" data-thickness="10" data-color="#00d9bf"><canvas width="128" height="128"></canvas><canvas width="128" height="128"></canvas>
												<div class="chart-circle-value"><div class="text-xxl">{{$product->total_stock-$product->available_stock}}</div></div>
											</div>
										</div>
									</div>
								</div>
                                <div class="col-xl-4 col-md-6">
									<div class="card shadow text-center">
										<div class="card-body">
											<h3 class="mb-3">Available Stock</h3>
											<div class="chart-circle" data-value="1.0" data-thickness="10" data-color="#00b3ff"><canvas width="128" height="128"></canvas><canvas width="128" height="128"></canvas>
												<div class="chart-circle-value"><div class="text-xxl"> {{$product->available_stock}}</div></div>
											</div>
										</div>
									</div>
								</div>
                            
                               
                                
                            </div>
							<div class="row">

								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">

                                      <h2 class="mb-0">Stock Transfer Details
									  
									  
									  <a style="float:right" href="add_stock?product_id={{$product->product_id}}&product_price_id={{$product->product_prices_id}}" class="btn btn-icon btn-sm btn-info mt-1 mb-1" style="color:white">
												<span class="btn-inner--icon"><i class="fe fe-plus "></i></span>
												<span class="btn-inner--text">Add Stoke</span>
											</a>
									  </h2>
                                        </div>
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
														
															<th class="wd-15p">Date &<br>Time</th>
                                                            <th class="wd-15p">Transfer<br>ID</th>
															<th class="wd-15p">Remarks</th>
															<th class="wd-15p">Transfer Type<br></th>
															<th class="wd-15p">Transfer Qty</th>
                                                  
                                                            <th class="wd-15p">Available Stock</th>
														
															
														</tr>
													</thead>
													<tbody>
													
													@foreach($stoke_trans as $stoke_trans)
													<tr>
                                                            <td>{{$stoke_trans->created_at}}</td>
                                                            <td>@if($stoke_trans->transfer_id!=0){{$stoke_trans->transfer_id}}@else -- @endif</td>
															<td>{{$stoke_trans->remarks}}</td>
															<td>{{$stoke_trans->type}}<br></td>
															<td>
															@if($stoke_trans->type=='CREDIT')
															
															<span style="color:green;font-size:20px;font-weight:bold">+{{$stoke_trans->quantity}}</span>
															@else
															<span style="color:red;font-size:20px;font-weight:bold">-{{$stoke_trans->quantity}}</span>
															@endif
															
															</td>
                                       
                                                            <td>{{$stoke_trans->total_quantity}}</td>
														
										        	</tr>
                                                    @endforeach
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
						    <script>
							$(document).ready(function() {
							      

								  $('#example').DataTable( {
									   "order": [[ 0, "desc" ]]
								   } );
								   
							   });
														   </script>
														   
@endsection