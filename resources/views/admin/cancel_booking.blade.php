@extends('admin.layouts.menu')
@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Pending Booking</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Pending Booking</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
											<h2 class="mb-0">Pending Booking</h2>
										</div>
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
                                                        <th class="wd-15p">Date & <br>Time</th>
															<th class="wd-15p">Order <br>Id</th>
															
                                                            
                                                            
                                                            <th class="wd-15p">Customer <br>name</th>
															<th class="wd-15p">Email <br>Address</th>
                                                            <th class="wd-15p">Payble <br>Price</th>
															<th class="wd-15p">More <br>Details</th>
															
															
														</tr>
													</thead>
													<tbody>
												
												

@foreach($booking as $booking)
														<tr>
                                                        <td>{{$booking->created_at}}</td>
                                                        <td>{{$booking->order_id}}</td>
                                                        <td>{{$booking->name}}@if($booking->usertype=='MEDIATOR') <span class="badge badge-info">MEDIATOR</span> @endif</td>
                                                     
                                                   
                                                        <td>{{$booking->email}}</td>
														
                                                        <td>{{$booking->total_price}}@if($booking->payment_type=='online') <span class="badge badge-primary">Online</span> @elseif($booking->payment_type=='cash') <span class="badge badge-info">Cash</span> @else <span class="badge badge-warning">Wallet</span> @endif</td>
                                                    
														<td>
                                                      
                                                        
                                                        <a href="view_booking_details?booking_id={{$booking->booking_id}}&cancel={{csrf_token()}}" class="btn btn-icon btn-sm btn-primary mt-1 mb-1" type="button">
                                                            <span class="btn-inner--icon"><i class="fe fe-eye"></i></span>
                                                            <span class="btn-inner--text">View Details</span>
                                                        </a>
							
                                                        </tr>
														
@endforeach
													
													</tbody>
												</table>
											
											</div>
												
										</div>
										
									</div>
								</div>
							</div>
							<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
							<script type="text/javascript">
 $(document).ready(function() {
							      

                                  $('#example').DataTable( {
                                       "order": [[ 0, "desc" ]]
                                   } );
                                   
                               });
                                                           </script>
                               

</script>

@endsection