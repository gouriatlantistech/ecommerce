@extends('layouts.menu')

@section('title')
India's Most Popular Jewellery Shopping Website : SSJ Jewellery
@endsection

@section('content')
<style>
@media (max-width: 768px) {
  .mobile-none {
   display:none;
  }
}

</style>

 <!-- ========== MAIN CONTENT ========== -->
 <main id="content" role="main">
            <!-- Slider Section -->
            <div class="mb-5">
                <div class="bg-img-hero" style="background-image: url(assets/img/1920X422/img1.jpg);">
                    <div class="container min-height-420 overflow-hidden">
                        <div class="js-slick-carousel u-slick"
                            data-pagi-classes="text-center position-absolute right-0 bottom-0 left-0 u-slick__pagination u-slick__pagination--long justify-content-start mb-3 mb-md-4 offset-xl-3 pl-2 pb-1">
                        
                            @php($display_banners1=DB::table('coupon_banners')
                            ->Orderby('active_status','desc')
                            ->get())
                            @foreach($display_banners1 as $banner)

                            <div class="js-slide bg-img-hero-center">
                                <div class="row min-height-420 py-7 py-md-0">
                                    <div class="offset-xl-3 col-xl-4 col-6 mt-md-8">
                                        <h2 class="font-size-72 text-lh-57 font-weight-light"
                                            data-scs-animation-in="fadeInUp">
                                            {{$banner->title}}
                                         
                                        </h2>
                                        <h6 class="font-size-15 font-weight-bold mb-3 mobile-none"
                                            data-scs-animation-in="fadeInUp"
                                            data-scs-animation-delay="200">Coupon Code : {{$banner->coupon_code}} 
                                        </h6>
                                        <div class="mb-4"
                                            data-scs-animation-in="fadeInUp"
                                            data-scs-animation-delay="300" style="    margin-bottom: 0.5rem !important;">
                                            <span class="font-size-13">ORDER FROM</span>
                                            <div class="font-size-50 font-weight-bold text-lh-45">
                                                <sup class="" style="    font-size: 50%;">&#8377;</sup>{{$banner->min_price}}<sup class=""> Get @if($banner->coupon_type=='FLAT') &#8377;{{$banner->coupon_value}} FLAT Discount @else {{$banner->coupon_value}}% Discount @endif</sup>
                                            </div>
                                        </div>
                                        @if($banner->cat_id!=0)
                                        <h6 class="font-size-15 font-weight-bold mb-3 mobile-none"
                                            data-scs-animation-in="fadeInUp"
                                            data-scs-animation-delay="200">@php($cat=DB::table('cats')->where('cat_id',$banner->cat_id)->first())
														On {{$cat->cat_name}} Category Products
                                                        
                                                       
                                        </h6>
                                        <a href="/shop?cat_id={{$banner->cat_id}}" class="btn btn-primary transition-3d-hover rounded-lg font-weight-normal py-2 px-md-7 px-3 font-size-16"
                                            data-scs-animation-in="fadeInUp"
                                            data-scs-animation-delay="400">
                                            Start Buying
                                        </a>
                                        @else
                                        <h6 class="font-size-15 font-weight-bold mb-3 mobile-none"
                                            data-scs-animation-in="fadeInUp"
                                            data-scs-animation-delay="200">
														On all Products
                                                        
                                                       
                                        </h6>
                                         <a href="/shop" class="btn btn-primary transition-3d-hover rounded-lg font-weight-normal py-2 px-md-7 px-3 font-size-16"
                                            data-scs-animation-in="fadeInUp"
                                            data-scs-animation-delay="400">
                                            Start Buying
                                        </a>
                                        @endif
                                       
                                    </div>
                                    <div class="col-xl-5 col-6  d-flex align-items-center"
                                        data-scs-animation-in="zoomIn"
                                        data-scs-animation-delay="500">
                                        <img class="img-fluid" src="/coupon_image/{{$banner->coupon_image}}" alt="{{$banner->title}}">
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>

                
            </div>

            <!-- End Slider Section -->
            <div class="container">
                <!-- Banner -->
                <div class="mb-5">
                    <div class="row">
                    @php($banner1=DB::table('promotion_banners')->limit(4)->get())
                @foreach($banner1 as $banner1)
                        <div class="col-md-6 mb-4 mb-xl-0 col-xl-3">
                            <a href="/shop" class="d-black text-gray-90">
                                <div class="min-height-132 py-1 d-flex bg-gray-1 align-items-center">
                                
                                        <img class="img-fluid" src="/promotion_banner_image/{{$banner1->image}}" alt="Image Description">
                                    
                                </div>
                            </a>
                        </div>
                        @endforeach
                   
                        
                    </div>
                </div>
                <!-- End Banner -->
                <!-- Deals-and-tabs -->
                <div class="container">
         
         <!-- Recently viewed -->
         <div class="mb-6">
             <div class="position-relative">
                 <div class="border-bottom border-color-1 mb-2">
                     <h3 class="section-title mb-0 pb-2 font-size-22">New Arrival</h3>
                 </div>
                 <div class="js-slick-carousel u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1"
                     data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0"
                     data-slides-show="7"
                     data-slides-scroll="1"
                     data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                     data-arrow-left-classes="fa fa-angle-left right-1"
                     data-arrow-right-classes="fa fa-angle-right right-0"
                     data-responsive='[{
                       "breakpoint": 1400,
                       "settings": {
                         "slidesToShow": 6
                       }
                     }, {
                         "breakpoint": 1200,
                         "settings": {
                           "slidesToShow": 4
                         }
                     }, {
                       "breakpoint": 992,
                       "settings": {
                         "slidesToShow": 3
                       }
                     }, {
                       "breakpoint": 768,
                       "settings": {
                         "slidesToShow": 2
                       }
                     }, {
                       "breakpoint": 554,
                       "settings": {
                         "slidesToShow": 2
                       }
                     }]'>

                     @php($newarrivel=DB::table('products')
                     ->orderby('product_id','desc')
                     ->where('products.active_status','YES')
                     ->limit(10)
                     ->get())
                                        @foreach($newarrivel as $proa)
                                  @php($view_product_id=$proa->product_id)
                                  @php($product_price=DB::table('product_prices')->where('product_id',$view_product_id)->first())
                                     @php($mediator_price=$product_price->mediator_price)
                                     @php($selling_price=$product_price->selling_price)
                                     @php($product_image=DB::table('product_images')->where('product_id',$view_product_id)->limit(1)->get())
                                                @php($brand_name=DB::table('brands')->where('brand_id',$proa->brand_id)->first())
                                 
                     <div class="js-slide products-group">
                         <div class="product-item">
                             <div class="product-item__outer h-100">
                                 <div class="product-item__inner px-wd-4 p-2 p-md-3">
                                     <div class="product-item__body pb-xl-2">
                                         <div class="mb-2"><a href="#" class="font-size-12 text-gray-5">{{$brand_name->brand_name}}</a></div>
                                         <h5 class="mb-1 product-item__title"><a href="../shop/single-product-fullwidth.html" class="text-blue font-weight-bold">{{$proa->product_name}}</a></h5>
                                         <div class="mb-2">
                                         @foreach($product_image as $product_image)
                                                        <a href="/product_details?id={{$view_product_id}}" class="d-block text-center"><img class="img-fluid" src="/small_product_image/{{$product_image->image}}" alt="{{$proa->product_name}}"></a>
                                                   @endforeach
                                              </div>
                                         <div class="flex-center-between mb-1">
                                             <div class="prodcut-price">
                                                 <div class="text-gray-100"> @guest
                                                                &#8377;{{$selling_price}}
                                                            @else
                                                               @if(Auth::user()->usertype=='MEDIATOR')
                                                               &#8377;{{$mediator_price}}
                                                               @else
                                                               &#8377;{{$selling_price}}
                                                               @endif
                                                            @endguest
                                                    </div>
                                            </div>
                                             <div class="d-none d-xl-block prodcut-add-cart">
                                               
                                             @php($a=(int)$proa->review)
                                                    @for($i=0;$i<$a;$i++)
                                                    <small class="fas fa-star"></small>
                                                    @endfor
                                                 

                                                     @for($i=0;$i<5-$a;$i++)
                                                     <small class="far fa-star text-muted"></small>
                                                     @endfor
                                            </div>
                                         </div>
                                     </div>
                                     <div class="product-item__footer">
                                         <div class="border-top pt-2 flex-center-between flex-wrap">
                                         <a href="javascript:void(0)" onclick="cart({{$proa->product_id}},{{$product_price->product_prices_id}})" class="text-gray-6 font-size-13"><i class="ec ec-add-to-cart mr-1 font-size-15"></i> Cart</a>
                                                        <a href="javascript:void(0)" onclick="wishlist({{$proa->product_id}},{{$product_price->product_prices_id}})" class="text-gray-6 font-size-13"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 @endforeach
                 </div>
             </div>
         </div>
      
     </div>


     <div class="container">
         
         <!-- Recently viewed -->
         <div class="mb-6">
             <div class="position-relative">
                 <div class="border-bottom border-color-1 mb-2">
                     <h3 class="section-title mb-0 pb-2 font-size-22">Best Seller</h3>
                 </div>
                 <div class="js-slick-carousel u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1"
                     data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0"
                     data-slides-show="7"
                     data-slides-scroll="1"
                     data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                     data-arrow-left-classes="fa fa-angle-left right-1"
                     data-arrow-right-classes="fa fa-angle-right right-0"
                     data-responsive='[{
                       "breakpoint": 1400,
                       "settings": {
                         "slidesToShow": 6
                       }
                     }, {
                         "breakpoint": 1200,
                         "settings": {
                           "slidesToShow": 4
                         }
                     }, {
                       "breakpoint": 992,
                       "settings": {
                         "slidesToShow": 3
                       }
                     }, {
                       "breakpoint": 768,
                       "settings": {
                         "slidesToShow": 2
                       }
                     }, {
                       "breakpoint": 554,
                       "settings": {
                         "slidesToShow": 2
                       }
                     }]'>

                     @php($newarrivel=DB::table('products')
                     ->orderby('product_id','desc')
                     ->where('products.active_status','YES')
                     ->limit(10)
                     ->get())
                                        @foreach($newarrivel as $proa)
                                  @php($view_product_id=$proa->product_id)
                                  @php($product_price=DB::table('product_prices')->where('product_id',$view_product_id)->first())
                                     @php($mediator_price=$product_price->mediator_price)
                                     @php($selling_price=$product_price->selling_price)
                                     @php($product_image=DB::table('product_images')->where('product_id',$view_product_id)->limit(1)->get())
                                                @php($brand_name=DB::table('brands')->where('brand_id',$proa->brand_id)->first())
                                 
                     <div class="js-slide products-group">
                         <div class="product-item">
                             <div class="product-item__outer h-100">
                                 <div class="product-item__inner px-wd-4 p-2 p-md-3">
                                     <div class="product-item__body pb-xl-2">
                                         <div class="mb-2"><a href="#" class="font-size-12 text-gray-5">{{$brand_name->brand_name}}</a></div>
                                         <h5 class="mb-1 product-item__title"><a href="../shop/single-product-fullwidth.html" class="text-blue font-weight-bold">{{$proa->product_name}}</a></h5>
                                         <div class="mb-2">
                                         @foreach($product_image as $product_image)
                                                        <a href="/product_details?id={{$view_product_id}}" class="d-block text-center"><img class="img-fluid" src="/small_product_image/{{$product_image->image}}" alt="{{$proa->product_name}}"></a>
                                                   @endforeach
                                              </div>
                                         <div class="flex-center-between mb-1">
                                             <div class="prodcut-price">
                                                 <div class="text-gray-100"> @guest
                                                                &#8377;{{$selling_price}}
                                                            @else
                                                               @if(Auth::user()->usertype=='MEDIATOR')
                                                               &#8377;{{$mediator_price}}
                                                               @else
                                                               &#8377;{{$selling_price}}
                                                               @endif
                                                            @endguest
                                                    </div>
                                            </div>
                                             <div class="d-none d-xl-block prodcut-add-cart">
                                               
                                             @php($a=(int)$proa->review)
                                                    @for($i=0;$i<$a;$i++)
                                                    <small class="fas fa-star"></small>
                                                    @endfor
                                                 

                                                     @for($i=0;$i<5-$a;$i++)
                                                     <small class="far fa-star text-muted"></small>
                                                     @endfor
                                            </div>
                                         </div>
                                     </div>
                                     <div class="product-item__footer">
                                         <div class="border-top pt-2 flex-center-between flex-wrap">
                                         <a href="javascript:void(0)" onclick="cart({{$proa->product_id}},{{$product_price->product_prices_id}})" class="text-gray-6 font-size-13"><i class="ec ec-add-to-cart mr-1 font-size-15"></i> Cart</a>
                                                        <a href="javascript:void(0)" onclick="wishlist({{$proa->product_id}},{{$product_price->product_prices_id}})" class="text-gray-6 font-size-13"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 @endforeach
                 </div>
             </div>
         </div>
      
     </div>
 


     <div class="container">
         
         <!-- Recently viewed -->
         <div class="mb-6">
             <div class="position-relative">
                 <div class="border-bottom border-color-1 mb-2">
                     <h3 class="section-title mb-0 pb-2 font-size-22">Top Retted Products</h3>
                 </div>
                 <div class="js-slick-carousel u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1"
                     data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0"
                     data-slides-show="7"
                     data-slides-scroll="1"
                     data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                     data-arrow-left-classes="fa fa-angle-left right-1"
                     data-arrow-right-classes="fa fa-angle-right right-0"
                     data-responsive='[{
                       "breakpoint": 1400,
                       "settings": {
                         "slidesToShow": 6
                       }
                     }, {
                         "breakpoint": 1200,
                         "settings": {
                           "slidesToShow": 4
                         }
                     }, {
                       "breakpoint": 992,
                       "settings": {
                         "slidesToShow": 3
                       }
                     }, {
                       "breakpoint": 768,
                       "settings": {
                         "slidesToShow": 2
                       }
                     }, {
                       "breakpoint": 554,
                       "settings": {
                         "slidesToShow": 2
                       }
                     }]'>

                     @php($newarrivel=DB::table('products')
                     ->orderby('review','desc')
                     ->where('products.active_status','YES')
                     ->limit(10)
                     ->get())
                                        @foreach($newarrivel as $proa)
                                  @php($view_product_id=$proa->product_id)
                                  @php($product_price=DB::table('product_prices')->where('product_id',$view_product_id)->first())
                                     @php($mediator_price=$product_price->mediator_price)
                                     @php($selling_price=$product_price->selling_price)
                                     @php($product_image=DB::table('product_images')->where('product_id',$view_product_id)->limit(1)->get())
                                                @php($brand_name=DB::table('brands')->where('brand_id',$proa->brand_id)->first())
                                 
                     <div class="js-slide products-group">
                         <div class="product-item">
                             <div class="product-item__outer h-100">
                                 <div class="product-item__inner px-wd-4 p-2 p-md-3">
                                     <div class="product-item__body pb-xl-2">
                                         <div class="mb-2"><a href="#" class="font-size-12 text-gray-5">{{$brand_name->brand_name}}</a></div>
                                         <h5 class="mb-1 product-item__title"><a href="../shop/single-product-fullwidth.html" class="text-blue font-weight-bold">{{$proa->product_name}}</a></h5>
                                         <div class="mb-2">
                                         @foreach($product_image as $product_image)
                                                        <a href="/product_details?id={{$view_product_id}}" class="d-block text-center"><img class="img-fluid" src="/small_product_image/{{$product_image->image}}" alt="{{$proa->product_name}}"></a>
                                                   @endforeach
                                              </div>
                                         <div class="flex-center-between mb-1">
                                             <div class="prodcut-price">
                                                 <div class="text-gray-100"> @guest
                                                                &#8377;{{$selling_price}}
                                                            @else
                                                               @if(Auth::user()->usertype=='MEDIATOR')
                                                               &#8377;{{$mediator_price}}
                                                               @else
                                                               &#8377;{{$selling_price}}
                                                               @endif
                                                            @endguest
                                                    </div>
                                            </div>
                                             <div class="d-none d-xl-block prodcut-add-cart">
                                               
                                             @php($a=(int)$proa->review)
                                                    @for($i=0;$i<$a;$i++)
                                                    <small class="fas fa-star"></small>
                                                    @endfor
                                                 

                                                     @for($i=0;$i<5-$a;$i++)
                                                     <small class="far fa-star text-muted"></small>
                                                     @endfor
                                            </div>
                                         </div>
                                     </div>
                                     <div class="product-item__footer">
                                         <div class="border-top pt-2 flex-center-between flex-wrap">
                                         <a href="javascript:void(0)" onclick="cart({{$proa->product_id}},{{$product_price->product_prices_id}})" class="text-gray-6 font-size-13"><i class="ec ec-add-to-cart mr-1 font-size-15"></i> Cart</a>
                                                        <a href="javascript:void(0)" onclick="wishlist({{$proa->product_id}},{{$product_price->product_prices_id}})" class="text-gray-6 font-size-13"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 @endforeach
                 </div>
             </div>
         </div>
      
     </div>
 
     <div class="container">
         
         <!-- Recently viewed -->
         <div class="mb-6">
             <div class="position-relative">
                 <div class="border-bottom border-color-1 mb-2">
                     <h3 class="section-title mb-0 pb-2 font-size-22">Most Viewed Product</h3>
                 </div>
                 <div class="js-slick-carousel u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1"
                     data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0"
                     data-slides-show="7"
                     data-slides-scroll="1"
                     data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                     data-arrow-left-classes="fa fa-angle-left right-1"
                     data-arrow-right-classes="fa fa-angle-right right-0"
                     data-responsive='[{
                       "breakpoint": 1400,
                       "settings": {
                         "slidesToShow": 6
                       }
                     }, {
                         "breakpoint": 1200,
                         "settings": {
                           "slidesToShow": 4
                         }
                     }, {
                       "breakpoint": 992,
                       "settings": {
                         "slidesToShow": 3
                       }
                     }, {
                       "breakpoint": 768,
                       "settings": {
                         "slidesToShow": 2
                       }
                     }, {
                       "breakpoint": 554,
                       "settings": {
                         "slidesToShow": 2
                       }
                     }]'>

                     @php($newarrivel=DB::table('products')
                     ->where('products.active_status','YES')
                     ->orderby('view_count','desc')
                     ->limit(10)
                     ->get())
                                        @foreach($newarrivel as $proa)
                                  @php($view_product_id=$proa->product_id)
                                  @php($product_price=DB::table('product_prices')->where('product_id',$view_product_id)->first())
                                     @php($mediator_price=$product_price->mediator_price)
                                     @php($selling_price=$product_price->selling_price)
                                     @php($product_image=DB::table('product_images')->where('product_id',$view_product_id)->limit(1)->get())
                                                @php($brand_name=DB::table('brands')->where('brand_id',$proa->brand_id)->first())
                                 
                     <div class="js-slide products-group">
                         <div class="product-item">
                             <div class="product-item__outer h-100">
                                 <div class="product-item__inner px-wd-4 p-2 p-md-3">
                                     <div class="product-item__body pb-xl-2">
                                         <div class="mb-2"><a href="#" class="font-size-12 text-gray-5">{{$brand_name->brand_name}}</a></div>
                                         <h5 class="mb-1 product-item__title"><a href="../shop/single-product-fullwidth.html" class="text-blue font-weight-bold">{{$proa->product_name}}</a></h5>
                                         <div class="mb-2">
                                         @foreach($product_image as $product_image)
                                                        <a href="/product_details?id={{$view_product_id}}" class="d-block text-center"><img class="img-fluid" src="/small_product_image/{{$product_image->image}}" alt="{{$proa->product_name}}"></a>
                                                   @endforeach
                                              </div>
                                         <div class="flex-center-between mb-1">
                                             <div class="prodcut-price">
                                                 <div class="text-gray-100"> @guest
                                                                &#8377;{{$selling_price}}
                                                            @else
                                                               @if(Auth::user()->usertype=='MEDIATOR')
                                                               &#8377;{{$mediator_price}}
                                                               @else
                                                               &#8377;{{$selling_price}}
                                                               @endif
                                                            @endguest
                                                    </div>
                                            </div>
                                             <div class="d-none d-xl-block prodcut-add-cart">
                                               
                                             @php($a=(int)$proa->review)
                                                    @for($i=0;$i<$a;$i++)
                                                    <small class="fas fa-star"></small>
                                                    @endfor
                                                 

                                                     @for($i=0;$i<5-$a;$i++)
                                                     <small class="far fa-star text-muted"></small>
                                                     @endfor
                                            </div>
                                         </div>
                                     </div>
                                     <div class="product-item__footer">
                                         <div class="border-top pt-2 flex-center-between flex-wrap">
                                         <a href="javascript:void(0)" onclick="cart({{$proa->product_id}},{{$product_price->product_prices_id}})" class="text-gray-6 font-size-13"><i class="ec ec-add-to-cart mr-1 font-size-15"></i> Cart</a>
                                                        <a href="javascript:void(0)" onclick="wishlist({{$proa->product_id}},{{$product_price->product_prices_id}})" class="text-gray-6 font-size-13"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 @endforeach
                 </div>
             </div>
         </div>
      
     </div>
 


             
            
            <!-- End Products-4-1-4 -->
            <div class="container">
         
                <!-- Recently viewed -->
                @if(session()->has('product_id'))
                @php($value = session()->get('product_id'))
    
    @php($prduct=array_reverse($value))
    @php($i=0)

    @php($product=DB::table('products')
                            ->where('products.product_id',$recently_view_product_id)
                            ->where('products.active_status','YES')
                            ->count())

                            @if($product!=0)

    @foreach($prduct as $key=>$recently_view_product_id)
                <div class="mb-6">
                    <div class="position-relative">
                        <div class="border-bottom border-color-1 mb-2">
                            <h3 class="section-title mb-0 pb-2 font-size-22">Recently Viewed Product</h3>
                        </div>
                        <div class="js-slick-carousel u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1"
                            data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0"
                            data-slides-show="7"
                            data-slides-scroll="1"
                            data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                            data-arrow-left-classes="fa fa-angle-left right-1"
                            data-arrow-right-classes="fa fa-angle-right right-0"
                            data-responsive='[{
                            "breakpoint": 1400,
                            "settings": {
                                "slidesToShow": 6
                            }
                            }, {
                                "breakpoint": 1200,
                                "settings": {
                                "slidesToShow": 4
                                }
                            }, {
                            "breakpoint": 992,
                            "settings": {
                                "slidesToShow": 3
                            }
                            }, {
                            "breakpoint": 768,
                            "settings": {
                                "slidesToShow": 2
                            }
                            }, {
                            "breakpoint": 554,
                            "settings": {
                                "slidesToShow": 2
                            }
                            }]'>

                            @php($proa=DB::table('products')
                            ->where('products.product_id',$recently_view_product_id)
                            ->where('products.active_status','YES')
                            ->first())
                                   
                                        @php($view_product_id=$proa->product_id)
                                        @php($product_price=DB::table('product_prices')->where('product_id',$view_product_id)->first())
                                            @php($mediator_price=$product_price->mediator_price)
                                            @php($selling_price=$product_price->selling_price)
                                            @php($product_image=DB::table('product_images')->where('product_id',$view_product_id)->limit(1)->get())
                                                        @php($brand_name=DB::table('brands')->where('brand_id',$proa->brand_id)->first())
                                        
                            <div class="js-slide products-group">
                                <div class="product-item">
                                    <div class="product-item__outer h-100">
                                        <div class="product-item__inner px-wd-4 p-2 p-md-3">
                                            <div class="product-item__body pb-xl-2">
                                                <div class="mb-2"><a href="#" class="font-size-12 text-gray-5">{{$brand_name->brand_name}}</a></div>
                                                <h5 class="mb-1 product-item__title"><a href="../shop/single-product-fullwidth.html" class="text-blue font-weight-bold">{{$proa->product_name}}</a></h5>
                                                <div class="mb-2">
                                                @foreach($product_image as $product_image)
                                                                <a href="/product_details?id={{$view_product_id}}" class="d-block text-center"><img class="img-fluid" src="/small_product_image/{{$product_image->image}}" alt="{{$proa->product_name}}"></a>
                                                        @endforeach
                                                    </div>
                                                <div class="flex-center-between mb-1">
                                                    <div class="prodcut-price">
                                                        <div class="text-gray-100"> @guest
                                                                        &#8377;{{$selling_price}}
                                                                    @else
                                                                    @if(Auth::user()->usertype=='MEDIATOR')
                                                                    &#8377;{{$mediator_price}}
                                                                    @else
                                                                    &#8377;{{$selling_price}}
                                                                    @endif
                                                                    @endguest
                                                            </div>
                                                    </div>
                                                    <div class="d-none d-xl-block prodcut-add-cart">
                                                    
                                                    @php($a=(int)$proa->review)
                                                            @for($i=0;$i<$a;$i++)
                                                            <small class="fas fa-star"></small>
                                                            @endfor
                                                        

                                                            @for($i=0;$i<5-$a;$i++)
                                                            <small class="far fa-star text-muted"></small>
                                                            @endfor
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-item__footer">
                                                <div class="border-top pt-2 flex-center-between flex-wrap">
                                                <a href="javascript:void(0)" onclick="cart({{$proa->product_id}},{{$product_price->product_prices_id}})" class="text-gray-6 font-size-13"><i class="ec ec-add-to-cart mr-1 font-size-15"></i> Cart</a>
                                                                <a href="javascript:void(0)" onclick="wishlist({{$proa->product_id}},{{$product_price->product_prices_id}})" class="text-gray-6 font-size-13"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                      
                        </div>
                    </div>
                 </div>
          @endforeach
          @endif
            @endif
            
            
            
                <!-- End Recently viewed -->
                <!-- Brand Carousel -->
                <div class="mb-8">
                    <div class="py-2 border-top border-bottom">
                        <div class="js-slick-carousel u-slick my-1"
                            data-slides-show="5"
                            data-slides-scroll="1"
                            data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-normal u-slick__arrow-centered--y"
                            data-arrow-left-classes="fa fa-angle-left u-slick__arrow-classic-inner--left z-index-9"
                            data-arrow-right-classes="fa fa-angle-right u-slick__arrow-classic-inner--right"
                            data-responsive='[{
                                "breakpoint": 992,
                                "settings": {
                                    "slidesToShow": 2
                                }
                            }, {
                                "breakpoint": 768,
                                "settings": {
                                    "slidesToShow": 1
                                }
                            }, {
                                "breakpoint": 554,
                                "settings": {
                                    "slidesToShow": 1
                                }
                            }]'>
                            @php($brands=DB::table('brands')
                            ->get())
@foreach($brands as $brands)
                            <div class="js-slide">
                                <a href="#" class="link-hover__brand">
                                    <img class="img-fluid m-auto max-height-50" src="brand_logo/{{$brands->brand_image}}" title="{{$brands->brand_name}}" alt="{{$brands->brand_name}}">
                                </a>
                            </div>

         @endforeach             
                        
                        </div>
                    </div>
                </div>
                <!-- End Brand Carousel -->
            </div>




        </main>
        <!-- ========== END MAIN CONTENT ========== -->

	 
	 


@endsection