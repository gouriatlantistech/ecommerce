<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use DB;
use App\billing_add;
use App\addre;
use App\Rules\MatchOldPassword;

use GuzzleHttp\Client;
use App\user_wallet_transaction;
use App\user_wallet;
use Razorpay\Api\Api;
use Session;
use carbon;
use App\booking;
use App\refer_code;
use App\book_multi_item;
use App\stock_trasanction;
use App\cancel_reasons;
use App\review;
use Illuminate\Support\Facades\Mail;

class AfterLoginController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user_middleware');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function smsRequest($phone,$msg)
    {
        $client = new \GuzzleHttp\Client();
        $url = "http://weberleads.in/http-tokenkeyapi.php?authentic-key=3638417576657261613637381591103701&senderid=AUVERA&route=2&number=91".$phone."&message=".$msg;
        $request = $client->get($url);
        $response = $request->getBody();
    }
    public function checkout(Request $ss)
    {
        $user_id=Auth::user()->id;
        $phone=Auth::user()->phone_no;
        $name=Auth::user()->name;
        $to_mail=Auth::user()->email;
        $product_id=$ss->id;
        $address=DB::table('addres')->where('user_id',$user_id)->get();
        $address2=DB::table('addres')->where('user_id',$user_id)->count();
       
       if($product_id=="")
       {
            $product_id=0; 
            $cart=DB::table('carts')->join('products','carts.product_id','=','products.product_id')->join('sub_sub_cats','products.sub_sub_cat_id','=','sub_sub_cats.sub_sub_cat_id')->join('sub_cats','sub_sub_cats.sub_cat_id','=','sub_cats.sub_cat_id')->join('cats','sub_cats.cat_id','=','cats.cat_id')->join('users','carts.user_id','=','users.id')->where('carts.user_id',$user_id)->get();
            $cod_count=DB::table('carts')->join('products','carts.product_id','=','products.product_id')->join('sub_sub_cats','products.sub_sub_cat_id','=','sub_sub_cats.sub_sub_cat_id')->join('sub_cats','sub_sub_cats.sub_cat_id','=','sub_cats.sub_cat_id')->join('cats','sub_cats.cat_id','=','cats.cat_id')->join('users','carts.user_id','=','users.id')->where('carts.user_id',$user_id)->count();
            return view('check_out')->with('cart',$cart)->with('product_id1',$product_id)->with('address',$address)->with('address2',$address2)->with('cod_count',$cod_count);     }                                                                                                                                          
       else
       {
            $cart=DB::table('none_carts')->join('products','none_carts.product_id','=','products.product_id')->join('sub_sub_cats','products.sub_sub_cat_id','=','sub_sub_cats.sub_sub_cat_id')->join('sub_cats','sub_sub_cats.sub_cat_id','=','sub_cats.sub_cat_id')->join('cats','sub_cats.cat_id','=','cats.cat_id')->join('users','none_carts.user_id','=','users.id')->where('none_carts.product_id',$product_id)->where('none_carts.user_id',$user_id)->get();
            $cod_count=DB::table('none_carts')->join('products','none_carts.product_id','=','products.product_id')->join('sub_sub_cats','products.sub_sub_cat_id','=','sub_sub_cats.sub_sub_cat_id')->join('sub_cats','sub_sub_cats.sub_cat_id','=','sub_cats.sub_cat_id')->join('cats','sub_cats.cat_id','=','cats.cat_id')->join('users','none_carts.user_id','=','users.id')->where('none_carts.product_id',$product_id)->where('none_carts.user_id',$user_id)->count();
            return view('check_out')->with('cart',$cart)->with('product_id1',$product_id)->with('address',$address)->with('address2',$address2)->with('cod_count',$cod_count);
       }  
    }

    public function shipping_calculation(Request $crte)
    {
        $user_id=Auth::user()->id;
        $token=$crte->_token;
        $address_id=$crte->address_id;
        $price1=$crte->price1;
        $addresses=DB::table('addres')->where('id',$address_id)->first();
        $pin_code=$addresses->pincode;
        $pin_count=DB::table('pincodes')->where('pincode',$pin_code)->count();
        $pin_count11=DB::table('pincodes')->where('pincode',$pin_code)->first();
        $cod_avalilable=$pin_count11->cod;
        if($pin_count!=0){
        $name=$addresses->name;
        $phone_no=$addresses->phone_no;
        $add=$addresses->flat.','.$addresses->location.','.$addresses->city;
        $email=Auth::user()->email; 


        echo $data=$name.'quantexfggjfjftry'.$phone_no.'quantexfggjfjftry'.$email.'quantexfggjfjftry'.$add.'quantexfggjfjftry'.$cod_avalilable.'quantexfggjfjftry';
              $pin_code1=DB::table('pincodes')->where('pincode',$pin_code)->first();
              $delivery_charge=$pin_code1->delivery_charge;
              $minimum_order_value=$pin_code1->min_price;
         if($price1>$minimum_order_value)
         {
             echo 0;
         }
         else
         {
           echo $delivery_charge;
         }
        }else{
            echo 5;
        }
    }

    public function  wallet_calculation(Request $crte)
    {
        $user_id=Auth::user()->id;
        $token=$crte->_token;
        $price1=$crte->price1;
        $s_charge=$crte->s_charge;
        $coupon_discount=$crte->coupon_discount; 
        $total_amount=$s_charge+$price1-$coupon_discount;
        $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first(); 
        $wallet_ammount=$wallet->wallet_ammount; 

        if($wallet_ammount>$total_amount)
       {
          echo $total_amount;
       }
       else if($wallet_ammount<=$total_amount)
       {
          echo $wallet_ammount;
       }
    
    }

    public function  coupon_calculation(Request $crte)
{   
    $user_id=Auth::user()->id;
    $token=$crte->_token;
    $price1=$crte->price1;
    $s_charge=$crte->shipping_charge;
    $coupon_code=$crte->coupon_code;
    $product_id=$crte->product_id;
    if($coupon_code!=''){
        $copo=DB::table('coupon_banners')->where('coupon_code',$coupon_code)->count();
        if($copo!=0){
            $co=DB::table('bookings')->where('coupon_code',$coupon_code)->where('customer_id',$user_id)->count();
            if($co==0){
                $adc=DB::table('coupon_banners')->where('coupon_code',$coupon_code)->where('active_status','YES')->count();
                if($adc!=0){
                             $coupon=DB::table('coupon_banners')->where('coupon_code',$coupon_code)->first();
                             
                             $cat_id=$coupon->cat_id;
                            
                           
                             if($price1>$coupon->min_price){
                                    $coupon_value=$coupon->coupon_value;
                                    $coupon_status=$coupon->active_status;
                                    
                                   if( $cat_id==0) 
                                   {

                                   

                                if($coupon->coupon_type=='PERCENTAGE')
                                {
                                    $coupon_price= round($price1*($coupon_value/100));
                                    $total_amount=$s_charge+$price1-$coupon_price;
                                    $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
                                    $wallet_ammount=$wallet->wallet_ammount;
                                    if($wallet_ammount>$total_amount)
                                    {
                                        echo $total_amount."|".$coupon_price."|".$s_charge;
                                    }
                                    else if($wallet_ammount<=$total_amount)
                                    {
                                        echo $wallet_ammount."|".$coupon_price."|".$s_charge;
                                    }
                                }
                                else if($coupon->coupon_type=='FLAT')
                                {
                                    $coupon_price=$coupon_value;
                                    $total_amount=$s_charge+$price1-$coupon_price;
                                    $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
                                    $wallet_ammount=$wallet->wallet_ammount;
                                    if($wallet_ammount>$total_amount)
                                    {
                                        echo $total_amount."|".$coupon_price."|".$s_charge;
                                    }
                                    else if($wallet_ammount<=$total_amount)
                                    {
                                        echo $wallet_ammount."|".$coupon_price."|".$s_charge;
                                    }
                                }

                            }
                            else
                            {
                                if($product_id!=0)
                                {
                                    $user_id=Auth::user()->id;
                                   

                                    $cou=DB::table('none_carts')->where('user_id',$user_id)->get(); 
                                    $i=1;
                                    foreach($cou as $cou1)
                                    {
                                        $product_id=$cou1->product_id;
                                        $cou1=DB::table('products')->where('product_id',$product_id)->first(); 
                                        $cattt_id=$cou1->cat_id;
                                        if($cattt_id==$cat_id)
                                        {
                                            $i=$i+1;
                                        }

                                    }
                                 
                                   


                                    
                                        if($i==2)
                                        {
                                                if($coupon->coupon_type=='PERCENTAGE')
                                                {
                                                    $coupon_price= round($price1*($coupon_value/100));
                                                    $total_amount=$s_charge+$price1-$coupon_price;
                                                    $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
                                                    $wallet_ammount=$wallet->wallet_ammount;
                                                    if($wallet_ammount>$total_amount)
                                                    {
                                                        echo $total_amount."|".$coupon_price."|".$s_charge;
                                                    }
                                                    else if($wallet_ammount<=$total_amount)
                                                    {
                                                        echo $wallet_ammount."|".$coupon_price."|".$s_charge;
                                                    }
                                                }
                                                else if($coupon->coupon_type=='FLAT')
                                                {
                                                    $coupon_price=$coupon_value;
                                                    $total_amount=$s_charge+$price1-$coupon_price;
                                                    $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
                                                    $wallet_ammount=$wallet->wallet_ammount;
                                                    if($wallet_ammount>$total_amount)
                                                    {
                                                        echo $total_amount."|".$coupon_price."|".$s_charge;
                                                    }
                                                    else if($wallet_ammount<=$total_amount)
                                                    {
                                                        echo $wallet_ammount."|".$coupon_price."|".$s_charge;
                                                    }
                                                }
                                         }
                                        else
                                        {
                                           echo 8; 
                                        }





                                        }
                                        else
                                        {

                                         //   $cat_id1=$coupon11->cat_id;
                                            $user_id=Auth::user()->id;
                                          
                                            $cou=DB::table('carts')->where('user_id',$user_id)->get(); 
                                            $i=1;
                                            foreach($cou as $cou1)
                                            {
                                                 $product_id=$cou1->product_id;
                                               
                                           
                                                $cou1=DB::table('products')->where('product_id',$product_id)->first(); 
                                            
                                               $cattt_id=$cou1->cat_id;
                                         


                                              if($cattt_id!=$cat_id)
                                              {
                                                $i=$i+1;
                                              }

                                            }
                                         
                                       
                                            
                                                if($i==1)
                                                {
                                                        if($coupon->coupon_type=='PERCENTAGE')
                                                        {
                                                            $coupon_price= round($price1*($coupon_value/100));
                                                            $total_amount=$s_charge+$price1-$coupon_price;
                                                            $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
                                                            $wallet_ammount=$wallet->wallet_ammount;
                                                            if($wallet_ammount>$total_amount)
                                                            {
                                                                echo $total_amount."|".$coupon_price."|".$s_charge;
                                                            }
                                                            else if($wallet_ammount<=$total_amount)
                                                            {
                                                                echo $wallet_ammount."|".$coupon_price."|".$s_charge;
                                                            }
                                                        }
                                                        else if($coupon->coupon_type=='FLAT')
                                                        {
                                                            $coupon_price=$coupon_value;
                                                            $total_amount=$s_charge+$price1-$coupon_price;
                                                            $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
                                                            $wallet_ammount=$wallet->wallet_ammount;
                                                            if($wallet_ammount>$total_amount)
                                                            {
                                                                echo $total_amount."|".$coupon_price."|".$s_charge;
                                                            }
                                                            else if($wallet_ammount<=$total_amount)
                                                            {
                                                                echo $wallet_ammount."|".$coupon_price."|".$s_charge;
                                                            }
                                                        }
                                                 }
                                                else
                                                {
                                                    $catjs=DB::table('cats')->where('cat_id',$cat_id)->first();
                     
                                                   echo "9"."|".$catjs->cat_name; 
                                                   
                                                }
        








                                     }


















                            }

                            }else{
                                $a=4;
                                echo $a."|".$coupon->min_price;
                            }









                    }else{
                        echo 2; 
                    }

            }else{
                echo 5;
            }

        }else{
            echo 3;
        }

    }else{
        echo 3;
    }


}

                public function successfull(Request $ss1)
                {
                    return view('successfull');
                }

                public function order_submit(Request $ss1)
                {

                $product_id1=$ss1->input('product_id');

                $grand_total=$ss1->input('total_payble_value');
                $sub_total=$ss1->input('price1');
                $address_id=$ss1->input('radioValue');
                $razorpay_payment_id=$ss1->razorpay_payment_id;
                $payment_type=$ss1->payment_type;
                $wallet_amount=$ss1->input('wallet_amount');
                $delivery_charge=$ss1->input('shipping_charge');
                $coupon_discount=$ss1->input('coupon_discount');
                $coupon_code=$ss1->input('coupon_code');
                $token=$ss1->input('_token');
                $addressdetails=DB::table('addres')->where('id',$address_id)->first();
                $user_id=Auth::user()->id;
                $name=$addressdetails->name;
                $to_email=Auth::user()->email;
                $phone=$addressdetails->phone_no;
                $order_id1=uniqid();
                $order_id=strtoupper($order_id1);
                Session::flash('OrderID', $order_id); 
                Session::flash('amount', $grand_total); 
                $booking1=new booking; 
                $booking1->customer_id=$user_id;
                $booking1->coupon_code=$coupon_code;
                $booking1->price=$sub_total;
                $booking1->total_price=$grand_total;
                $booking1->wallet_amount=$wallet_amount;
                $booking1->delivery_charge=$delivery_charge;
                $booking1->coupon_discount=$coupon_discount;
                $booking1->order_id=$order_id;
                $booking1->razorpay_payment_id=$razorpay_payment_id;
                $booking1->payment_type=$payment_type;
                $booking1->address_id=$address_id;
                $booking1->remember_token=$token;
                $booking1->save();
                $booking_id=$booking1->id;
                if($product_id1==0)
                {
                $cart=DB::table('carts')->where('user_id',$user_id)->get();
                foreach($cart as $cats)
                {
                $qty=$cats->quantity;
                $product_id=$cats->product_id;
                $product_price_id=$cats->product_price_id;
                $product_price=DB::table('product_prices')->where('product_prices_id',$product_price_id)->first();
            
                $size=$product_price->size;
               if(Auth::user()->usertype=='USER') 
               {
                $price1=$product_price->selling_price;
               }
               else
               {

                $price1=$product_price->mediator_price;
               }

                $booking2=new book_multi_item;
                $booking2->product_id=$product_id;
                $booking2->booking_id=$booking_id;
                $booking2->quantity1=$qty;

                $booking2->size=$size;

                $booking2->product_prices_id=$product_price_id;
                $booking2->product_price=$price1;
                $booking2->remember_token=$token;
                $booking2->save();
               
            }
                $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
                $wallet_amount1=$wallet->wallet_ammount;
                if($wallet_amount!=0)
                    {
                    $remaining_wallet_amount=$wallet_amount1-$wallet_amount;
                    $user_wallet1=new user_wallet_transaction;
                    $user_wallet1->user_id=$user_id;
                    $user_wallet1->transaction_amount=$wallet_amount;
                    $user_wallet1->after_transaction_amount=$remaining_wallet_amount;
                    $user_wallet1->remarks='Booking';
                    $user_wallet1->order_id=$order_id;
                    $user_wallet1->tranasaction_type='Debit';
                    $user_wallet1->booking_id=$booking_id;
                    $user_wallet1->remember_token=$token;
                    $user_wallet1->save();
                    DB::table('user_wallets')->where('user_id',$user_id)->update(['wallet_ammount' => $remaining_wallet_amount]); 
                }
                echo 0;
               
                try{
                    self::smsRequest($phone,"".$name." your order have been placed. It will be delivered to you after 7-10 working days");      
                $data = array('name'=>$name,'email'=>$to_email,'body'=>"Hii ".$name." your order have been placed. It will be delivered to you after 7-10 working days");
                    Mail :: send('mail',$data,function($message) use ($name,$to_email)
                {
                $message->to($to_email)
                ->subject('Booking');
                });
                }
                catch (\Exception $e)
                {
                echo 0;
                }
                $user_id=Auth::user()->id;
                $cart=DB::table('carts')->where('user_id',$user_id)->get();
                foreach($cart as $cats)
                {
                    $qty=$cats->quantity;
                    $product_id=$cats->product_id;
                    $product_price_id=$cats->product_price_id;



                   $tta=DB::table('product_prices')->where('product_prices_id',$product_price_id)->first();
                   $available_stock=$tta->available_stock; 
                   $remaining_stack=$available_stock-$qty;
                    

                   $stock= new stock_trasanction;
                    $stock->product_id=$product_id;
                    $stock->product_price_id=$product_price_id; 
                    $stock->quantity=$qty;
                    $stock->type='DEBIT';
                    $stock->remarks='BOOKING';
                    $stock->transfer_id=$order_id;
                    $stock->total_quantity=$remaining_stack;
                    $stock->save();
                    DB::table('product_prices')->where('product_prices_id',$product_price_id)->update(['available_stock' => $remaining_stack]);
                 
                   
                }
                DB::table('carts')->where('user_id',$user_id)->delete();
                }
                else{
                    $cart=DB::table('none_carts')->where('user_id',$user_id)->where('product_id',$product_id1)->get();
                    foreach($cart as $cats)
                    {
                    $qty=$cats->quantity;
                    $product_id=$cats->product_id;
                    $product_price_id=$cats->product_price_id;
                    $product_price=DB::table('product_prices')->where('product_prices_id',$product_price_id)->first();
                    $size=$product_price->size;
                    if(Auth::user()->usertype=='USER') 
                    {
                     $price1=$product_price->selling_price;
                    }
                    else
                    {
     
                     $price1=$product_price->mediator_price;
                    }

                    $booking2=new book_multi_item;
                    $booking2->product_id=$product_id1;
                    $booking2->booking_id=$booking_id;
                    $booking2->quantity1=$qty;
                    $booking2->size=$size;
                    $booking2->product_prices_id=$product_price_id;
                    $booking2->product_price=$price1;
                    $booking2->remember_token=$token;
                    $booking2->save();
                    }
                    $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
                    $wallet_amount1=$wallet->wallet_ammount;
                    if($wallet_amount!=0)
                    {
                        $remaining_wallet_amount=$wallet_amount1-$wallet_amount;
                        $user_wallet1=new user_wallet_transaction;
                        $user_wallet1->user_id=$user_id;
                        $user_wallet1->transaction_amount=$wallet_amount;
                        $user_wallet1->after_transaction_amount=$remaining_wallet_amount;
                        $user_wallet1->remarks='Booking';
                        $user_wallet1->order_id=$order_id;
                        $user_wallet1->tranasaction_type='Debit';
                        $user_wallet1->booking_id=$booking_id;
                        $user_wallet1->remember_token=$token;
                        $user_wallet1->save();
                        DB::table('user_wallets')->where('user_id',$user_id)->update(['wallet_ammount' => $remaining_wallet_amount]);
                    } 
                echo 1;
                   try{
                    self::smsRequest($phone,"".$name." your order have been placed. It will be delivered to you after 7-10 working days");      
               
                $data = array('name'=>$name,'email'=>$to_email,'body'=>"Hii ".$name." your order have been placed. It will be delivered to you after 7-10 working days");
                    Mail :: send('mail',$data,function($message) use ($name,$to_email)
                {
                $message->to($to_email)
                ->subject('Booking');
                });
                }
                catch (\Exception $e)
                {
                    echo 0;

                }
                $user_id=Auth::user()->id;
                $cart=DB::table('none_carts')->where('user_id',$user_id)->where('product_id',$product_id1)->get();
                foreach($cart as $cats)
                {
                    $qty=$cats->quantity;
                    $product_id=$cats->product_id;
                    $product_price_id=$cats->product_price_id;
                $tta=DB::table('product_prices')->where('product_prices_id',$product_price_id)->first();
                $available_stock=$tta->available_stock; 
                $remaining_stack=$available_stock-$qty;
                 

                $stock= new stock_trasanction;
                 $stock->product_id=$product_id;
                 $stock->product_price_id=$product_price_id; 
                 $stock->quantity=$qty;
                 $stock->type='DEBIT';
                 $stock->remarks='BOOKING';
                 $stock->transfer_id=$order_id;
                 $stock->total_quantity=$remaining_stack;
                 $stock->save();
                 DB::table('product_prices')->where('product_prices_id',$product_price_id)->update(['available_stock' => $remaining_stack]);
              
               
                
                }
                    DB::table('none_carts')->where('user_id',$user_id)->delete();
                }
         }

               public function pay(Request $req){
                    $api_key = "rzp_test_s8waJxmKNm05sl";
                    $api_secret = "jtG3DZuRxBvXk7sHfJPjFAL1";

                    $api = new Api($api_key, $api_secret);

                    $order = $api->order->create(array(  'receipt' => '1234456',  'amount' => $req->amt_val*100,  'payment_capture' => 1,  'currency' => 'INR'  ));
                    $returnData = [
                        'order_id'=> $order->id,
                        'amount'=> $order->amount_due 
                    ];
                    return $returnData;
                 }

         public function paySuccess(Request $req){
            $donation = new donation();
            $donation->name = $req->name;
            $donation->amount = $req->amount;
            $donation->phone = $req->phone;
            $donation->payment_id = $req->payment_id;
            $donation->save();
        // self::smsRequest($req->phone,"Payment Received of amount : Rs.".$req->amount);
            return "Payment Received";
          }

           public function payTest(Request $req){
                $array = [
                    'name'=> $req->name_val,
                    'amount'=> $req->amt_val,
                    'phone' => $req->phone_val
                ];
                return $array;
            }



    public function address_submit( Request $req11)
    {
        $user_id=Auth::user()->id;
        $gst=$req11->input('gst');
        $phone2=$req11->input('phone2');
        $token=$req11->input('_token');
        $phone1=$req11->input('phone1');
        $name=$req11->input('name');
        $address=$req11->input('address');
        $state=$req11->input('state');
        $district=$req11->input('district');
        $city=$req11->input('city');
        $area=$req11->input('area');
        $pin_code=$req11->input('pin_code');
        $landmark=$req11->input('landmark');
        $house_no=$req11->input('house_no');
            $address2=new addre;
            $address2->flat=$house_no;
            $address2->landmark=$landmark;
            $address2->location=$area;
            $address2->pincode=$pin_code;
            $address2->city=$city;
            $address2->district=$district;
            $address2->state=$state;
            $address2->name=$name;
            $address2->phone_no=$phone1;
            $address2->optional_phone=$phone2;
            $address2->address=$address;
            $address2->gst_no=$gst;
            $address2->user_id=$user_id;
            $address2->remember_token=$token;
            $address2->save();
            $address223=DB::table('billing_adds')->where('user_id',$user_id)->count();
            if($address223==0)
            {
                $item = new billing_add;
                $item->user_id=Auth::user()->id;
                $item->pincode=$pin_code;
                $item->address=$address;
                $item->landmark=$landmark;
                $item->location=$area;
                $item->city=$city;
                $item->district=$district;
                $item->state=$state;
                $item->remember_token=$token;
                $item->save(); 
            } 
      }

      public function fetch_address_checkout(Request $req)
     { 
                $pin=$req->pin;
                $address111=DB::table('pincodes')->where('pincode',$pin)->where('active_status','YES')->count();
                if($address111!=0){
                $client = new Client();
                try {
                    $response = $client->request('GET', 'https://pincode.saratchandra.in/api/pincode/'.$pin);
                    $statusCode = $response->getStatusCode();
                    $body = json_decode($response->getBody()->getContents());
                    $location=$body->data[0]->office_name;
                    $city=$body->data[0]->taluk;
                    $dist=$body->data[0]->district;
                    $state=$body->data[0]->state_name;
                    echo $location.'|checkoutfatchaddredd|'.$city.'|checkoutfatchaddredd|'.$dist.'|checkoutfatchaddredd|'.$state.'|checkoutfatchaddredd|1';
                } catch (\Exception $e) {
                    echo '|checkoutfatchaddredd| |checkoutfatchaddredd| |checkoutfatchaddredd| |checkoutfatchaddredd|0';
                }
                }else{
                $client = new Client();
                try {
                    $response = $client->request('GET', 'https://pincode.saratchandra.in/api/pincode/'.$pin);
                    $statusCode = $response->getStatusCode();
                    $body = json_decode($response->getBody()->getContents());
            
                    echo  " |checkoutfatchaddredd| |checkoutfatchaddredd| |checkoutfatchaddredd| |checkoutfatchaddredd|2";
                } catch (\Exception $e) {
                    echo '|checkoutfatchaddredd| |checkoutfatchaddredd| |checkoutfatchaddredd| |checkoutfatchaddredd|0';
                }
            }
       }

       public function address_update( Request $req11)
       {
           $user_id=Auth::user()->id;
           $gst=$req11->input('gst');
           $phone2=$req11->input('phone2');
           $token=$req11->input('_token');
           $phone1=$req11->input('phone1');
           $name=$req11->input('name');
           $address=$req11->input('address');
           $state=$req11->input('state');
           $district=$req11->input('district');
           $city=$req11->input('city');
           $area=$req11->input('area');
           $pin_code=$req11->input('pin_code');
           $landmark=$req11->input('landmark');
           $house_no=$req11->input('house_no');
           $address_id=$req11->input('address_id');
               DB::table('addres')
               ->where('id', $address_id)
               ->update(['flat' =>$house_no,'landmark' =>$landmark,'location' =>$area,'pincode' =>$pin_code,'city' =>$city,'district' =>$district,'state' =>$state,'name' =>$name,'phone_no' =>$phone1,'optional_phone' =>$phone2,'address' =>$address,'gst_no' =>$gst,'remember_token'=>$token]);  
       }
   

    public function cart()
    {
        return view('cart');
    }
    public function wishlist()
    {
        return view('wishlist');
    }
    public function wishlist_page_ajax(Request $req)
    {
        
        $user=Auth::user()->id;
       
        $pro=DB::table('wishlists')->where('user_id',$user)->get();
        return view('wishlist_page_ajax')->with('item',$pro);


    }
    public function cart_page_ajax()
    {
        if (Auth::check()) {
            $user_id=Auth::user()->id;
           
            $for_cart_view=DB::table('carts')->where('user_id',$user_id)->join('products','carts.product_id','=','products.product_id')->join('product_prices','carts.product_price_id','=','product_prices.product_prices_id')->get();
        
            return view('cart_page_ajax')->with('for_cart_view',$for_cart_view);

                }
         else{
                    $user_id=0;
                    return view('cart_page_ajax');
        
         }




      
    }
    public function update_qty(Request $rp)
    {
     $user_id=Auth::user()->id;
      $id=$rp->id;
       $qty=$rp->value;
     
        $t=DB::table('carts')->where('user_id',$user_id)->where('id',$id)->update(['quantity' => $qty]);

    }
    public function profile(){
        $user_id=Auth::user()->id;
        $address=DB::table('billing_adds')->where('user_id',$user_id)->where('user_id',$user_id)->first();
        $address_count=DB::table('billing_adds')->where('user_id',$user_id)->where('user_id',$user_id)->count();

        return view('profile')->with('address',$address)->with('address1',$address)->with('address_count',$address_count);
    }
    
public function update_profile(Request $details)
{
    $this->validate($details,[
        'phone'=>'required|min:10',
        'name'=>'required'
        
    ],
 );


    $id=Auth::user()->id;
    $name=$details->input('name');
  
    $email=$details->input('email');
    $phone_no=$details->input('phone');
    $details->hasFile('profile_image');
   

    DB::table('users')->where('id',$id)->update(['name' =>$name,'email' =>$email,'mobile' =>$phone_no]);
            
    return redirect('/profile')->with('success','Your Profile Update Successfully');

}

protected function change_password(Request $request)
{
 
   $request->validate([
    'current_password' => ['required', new MatchOldPassword],
        'password' => 'required|min:8',
        'confirm' => ['same:password'],
    ], [
        'current_password.required' => 'Please enter Your Current Password',
        'password.required' => 'Please enter New Password',
        'confirm.same' => 'Confirm Password do not match',
    ]);

    // check validation
   


    User::find(auth()->user()->id)->update(['password'=> Hash::make($request->password)]);

        return redirect()->back()->with('success','Password Change Successfully');

  
}

public function billing_address(Request $req)
 { 

    $req->validate([
            'pin' => 'required|min:6',
            'address' => 'required',
            'state' => 'required',
            'landmark' => 'required',
            'location' => 'required',
            'city' => 'required',
            'district' => 'required',
        ]);


    $user_id=Auth::user()->id;

    $pin=$req->input('pin');
    $address=$req->input('address');
    $landmark=$req->input('landmark');
    $location=$req->input('location');
    $city=$req->input('city');
    $district=$req->input('district');
    $state=$req->input('state');

    $address22=DB::table('billing_adds')->where('user_id',$user_id)->where('user_id',$user_id)->count();

if($address22==0){

    $item = new billing_add;
    $item->user_id=Auth::user()->id;
    $item->pincode=$pin;
    $item->address=$address;
    $item->landmark=$landmark;
    $item->location=$location;
    $item->city=$city;
    $item->district=$district;
    $item->state=$state;


    $item->remember_token=$req->input('_token');
    $item->save();
    $ship=DB::table('addres')->where('user_id',$user_id)->where('user_id',$user_id)->count();
    if($ship==0){
        $item1 = new addre;
        $item1->user_id=Auth::user()->id;
        $item1->pincode=$pin;
        $item1->address=$address;
        $item1->landmark=$landmark;
        $item1->location=$location;
        $item1->city=$city;
        $item1->district=$district;
        $item1->state=$state;
        $item1->name=Auth::user()->name;
        $item1->phone_no=Auth::user()->mobile;
        $item1->remember_token=$req->input('_token');
        $item1->save();
    }
    return redirect()->route('profile')->with('success1','Address Save successfully!');
}else{
    $address1=DB::table('billing_adds')->where('user_id',$user_id)->where('user_id',$user_id)->first();
    $id=$address1->id;
    DB::table('billing_adds')->where('id',$id)->update(['pincode' => $pin,'address' => $address,'landmark' => $landmark,'location' => $location,'city' => $city,'district' => $district,'state' => $state]);
    return redirect()->route('profile')->with('success1','Address update successfully!');

}



 }
 public function fetch_address(Request $req)
 { 
     $pin=$req->pin;
     $client = new Client();
     try {
         $response = $client->request('GET', 'https://pincode.saratchandra.in/api/pincode/'.$pin);
         $statusCode = $response->getStatusCode();
         $body = json_decode($response->getBody()->getContents());
 
         $location=$body->data[0]->office_name;
         $city=$body->data[0]->taluk;
         $dist=$body->data[0]->district;
         $state=$body->data[0]->state_name;
         echo $location.'|'.$city.'|'.$dist.'|'.$state.'|1';
    } catch (\Exception $e) {
         echo '| | | |0';
     }
 
 }
 public function order(Request $req)
 { 

    $user_id=Auth::user()->id;
    $booking= DB::table('bookings')
    ->join('book_multi_items', 'bookings.booking_id', '=', 'book_multi_items.booking_id')->join('products', 'book_multi_items.product_id', '=', 'products.product_id')
   
    ->where('bookings.customer_id','=',$user_id)
    ->orderby('bookings.booking_id','desc')
  
    ->get();


    $booking1= DB::table('bookings')
    ->join('book_multi_items', 'bookings.booking_id', '=', 'book_multi_items.booking_id')->join('products', 'book_multi_items.product_id', '=', 'products.product_id')
   
    ->where('bookings.customer_id','=',$user_id)
    ->orderby('bookings.booking_id','desc')
    ->where('book_multi_items.reword_status','NO')
    ->get();



    foreach($booking1 as $booking11)
        {

         $multi_id=$booking11->multi_id;
         $delivery_date=$booking11->delivery_date;
         $valedity=3;
         $redeemdate=date('Y-m-d', strtotime(($delivery_date). ' + '.$valedity.' days'));
         $mytime = Carbon\Carbon::now();
         $date1=date_create($redeemdate);
         $date2=date_create($mytime);
         $diff=date_diff($date2,$date1);

         $deffrant=$diff->format("%R%a");

         if( $deffrant<=0)
         {
              DB::table('book_multi_items')
                ->where('multi_id',$multi_id)
                ->update(['return_status' => 'Return Policy is Over']);

             $user_id=Auth::user()->id;
             



             $tt=DB::table('bookings')->where('customer_id', $user_id)->where('booking_id','<',$booking11->booking_id)->count();
             if( $tt ==0)
             {
             
                $user_wallet_details= DB::table('user_wallets')->where('user_id',$user_id)->first();
               


                $book_multi_items_details= DB::table('book_multi_items')->where('multi_id',$multi_id)->first();
                $booking_id=$book_multi_items_details->booking_id;
                $product_price=$book_multi_items_details->product_price;
                $book_details= DB::table('bookings')->where('booking_id',$booking_id)->first();
                $order_id=$book_details->order_id;
                $order_price=$book_details->price;
                $token=$book_details->remember_token;
                $persentage=(($product_price/$order_price)*100);

                $user_earn=($order_price*($persentage/100)*(10/100));
               
               


                $user_wallet_amount=$user_wallet_details->wallet_ammount;
                $updated_user_wallet_amount=$user_wallet_amount+$user_earn;

              

  
                    $user_wallet1=new user_wallet_transaction;
                    $user_wallet1->user_id=$user_id;
                    $user_wallet1->transaction_amount=$user_earn;
                    $user_wallet1->after_transaction_amount=$updated_user_wallet_amount;
                    $user_wallet1->remarks='Welcome Booking Cashback';
                    $user_wallet1->order_id=$order_id;
                    $user_wallet1->tranasaction_type='Credit';
                    $user_wallet1->booking_id=$booking_id;
                    $user_wallet1->remember_token=$token;
                    $user_wallet1->save();

                    DB::table('user_wallets')->where('user_id',$user_id)->update(['wallet_ammount' => $updated_user_wallet_amount]); 
                    DB::table('book_multi_items')->where('multi_id',$multi_id)->update(['reword_status' => 'YES']); 
                  
            }
             


         }

        }

        $use_cust_id=Auth::user()->use_cust_id;
 
        if($use_cust_id!="")
        {

            $tt1=DB::table('users')->where('id', $user_id)->where('refer_status','=','NO')->count();
            $bookings_id=DB::table('bookings')->where('customer_id', $user_id)->first();
        if($tt1>0)
{
        $reff_user_wallet_details= DB::table('user_wallets')->where('user_id',$use_cust_id)->first();
        $reffer_earn=50;
        $refarar_wallet_amount=$reff_user_wallet_details->wallet_ammount;
        $updated_refarar_wallet_amount=$refarar_wallet_amount+$reffer_earn;


        $user_wallet1=new user_wallet_transaction;
        $user_wallet1->user_id=$use_cust_id;
        $user_wallet1->transaction_amount=$reffer_earn;
        $user_wallet1->after_transaction_amount=$updated_refarar_wallet_amount;
        $user_wallet1->remarks='Refer Cashback';
        $user_wallet1->order_id=$bookings_id->order_id;
        $user_wallet1->tranasaction_type='Credit';
        $user_wallet1->booking_id=$bookings_id->booking_id;
  
        $user_wallet1->save();

        DB::table('user_wallets')->where('user_id',$use_cust_id)->update(['wallet_ammount' => $updated_refarar_wallet_amount]); 
        DB::table('users')->where('id', $user_id)->update(['refer_status' => 'YES']); 
}

 }




     return view('order',['booking'=>$booking]);
 }

 public function return_status(Request $tr)
 {
     $multi_id=$tr->product_id;
     $cancel_option=$tr->cancel_option;
     $message=$tr->message;
     $refund_option=$tr->refund_option;
     if(isset($tr->acc_name))
     {
     $acc_name=$tr->acc_name;
     }
     else
     {
        $acc_name='NO';  
     }

     if(isset($tr->acc_no))
     {
        $acc_no=$tr->acc_no;
     }
     else
     {
        $acc_no='NO';  
     }
     if(isset($tr->acc_ifsc))
     {
        $acc_ifsc=$tr->acc_ifsc;
     }
     else
     {
        $acc_ifsc='NO';  
     }
     
    
     $token=$tr->_token;

 $user=Auth::user()->id;
 $cancel_status = new cancel_reasons;
 $cancel_status->book_muti_item_id=$multi_id;
 $cancel_status->cancel_option=$cancel_option;
 $cancel_status->message=$message;
 $cancel_status->refund_type=$refund_option;
 $cancel_status->acc_name=$acc_name;
 $cancel_status->acc_no=$acc_no;
 $cancel_status->acc_ifsc=$acc_ifsc;
 $cancel_status->remember_token=$token;
 $cancel_status->status='Return';
 $cancel_status->save();

 DB::table('book_multi_items')
 ->where('multi_id',$multi_id)
 ->update(['order_status' => 5]);
 $as=DB::table('book_multi_items')
 ->where('multi_id',$multi_id)
 ->first();

 $product_price=$as->product_price*$as->quantity1;



 $booking_id=$as->booking_id;
 $ass=DB::table('bookings')
 ->where('booking_id',$booking_id)
 ->first();


 $address_id=$ass->address_id;
 $order_id=$ass->order_id;

 $asss=DB::table('addres')
 ->where('id',$address_id)
 ->first();


 $to_name=$asss->phone_no;
 $to_name=$asss->name;
 $to_email=Auth::user()->email;












//    try{
//              $data = array('name'=>$to_name,'email'=>$to_email,'body'=>"Hi ".$to_name.", We have Received Your Return Request for Order Id ".$order_id.". Your request is under process. You will be notified shortly on further progress in this regard. ");
//             Mail :: send('mail1',$data,function($message) use ($to_name,$to_email)
//             {
//                 $message->to($to_email)
//                 ->subject('Return Requested');
//             });
//          }
//          catch(\Exception $e) {
 
//          }
//             $phone=Auth::user()->phone;
           // self::smsRequest($phone,"Thank you for your Order with Drishtikart. Your order with Order ID ".$order_id." for ".$product_name.", is successfully placed.<br>Total price: ".$price1.", Payment mode: ".$payment_type.", Shipping Address: ".$address->flat.",".$address->location." <br> ".$address->city.", ".$address->state."<br> ".$address->pincode."");
 return redirect()->back();
 }

 public function cencel_status(Request $tr)
 {
      $multi_id=$tr->product_id;
      $cancel_option=$tr->cancel_option;
      $message=$tr->message;
      $token=$tr->_token;
      $product= DB::table('book_multi_items')->where('multi_id',$multi_id)->first();
       $product_price=$product->product_price*$product->quantity1;


      $booking_id=$product->booking_id;

      $boooking= DB::table('bookings')->where('booking_id',$booking_id)->first();

      $total_booking_price=$boooking->price;
      $parsentage=((($product_price)/$total_booking_price)*100);
      $delivary_charge=$boooking->delivery_charge;
      $coupon_discount=$boooking->coupon_discount;
      $wallet_amount1=$boooking->wallet_amount;
      $order_id=$boooking->order_id;
      $token=$boooking->remember_token;

      $product= DB::table('book_multi_items')->where('booking_id',$booking_id)->count();




      if($delivary_charge!=0){
          $product_delivary_charge=$delivary_charge*($parsentage/100);
      }else{
        $product_delivary_charge=0;
      }
        if($coupon_discount!=0){
        $product_coupon_discount=$coupon_discount*($parsentage/100);
        }else{
        $product_coupon_discount=0;
        }


        if($wallet_amount1!=0){
        $product_wallet_amount=$wallet_amount1*($parsentage/100);
        }else{
        $product_wallet_amount=0;
        }

    $amount=round(($product_price+$product_delivary_charge)-$product_coupon_discount-$product_wallet_amount);

    $user_id=Auth::user()->id;
    $wallet= DB::table('user_wallets')->where('user_id',$user_id)->first();
     if($wallet_amount1!=0){
     $wallet_id=$wallet->wallet_id;
     $wallet_amount=$wallet->wallet_ammount;
     $wallet_updated_amount=$wallet_amount+$product_wallet_amount;

      DB::table('user_wallets')
      ->where('wallet_id',$wallet_id)
      ->update(['wallet_ammount' => $wallet_updated_amount]);
      $non_cart1 = new user_wallet_transaction;
      $non_cart1->user_id=$user_id;
      $non_cart1->transaction_amount=$product_wallet_amount;
      $non_cart1->after_transaction_amount=$wallet_updated_amount;
      $non_cart1->remarks='Order Canceled';
      $non_cart1->order_id=$order_id;
      $non_cart1->tranasaction_type='Credit';
      $non_cart1->booking_id=$booking_id;
      $non_cart1->remember_token=$token;
      $non_cart1->save();
 }


        $cancel_status = new cancel_reasons;
        $cancel_status->book_muti_item_id=$multi_id;
        $cancel_status->cancel_option=$cancel_option;
        $cancel_status->message=$message;
        $cancel_status->remember_token=$token;
        $cancel_status->status='Cancel';
        $cancel_status->save();
      DB::table('book_multi_items')
      ->where('multi_id',$multi_id)
      ->update(['order_status' => 8]);
      return redirect()->back();
 }
 







 public function refer(Request $req)
 { 
    if (Auth::check()) {
        $user=Auth::user()->id;
        $pro=DB::table('refer_codes')->where('user_id',$user)->count();

        if($pro==0)
        {

       $user=Auth::user()->id;
       $unicode=strtoupper(uniqid())."SSJ".$user;
      
       
       $reff = new refer_code;
       $reff->user_id=$user;
       $reff->refer_code=$unicode;
 
       $reff->save();
   }
}
     $user_id=Auth::user()->id;
    $ad=DB::table('refer_codes')->where('user_id',$user_id)->first();
    return view('refer')->with('refer_code',$ad);
 }
 public function refersend(Request $req)
 { 
    $phone=$req->phone;
    $code=$req->code;
   
    self::smsRequest($phone,"Join me on Siddharth Shah Jewelers a Secure website For Jewellery Shopping. Enter my Code {$code} or click the link to earn exclusive offers and your friends get cashback on your first order. https://ssj.com/register?ref_code={$code}.");


 }

 
 public function sendReview(Request $req)
 { 
    $user_id=Auth::user()->id;
    $product_id=$req->product_id;
    $reviews=$req->review;
    $review_title=$req->title;
    $review_description=$req->message;
    $token=$req->_token;

    $rev_count=DB::table('reviews')->where('product_id',$product_id)->where('user_id',$user_id)->count();
   
if($rev_count==0)
{
   $review1 = new review;
   $review1->user_id=$user_id;
   $review1->product_id=$product_id;
   $review1->review=$reviews;
   $review1->review_title=$review_title;
   $review1->review_details=$review_description;
   $review1->remember_token=$token;
   $review1->save();


   $product_review=DB::table('products')->where('product_id',$product_id)->first();
   $rating=$product_review->review;

   $review_count=$product_review->total_review;

   $previous_total_review=$rating*$review_count;

   $recent_total_review=$previous_total_review+$reviews;

   $recent_person=$review_count+1;

   $recent_avg_review=$recent_total_review/$recent_person;
   DB::table('products')->where('product_id',$product_id)->update(['review'=>$recent_avg_review,'total_review'=>$recent_person]);
   DB::table('book_multi_items')->join('bookings','bookings.booking_id','=','book_multi_items.booking_id')->where('book_multi_items.product_id',$product_id)->where('book_multi_items.order_status','Delivered')->where('book_multi_items.review_status','NO')->where('bookings.customer_id',$user_id)->update(['review_status'=>'YES']);

  
}
else{
    

$rd=DB::table('reviews')->where('product_id',$product_id)->where('user_id',$user_id)->first();
$review1=$rd->review;

$product_review=DB::table('products')->where('product_id',$product_id)->first();
$rating=$product_review->review;

$review_count=$product_review->total_review;

$previous_total_review=$rating*$review_count;

$recent_total_review=$previous_total_review+$reviews-$review1;

$recent_person=$review_count;

$recent_avg_review=$recent_total_review/$recent_person;
 DB::table('products')->where('product_id',$product_id)->update(['review'=>$recent_avg_review,'total_review'=>$recent_person]);
DB::table('book_multi_items')->join('bookings','bookings.booking_id','=','book_multi_items.booking_id')->where('book_multi_items.product_id',$product_id)->where('book_multi_items.order_status','Delivered')->where('book_multi_items.review_status','NO')->where('bookings.customer_id',$user_id)->update(['review_status'=>'YES']);

DB::table('reviews')->where('product_id',$product_id)->where('user_id',$user_id)->update(['review'=>$reviews,'review_title'=>$review_title,'review_details'=>$review_description]);


}
}

function my_wallet()
{
$id=Auth::user()->id;
$wallet=DB::table('user_wallets')->where('user_id',$id)->first();
$wallet_transction=DB::table('user_wallet_transactions')->where('user_id',$id)->orderby('wallet_transaction_id','desc')->limit(10)->get();



return view('my_wallet')->with('wallet_amount',$wallet)->with('user_wallet_transactions',$wallet_transction);

}




function account(Request $req)
{
    $bb=$req->bb;
    $aa=$req->aa;
    return view('account')->with('bb',$bb)->with('aa',$aa);
}


}
