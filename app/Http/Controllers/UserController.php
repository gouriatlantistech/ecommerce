<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Validator;
use Session;
use DB;
class UserController extends Controller
{
    public $successStatus = 200;

    public function login(Request $request){
        Log::info($request);
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            return view('home');
        }
        else{
            return Redirect::back ();
        }
    }


    public function smsRequest()
{

    $phone=9635371980;
    $msg='juiy';
    $client = new \GuzzleHttp\Client();
    $url = "http://weberleads.in/http-tokenkeyapi.php?authentic-key=37616f6e6b696164656c3435391582279225&senderid=GAON&route=2&number=91".$phone."&message=".$msg;
    $request = $client->get($url);
    $response = $request->getBody();
}




    public function loginWithOtp(Request $request){
        Log::info($request);
        $user  = User::where([['mobile','=',request('mobile')],['otp','=',request('otp')]])->first();
        if( $user){
            Auth::login($user, true);
            User::where('mobile','=',$request->mobile)->update(['otp' => null]);
           
    if($_POST["role"]==Auth::user()->role){

        switch(Auth::user()->role){
            case 'ADMIN':
              
                $this->redirectTo = '/';
            return $this->redirectTo;
                break;
            case 'USER':
                if(Auth::user()->usertype=="USER")
                {
                    return redirect()->route('home');
                    break;
                }
                else{
                    if(Auth::user()->status=='YES')
                    {
                        return redirect()->route('home');
                        break;
                    }
                    else{
                        Auth::logout();
                        Session::flash('message', 'Please wait for admin Approval'); 
                        return redirect()->route('login');
                    } 
                }
            default:
            return redirect()->route('index');
        }
        
    }else{
      
        Auth::logout();
       
        switch($_POST["role"]){
            case 'ADMIN':
                Session::flash('message', 'This is a Admin Dashboard. Here only admin can login and you not admin!'); 
           
            return redirect()->route('admin_login');
                break;
            case 'USER':
                Session::flash('message', 'This is a User Dashboard. Here only User can login and you not a User!'); 
                return redirect()->route('login');
                break;
         
            default:
            return redirect()->route('login');
       }
   }


        }
        else{
            return Redirect::back ();
        }





        
    }
    public function loginWithOtp1(){
        return view('auth/OtpLogin');
    }
    public function register1(Request $request)
    {
        return view('auth/mediator_register');
    }
    
    public function register(Request $request)
    {

      

        request()->validate([

            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'mobile' => ['required', 'integer', 'min:10', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
 
        ]);

        // if ($validator->fails()) {

        //     return redirect()->back();
        //     return response()->json(['error'=>$validator->errors()], 401);
        // }

       
        $input = $request->all();
    
        $input['password'] = bcrypt($input['password']);
        User::create($input);

        return redirect('login');
    }


    public function sendOtp(Request $request){

        $otp = rand(1000,9999);
        Log::info("otp = ".$otp);
        $user = User::where('mobile','=',$request->mobile)->update(['otp' => $otp]);
        self::smsRequest($request->mobile,"Use This otp for Login".$otp);
       
         echo $user."|".$otp;

       // return response()->json([$user],200);
    }



    /********* Admin Login********** */
       
    public function admin_login(){

        $admin=DB::table('users')->where('role','ADMIN')->first();
        return view('admin.auth.login')->with('admin',$admin);
    }
}
