<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Str;//require and composer require laravel/helpers also 
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Validator;
use DB;
use Auth;
use App\user;

class AccountsController extends Controller
{


    function reset1()
    {
       return view('auth.passwords.reset1');
    }

    function reset_link()
    {
       return view('auth.passwords.phone');
    }


    public function smsRequest($phone,$link)
    {
        $client = new \GuzzleHttp\Client();
     
      
       $url = "http://weberleads.in/http-tokenkeyapi.php?authentic-key=38356c656e736465616c3537371583130050&senderid=DRISKT&route=2&number=91".$phone."&message=".$link;
       //    $url = "http://weberleads.in/http-tokenkeyapi.php?authentic-key=38356c656e736465616c3537371583130050&senderid=DRISKT&route=2&number=6294008510&message=hello%20there";  
        $request = $client->get($url);
        $response = $request->getBody();
    }


function validatePasswordRequest(Request $request)
{
    $user = DB::table('users')->where('mobile', '=', $request->mobile)
    ->first();
    $user1 = DB::table('users')->where('mobile', '=', $request->mobile)
    ->count();

    if ($user1 < 1) {
        return redirect()->back()->with('status', trans('User does not exist.'));
    }

    DB::table('password_reset_with_mobiles')->insert([
        'mobile' => $request->mobile,
        'token' => str_random(60),
        'created_at' => Carbon::now()
    ]);

    $tokenData = DB::table('password_reset_with_mobiles')
    ->where('mobile', $request->mobile)->first();
 
    if ($this->sendResetEmail($request->mobile, $tokenData->token)) {
        return redirect()->back()->with('status', trans('A reset link has been sent to your Phone No.'));
    } else {
        return redirect()->back()->withErrors(['error' => trans('A Network Error occurred. Please try again.')]);
    }

}

private function sendResetEmail($mobile, $token)
{
//Retrieve the user from the database
$user = DB::table('users')->where('mobile', $mobile)->select('name', 'mobile')->first();
//Generate, the password reset link. The token generated is embedded in the link
//$link = config('USER_APP_URL') . 'password/reset1/?token=' . $token . '?mobile=' . urlencode($user->mobile);
$link='http://localhost:8000/reset1/?token=' . $token . '?mobile=' . urlencode($user->mobile);

    try {
    //Here send the link with CURL with an external email API 
     self::smsRequest($mobile,$link);
      
        return true;
    } catch (\Exception $e) {
        return false;
        
    }
}


public function resetPassword1(Request $request)
{
    //Validate input
    $validator = Validator::make($request->all(), [
        'mobile' => 'required|integer|exists:users,mobile',
        'password' => 'required|confirmed',
        'token' => 'required' ]);

    //check if payload is valid before moving on
    if ($validator->fails()) {
        return redirect()->back()->withErrors(['email' => 'Please complete the form']);
    }

    $password = $request->password;
// Validate the token
//     $tokenData = DB::table('password_reset_with_mobiles')
//     ->where('token', $request->token)->first();
// // Redirect the user back to the password reset request form if the token is invalid
//     if (!$tokenData) return view('auth.passwords.phone');

    $user = User::where('mobile', $request->mobile)->first();
// Redirect the user back if the email is invalid
    if (!$user) return redirect()->back()->withErrors(['mobile' => 'phone No not found']);
//Hash and update the new password
    $user->password = \Hash::make($password);
    $user->update(); //or $user->save();

    //login the user immediately they change password successfully
    Auth::login($user);

    //Delete the token
   $tt= DB::table('password_reset_with_mobiles')->where('mobile', $user->mobile)
    ->delete();

    //Send Email Reset Success Email
    if ($user) {
        return view('index');
    } else {
        return redirect()->back()->withErrors(['mobile' => trans('A Network Error occurred. Please try again.')]);
    }

}



}
