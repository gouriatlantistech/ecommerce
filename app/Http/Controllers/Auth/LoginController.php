<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Session;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;

    public function redirectTo()
    {
 
//echo $_POST["role"];exit; 
    if($_POST["role"]==Auth::user()->role){

        switch(Auth::user()->role){
            case 'ADMIN':
              
                $this->redirectTo = '/';
            return $this->redirectTo;
                break;
            case 'USER':



                if(Auth::user()->usertype=='USER')
                {
                    $this->redirectTo = '/';
                    return $this->redirectTo;
                    break;

                }
                else{
                    if(Auth::user()->status=='YES')
                    {
                        $this->redirectTo = '/';
                        return $this->redirectTo;
                        break;

                    }
                    else{
                        Auth::logout();
                        Session::flash('message', 'Please wait for admin Approval'); 
                        $this->redirectTo = '/login';
                    return $this->redirectTo;
                    }

                    
                }
                
         
            default:
                $this->redirectTo = '/';
                return $this->redirectTo;
        }
        
    }else{
      
        Auth::logout();
       
        switch($_POST["role"]){
            case 'ADMIN':
                Session::flash('message', 'This is a Admin Dashboard. Here only admin can login and you not admin!'); 
                $this->redirectTo = '/admin_login';
            return $this->redirectTo;
                break;
            case 'USER':
                Session::flash('message', 'This is a User Dashboard. Here only User can login and you not a User!'); 
          
                $this->redirectTo = '/login';
                return $this->redirectTo;
                break;
         
            default:
                $this->redirectTo = '/login';
                return $this->redirectTo;
       }
   }



        // return $next($request);
    } 






    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
