<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\cart;
use App\none_cart;
use App\subscriber;
use App\wishlist;
use Illuminate\Support\Facades\Mail;
use App\user_wallet;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    // public function __construct()
    // {
    //     $this->middleware('verified');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }
    public function shop(Request $req)
    {   
   
        $query=DB::table('products')->join('brands','brands.brand_id','=','products.brand_id')->where('products.active_status','=','YES');
        $query2=DB::table('products')->join('product_prices','product_prices.product_id','=','products.product_id')->where('products.active_status','YES');

        if(isset($_GET['cat_id'])){
            $query=$query->where('products.cat_id','=',$req->cat_id);
            $query2=$query2->where('products.cat_id','=',$req->cat_id);
        }
        if(isset($_GET['sub_cat_id'])){
            $query=$query->where('products.sub_cat_id','=',$req->sub_cat_id);
            $query2=$query2->where('products.sub_cat_id','=',$req->sub_cat_id);
        }
        if(isset($_GET['sub_sub_cat_id'])){
            $query=$query->where('products.sub_sub_cat_id','=',$req->sub_sub_cat_id);
            $query2=$query2->where('products.sub_sub_cat_id','=',$req->sub_sub_cat_id);
        }

        $brand=$query->groupBy('products.brand_id')->inRandomOrder()->get();
        $count_brand_product=$query->join('product_prices','product_prices.product_id','=','products.product_id')->count();
        $product_size=$query2->groupBy('product_prices.size')->inRandomOrder()->get();
        $product_size_count=$query2->groupBy('product_prices.size')->count();

        $brand1=$query->groupBy('products.brand_id')->inRandomOrder()->get();
        $count_brand_product1=$query->groupBy('products.brand_id')->count();
        $product_size1=$query2->groupBy('product_prices.size')->inRandomOrder()->get();
        $count_brand_product1=$query->count();

        
        return view('shop')->with('brand',$brand)->with('product_size',$product_size)->with('count_brand_product',$count_brand_product)->with('product_size_count',$product_size_count)
        ->with('brand1',$brand)->with('product_size1',$product_size)->with('count_brand_product1',$count_brand_product)->with('product_size_count1',$product_size_count);
   
    }




    public function fetch_data(Request $req)
    {


        
        $user_type=$req->user_type;
        $price=0;
        $query=DB::table('products')->join('product_prices','products.product_id','=','product_prices.product_id')->where('active_status','YES');
        if($req->cat_id!=0){
            $query=$query->where('products.cat_id','=',$req->cat_id);
        }
        if($req->sub_cat_id!=0){
            $query=$query->where('products.sub_cat_id','=',$req->sub_cat_id);
        }
        if($req->sub_sub_cat_id!=0){
            $query=$query->where('products.sub_sub_cat_id','=',$req->sub_sub_cat_id);
        }
       
        if(!empty($req->Product_name)){
           
            $query=$query->where('products.product_name', 'LIKE', '%' . $req->Product_name . '%');
        }
        if(isset($req->brand))
        {
      
                $query=$query->join('brands','brands.brand_id','=','products.brand_id')->whereIn('products.brand_id', $req->brand);
              
         
        
        }
        if(($req->minimum_price!=0 || $req->maximum_price!=0))
        {
           
            if($user_type=='MEDIATOR'){

                  // echo $_POST["minimum_price"]."-".$_POST["maximum_price"];
                  $query =$query->where('product_prices.selling_price','>=',$req->minimum_price)
              ->where('product_prices.selling_price','<=',$req->maximum_price);
              
            }else{
                // echo $_POST["minimum_price"]."-".$_POST["maximum_price"];
                $query =$query->where('product_prices.mediator_price','>=',$req->minimum_price)
            ->where('product_prices.mediator_price','<=',$req->maximum_price);
            
               
            }
            $price=1;
           
          
        }
        if(isset($req->review))
        {
           

            $query=$query->where('products.review','>=', min($req->review));
        
        }
        if(isset($req->size))
        {
            if(($req->minimum_price==0 && $req->maximum_price==0))
            {
              //  $query=$query->join('product_prices','products.product_id','=','product_prices.product_id');
            }
             $query=$query->whereIn('product_prices.size', $req->size);
             $price=1;
        }
      
      
         
            if($req->status=='latest')
            {
                $query =$query->orderby('products.product_id','desc');
                 
               
            }
            if($req->status=='popular')
            {
                $query =$query->orderby('products.View_count','desc');
               
            }
            if($req->status=='low')
            {
                if(($req->minimum_price==0 && $req->maximum_price==0))
                {
              //  $query =$query->join('product_prices','products.product_id','=','product_prices.product_id');
                }

                if($user_type=='MEDIATOR'){
                $query =$query->orderby('product_prices.mediator_price','asc');
                }else{
                    $query =$query->orderby('product_prices.selling_price','asc');
                }
                $price=1;
            }
            if($req->status=='high')
            {
                if(($req->minimum_price==0 && $req->maximum_price==0) && !isset($req->size))
                {
              //  $query =$query->join('product_prices','products.product_id','=','product_prices.product_id');
                }
                
                if($user_type=='MEDIATOR'){
                $query =$query->orderby('product_prices.mediator_price','desc');
                }else{
                    $query =$query->orderby('product_prices.selling_price','desc');
                }
               
            }
            if($req->status=='highretting')
            {
                $query =$query->orderby('products.total_review','asc');
                
               
            }
            if($req->status=='lowretting')
            {
                $query =$query->orderby('products.total_review','desc');
               
            }
        

        $pro=$query->inRandomOrder()->get();
           
        $pro_count=$query->count();
        return view('product_ajax')->with('pro',$pro)->with('pro_count',$pro_count)->with('price',$price);
        //return view('shop_view')->with('product',$pro)->with('count',$pro1);
    }

    public function product_details(Request $req)
    {
       $product_id=$req->id;
       $product_details=DB::table('products')->join('brands','products.brand_id','=','brands.brand_id')->where('product_id',$product_id)->first();
       $product_image=DB::table('product_images')->where('product_id',$product_id)->get();
       $product_feature=DB::table('product_features')->where('product_id',$product_id)->get();
       $product_specification=DB::table('product_spacifications')->where('product_id',$product_id)->get();
       $product_price=DB::table('product_prices')->where('product_id',$product_id)->get();
       $category_id= $product_details->cat_id;
       $related_product_details=DB::table('products')->where('cat_id',$category_id)->where('active_status','YES')->get();

       if(auth::check())
       {
       $user_id=auth::user()->id;
       $review_submit=DB::table('bookings')->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')->where('bookings.customer_id',$user_id)->where('book_multi_items.product_id',$product_id)->where('book_multi_items.order_status',4)->orWhere('book_multi_items.order_status', '=', 5)->orWhere('book_multi_items.order_status', '=', 6)->orWhere('book_multi_items.order_status', '=', 7)->count();
       $review_update=DB::table('reviews')->where('user_id',$user_id)->where('product_id',$product_id)->first();
       $review_update1=DB::table('reviews')->where('user_id',$user_id)->where('product_id',$product_id)->count();
     
       }
       else
       {
        $review_submit=0;
        $review_update=0;
        $review_update1=0;
       }
       
       $review_star1=DB::table('reviews')->where('product_id',$product_id)->where('review',1)->count();
       $review_star2=DB::table('reviews')->where('product_id',$product_id)->where('review',2)->count();
       $review_star3=DB::table('reviews')->where('product_id',$product_id)->where('review',3)->count();
       $review_star4=DB::table('reviews')->where('product_id',$product_id)->where('review',4)->count();
       $review_star5=DB::table('reviews')->where('product_id',$product_id)->where('review',5)->count();
       $revie=DB::table('reviews')->where('product_id',$product_id)->limit(5)->orderBy('review', 'DESC')->get();

    
    


     return view('product_details',['product_details'=>$product_details,'product_image'=>$product_image,'product_feature'=>$product_feature,'product_specification'=>$product_specification,'product_price'=>$product_price,'related_product_details'=>$related_product_details,'review_submit'=>$review_submit,'review_update'=>$review_update,'review_update1'=>$review_update1,'review_star1'=>$review_star1,'review_star2'=>$review_star2,'review_star3'=>$review_star3,'review_star4'=>$review_star4,'review_star5'=>$review_star5,'revie'=>$revie]);

    }

    function price(Request $req)
    {
        $token=$req->_token;
        $price_id=$req->price_id;
        $product_price1=DB::table('product_prices')->where('product_prices_id',$price_id)->first();
        return view('price',['product_price'=>$product_price1]);

    }

      
    public function check_out()
    {
        return view('check_out');
    }
    public function cart()
    {
        return view('cart');
    }
    public function wishlist()
    {
        return view('wishlist');
    }

    public function sendOtp1(Request $request){

         //$otp = rand(1000,9999);
         $otp=1234;
       
         echo  $otp;
    }

       public function sendOtp11(Request $request){

        //$otp = rand(1000,9999);
        $otp=1234;
      
        echo  $otp;
   }


   public function cart_ajax()
    {
        
    if (Auth::check()) {
    $user_id=Auth::user()->id;
    $cart_count=DB::table('carts')->where('user_id',$user_id)->count();
    $cart_item=DB::table('carts')->where('user_id',$user_id)->join('products','carts.product_id','=','products.product_id')->orderBy('carts.id', 'desc')->limit(3)->get();

    return view('layouts.cart_ajax')->with('cart_count',$cart_count)->with('cart_items',$cart_item)->with('cart_item1',$cart_item);
        }


        else{
            $user_id=0;
            return view('layouts.cart_ajax');

        }  
    }
    public function wishlist_ajax()
    {
        if (Auth::check()) {
            $user_id=Auth::user()->id;
            $wish_count=DB::table('wishlists')->where('user_id',$user_id)->count();
           // $cart_item=DB::table('carts')->where('user_id',$user_id)->join('products','carts.product_id','=','products.product_id')->join('users','products.vendor_id','=','users.id')->get();
        
            return view('layouts.wishlist_ajax')->with('wish_count',$wish_count);
                }
                else{
                    $user_id=0;
                    return view('layouts.wishlist_ajax');
        
                }
                
                
        
         
    }

    public function addtocart_ajax(Request $req)
    {
        
            $user=Auth::user()->id;
            $product=$req->product_id;
            $product_price_id=$req->product_price_id;
            $pro=DB::table('carts')->where('product_id',$product)->where('product_price_id',$product_price_id)->where('user_id',$user)->count();

            if($pro<1){
                    $item = new cart;
                    $item->user_id=Auth::user()->id;
                    $item->product_id=$req->product_id;
                    $item->product_price_id=$req->product_price_id;
                    $item->quantity=1;
                    $item->remember_token=$req->input('_token');
                    $item->save();
                    //return view('layouts.wishlist_ajax');
                }else{
                    echo 2;
                }
                }


    public function add_to_cart_ajax1(Request $req)
    {
                $user=Auth::user()->id;
                $product=$req->product_id;
                $product_price_id=$req->product_price_id;
                $quantity=$req->quantity;
                $pro=DB::table('carts')->where('product_id',$product)->where('product_price_id',$product_price_id)->where('user_id',$user)->count();

            if($pro<1){
                $item = new cart;
                $item->user_id=Auth::user()->id;
                $item->product_id=$req->product_id;
                $item->product_price_id=$req->product_price_id;
                $item->quantity=$quantity;
                $item->remember_token=$req->input('_token');
                $item->save();
                //return view('layouts.wishlist_ajax');
            }else{
                echo 2;
            }
    }


    public function removetocart_ajax(Request $req)
    {
        
        $user=Auth::user()->id;
        $product=$req->product_id;
        $product_price_id=$req->product_price_id;
        $pro=DB::table('carts')->where('product_id',$product)->where('product_price_id',$product_price_id)->where('user_id',$user)->delete();


    }
    public function addtowishlist_ajax(Request $req)
    {
        
        $user=Auth::user()->id;
        $product=$req->product_id;
        $product_price_id=$req->product_price_id;
        $pro=DB::table('wishlists')->where('product_id',$product)->where('product_price_id',$product_price_id)->where('user_id',$user)->count();

  if($pro<1){
        $item = new wishlist;
        $item->user_id=Auth::user()->id;
        $item->product_id=$req->product_id;
        $item->product_price_id=$req->product_price_id;
        $item->quantity=1;
        $item->remember_token=$req->input('_token');
        $item->save();
    }else{
        echo 2;
    }
    }
     
    public function addtowishlist_ajax1(Request $req)
    {
        
        $user=Auth::user()->id;
        $product=$req->product_id;
        $product_price_id=$req->price_id;
        $qty=$req->qty;
        $pro=DB::table('wishlists')->where('product_id',$product)->where('product_price_id',$product_price_id)->where('user_id',$user)->count();
  if($pro<1){
        $item = new wishlist;
        $item->user_id=Auth::user()->id;
        $item->product_id=$req->product_id;
        $item->product_price_id=$product_price_id;
        $item->quantity=$qty;
        $item->remember_token=$req->input('_token');
        $item->save();
    }else{
        echo 2;
    }
    }





    public function removetowishlist_ajax(Request $req)
    {
        
        $user=Auth::user()->id;
        $product=$req->product_id;
        $product_price_id=$req->product_price_id;
        $pro=DB::table('wishlists')->where('product_id',$product)->where('product_price_id',$product_price_id)->where('user_id',$user)->delete();


    }


    public function toggle( Request $qp)
    {
            $product_id=$qp->product_id;
            $price_id=$qp->price_id;
            $token=$qp->input('_token');
            if (Auth::check()) {
            $user_id=Auth::user()->id;
            $store=DB::table('carts')->where('product_id',$product_id)->where('product_price_id', $price_id)->where('user_id',$user_id)->count();
            }
            else{
                $store=DB::table('carts')->where('product_id',$product_id)->where('product_price_id', $price_id)->count();
       

            }
                if($store>0)
                {
                $abc=1;
                return view('toggle')->with('abc',$abc);

                }else{
                    $abc=2;
                    return view('toggle')->with('abc',$abc);
                }
    }
    public function toggle_wish( Request $qp)
  { 
    $product_id=$qp->product_id;
    $price_id=$qp->price_id;
    $token=$qp->input('_token');
    if (Auth::check()) {
    $user_id=Auth::user()->id;
    $store=DB::table('wishlists')->where('product_id',$product_id)->where('product_price_id',$price_id)->where('user_id',$user_id)->count();
    }
    else
    {
        $store=DB::table('wishlists')->where('product_id',$product_id)->where('product_price_id',$price_id)->count();
    
    }
    if($store>0)
    {
     $abc=1;
      return view('toggle_wesh')->with('abc',$abc);

    }else{
        $abc=2;
        return view('toggle_wesh')->with('abc',$abc);
    }
  }

    
public function subscribe(Request $req)
{   if (Auth::check()) { 
    $id=Auth::user()->id;
    $email=$req->input('email');
    $customer=DB::table('users')->where('email',$email)->first();
    $customer_count=DB::table('users')->where('id',$id)->count();
    if($customer_count=='0')
    {
        echo 1;  
    }
    else
    {
        echo 2;
      
       $subs=new subscriber;
        $subs->user_id=$id;
      $subs->remember_token=$req->input('_token');
        $subs->save();
      
    }
}else{
    echo 3;  
}
} 

public function unsubscribe(Request $req)
{    
    $id=Auth::user()->id;
    $email=$req->input('email');
    $customer=DB::table('users')->where('email',$email)->first();
    $customer_count=DB::table('users')->where('id',$id)->count();
    if($customer_count=='0')
    {
        echo 1;  
    }
    else
    {  

        
        Db::table('subscribers')->where('user_id',$id)->delete();
       echo 2; 
    }
    
} 



public function add_to_noe_cart(Request $req)
{
            $user=Auth::user()->id;
            $product=$req->product_id;
            $product_price_id=$req->product_price_id;
            $quantity=$req->quantity;
            $pro=DB::table('none_carts')->where('product_id',$product)->where('product_price_id',$product_price_id)->where('user_id',$user)->count();

        if($pro<1){
            $item = new none_cart;
            $item->user_id=Auth::user()->id;
            $item->product_id=$req->product_id;
            $item->product_price_id=$req->product_price_id;
            $item->quantity=$quantity;
            $item->remember_token=$req->input('_token');
            $item->save();
            //return view('layouts.wishlist_ajax');
        }else{
            DB::table('none_carts')
        ->where('user_id', $user)->where('product_id',$req->product_id)->where('product_price_id',$req->product_price_id)
        ->update(['quantity' => $quantity]);

        }
}
public function pinckeck(Request $req)
{

   $pin=$req->pin;
$store=DB::table('pincodes')->where('active_status','YES')->where('pincode',$pin)->count();
if($store==0){
echo "1";
}else{
echo "2";
}
    
}


public function wallet(Request $req)
{
    if(Auth::check()){

    
   $token=$req->_token;
   $user=Auth::user()->id;
   $store=DB::table('user_wallets')->where('user_id',$user)->count();
   if($store==0)
   {
    $user_wallet=new user_wallet;
    $user_wallet->user_id=Auth::user()->id;
    $user_wallet->wallet_ammount=0;
    $user_wallet->remember_token=$token;
    $user_wallet->save();

   }
 
}
}


public function fafasend(Request $req)
{ 
    $review=$req->review1;
 
    $token=$req->_token;
   
   return view('fafa')->with('review',$review);
}
public function contact(Request $req)
{ 
     return view('contact');
}

public function about(Request $req)
{ 
     return view('about');
}

public function contactmail(Request $d)
{
    $name=$d->name;
    $user_email=$d->email;
    $email='support@ssjewelers.com';
    $subject=$d->subject;
    $message=$d->message;
    try{
        $data = array('name'=>$name,'email'=>$email,'body'=>"Hii admin, A user send a Customar Support request. User Name : $name, Email : $user_email, Subject : $subject, Message : $message");


        Mail :: send('mail',$data,function($message) use ($name,$email)
        {
        $message->to($email)
        ->subject('Customar Support Request');
        
        });
        echo '1';
    } catch (\Exception $e) {
    echo 0;
}


    
}

public function faq(Request $req)
{ 
     return view('faq');
}
public function privacy(Request $req)
{ 
     return view('privacy');
}
public function trams(Request $req)
{ 
     return view('trams');
}
public function return(Request $req)
{ 
     return view('return');
}

public function offer(Request $req)
{ 
    
    
    $pq=DB::table('coupon_banners')->where('active_status','YES')->get();

    return view('offer')->with('coupon',$pq);
  
}
}
