<?php

namespace App\Http\Controllers;
use App\product_size;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Image;
use Auth;
use App\product;
use App\product_image;
use App\product_features;
use Illuminate\Support\Facades\Mail;
use App\cat;
use App\product_spacification;
use App\product_price;
use App\stock_trasanction;



class add_product extends Controller
{


    public function formValidation()
    {
      return view('form-validation');
    }

    public function product_action(Request $req)
    {


      $this->validate($req,[
        'hsn' => 'required',
        'product_cat_id' => 'required',
        'product_name' => 'required',
        'brand' => 'required',
        'editor1' => 'required',
        'image_name' => 'required',
        'size' => 'required',
        'mrp' => 'required',
        'selling_price' => 'required',
        'mediator_price' => 'required',
        'qty' => 'required'
      ],[
        'product_cat_id.required' => ' The Category field is Required.',
        'product_name.required' => ' The Product Name field is Required.',
        'brand.required' => ' The brand field is Required.',
        'editor1.required' => ' The Description field is Required.',
        'image_name.mimes' => ' The Image  Must be must be jpeg,png,jpg,gif,svg.',
        'image_name.max' => ' You Can Upload Maximum 2Kb Image.',
        
      ]);


        
       
     $product = new product;
     $vendor_id=Auth::user()->id;
     $product->product_code=$req->input('product_code');
     $product->hsn_code=$req->input('hsn');
     $product->product_name=$req->input('product_name');
     $product->cat_id=$req->input('product_cat_id');
     $product->sub_cat_id=$req->input('product_sub_cat_id');
     $product->sub_sub_cat_id=$req->input('product_sub_sub_cat_id');
    
     $product->brand_id=$req->input('brand');
     $product->description=$req->input('editor1');
     $token=$req->input('_token');
     $product->remember_token=$token;
     $product->save();
     $product_id=$product->id;

     /*
     DB::table('products')->where('product_code',$code)->get()->first();
     $product_id->product_id;

*/

  $specification_name=$req->input('specification1');
  $specification_type=$req->input('specification');

 
     
if(isset($specification_name)){

for($i=0;$i<count($specification_name);$i++)
{

    $specification= new product_spacification;
    $specification_name1=$specification_name[$i];
    $specification_type1=$specification_type[$i];
    $specification->title=$specification_name1;
    $specification->description=$specification_type1;

    $specification->product_id=$product_id;
    $specification->remember_token=$token;
    $specification->save();
}
}
        
        $feture=$req->input('fetuars');
     
        if(isset($feture)){

        for($i=0;$i<count($feture);$i++)
        {

            $features= new product_features;
            $feature1=$feture[$i];
            $features->features=$feature1;

            $features->product_id=$product_id;
            $features->remember_token=$token;
            $features->save();
        }
    }



     

      //  DB::table('features')->insert(['product_id' =>$product_id,'feature_name'=> $fetuars,'remember_token'=>$token]);



     
    /*  $p_type=$req->input('size');
      if($p_type[0]!=''){
     for($i=0;$i<count($p_type);$i++)
     {
         $for_size= new product_size;
         $for_size->product_id=$product_id;
         $for_size->product_size=$p_type[$i];
         $for_size->rememeber_token=$token;
         $for_size->save();
     }

    
    }*/











     
  if($req->hasFile('image_name'))
	{	
     $image_array3=$req->file('image_name');
     
    
  for($i=0;$i<count($image_array3);$i++)
  {




    
    
      $image_name = uniqid() . '.' . $image_array3[$i]->getClientOriginalExtension();


        $destinationPath = public_path('/product_image');
        $destinationPath1 = public_path('/small_product_image');
        $resize_image = Image::make($image_array3[$i]->getRealPath());
     $resize_image->resize(1000,1000, function($constraint){
     })->save($destinationPath . '/' . $image_name);


    $resize_image->resize(255,255, function($constraint){
    })->save($destinationPath1 . '/' . $image_name);






    // $image_ext3=$image_array3[$i]->getClientOriginalExtension();
            
    // $new_image_name3=rand(123456,999999).".".$image_ext3;
    
    // $destination_path3=public_path("/product_image/");
    // $image_array3[$i]->move($destination_path3,$new_image_name3);





   
     DB::table('product_images')->insert(['product_id' =>$product_id,'image'=>$image_name,'remember_token'=>$token]);
  }
    }


   $size=$req->input('size');
   $mrp=$req->input('mrp');
   $selling_price=$req->input('selling_price');
   $mediator_price=$req->input('mediator_price');
 

   $available_stock=$req->input('qty');


for($i=0;$i<count($size);$i++)
{
   $for_size= new product_price;
   $for_size->size=$size[$i];
   $for_size->mrp=$mrp[$i]; 
   $for_size->selling_price=$selling_price[$i];
   $for_size->mediator_price=$mediator_price[$i];
   $for_size->available_stock=$available_stock[$i];
   $for_size->total_stock=$available_stock[$i];

   $for_size->product_id=$product_id;
   $for_size->save();
  
   $size_id=$for_size->id;

   $stock= new stock_trasanction;
   $stock->product_id=$product_id;
   $stock->product_price_id=$size_id; 
   $stock->quantity=$available_stock[$i];
   $stock->type='CREDIT';
   $stock->remarks='INITIAL ADD';
   $stock->transfer_id=0;
   $stock->total_quantity=$available_stock[$i];
 
   $stock->save();

}  

        /* for($i=0;$i<$size;$i++)
         {
             $new_size=new product_size;
             $new_size->product_id=$product_id;
             $new_size->product_size=$size[$i];
             $new_size->save();
         }*/
        /* $product=DB::table('products')->join('cats','products.product_cat_id','=','cats.cat_id')->join('sub_cats','products.product_sub_cat_id','=','sub_cats.sub_cat_id')->join('sub_sub_cats','products.product_sub_sub_cat_id','=','sub_sub_cats.sub_sub_cat_id')->where('products.vendor_id',$vendor_id)->get();
         return view('/vendor/view_product')->with('product',$product);*/
         return redirect('/view_product');
         
   
    }

  

}
