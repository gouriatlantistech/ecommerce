<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use App\cat;
use App\sub_cat;
use App\sub_sub_cat;
use App\product_image;
use App\brand;
use Image;
use App\user_wallet_transaction;
use App\user_wallet;

use App\display_banner;
use App\pincode;
use App\coupon_banner;
use App\product_features;
use App\product_spacification;
use Illuminate\Support\Facades\Mail;
use App\stock_trasanction;
use PDF;
class AdminController extends Controller
{
 

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin_middleware');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index(){
      $total_booking=DB::table('bookings')->count();
      $total_product=DB::table('products')->count();
      $normal_user=DB::table('users')->where('role','USER')->where('usertype','USER')->count();
      $madiator=DB::table('users')->where('role','USER')->where('usertype','MEDIATOR')->count();

      $pending_booking=DB::table('bookings')
      ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
      ->where('book_multi_items.order_status',1)
      ->orwhere('book_multi_items.order_status',2)
      ->orwhere('book_multi_items.order_status',3)
      ->count();

      $return_booking=DB::table('bookings')
      ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
      ->where('book_multi_items.order_status',5)
      ->orwhere('book_multi_items.order_status',6)

      ->count();

      $cancel_booking=DB::table('bookings')
      ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
      ->where('book_multi_items.order_status',8)
      
      ->count();


        return view('admin.index')
        ->with('total_booking',$total_booking)
        ->with('total_product',$total_product)
        ->with('normal_user',$normal_user)
        ->with('madiator',$madiator)
        ->with('pending_booking',$pending_booking)
        ->with('return_booking',$return_booking)
        ->with('cancel_booking',$cancel_booking);
    }
    protected function admin_logout(){

        Auth::logout();
        return redirect()->route('admin_login');
        //return view('vendor.auth.login');
    }
    public function developer_setting(){
        return view('admin.developer_setting');
    }
    public function display_banner()
    {
        
       $banner=DB::table('display_banners')->get();
       return view('admin.display_banner')->with('banner',$banner);
    }
    public function add_display_banner(Request $req)
    {
        $image=$req->file('m_pic');


        $image_name = uniqid() . '.' . $image->getClientOriginalExtension();

        $destinationPath = public_path('banner');
        
        $resize_image = Image::make($image->getRealPath());
        
        $resize_image->resize(416,420, function($constraint){
            
        })->save($destinationPath . '/' . $image_name);
              
   
   
        $my_image=new display_banner;
        $my_image->display_banner=$image_name;
        
        $my_image->remember_token = $req->input('_token');
        $my_image->save();
        return redirect()->back();
    }
    public function delete_display_banner()
    {
        $id=$_GET['id'];
        try{

        
        $image2=DB::table('display_banners')->where('dis_id',$id)->first();
        $link="banner/".$image2->display_banner;
        unlink($link);
      }
      catch (\Exception $e)
     {
          
     }
        $delete_query=DB::table('display_banners')->where('dis_id',$id)->delete();
        return redirect()->back();
   
    }


    //coupon
public function view_coupon()
{
    $coupon=DB::table('coupon_banners')->get();
    return view('admin.view_coupon')->with('coupon',$coupon);
}


public function add_coupon()
{

return view('admin.add_coupon');

}


public function add_coupon_action(Request $req)
 {
   
   $title=$req->coupon_title;
   $coupon_value=$req->coupon_value;
   $coupon_validity=$req->coupon_validity;
   $min_price=$req->min_price;
  // $code=uniqid();
  // $coupon_code2=substr(md5($code), 0, 8);

  $coupon_code3=$req->coupon_code;
   $coupon_code=strtoupper($coupon_code3);

   $cat_id=$req->cat_id;
   $coupon_type=$req->coupon_type;


    $image=$req->file('coupon_image');
 /*  $image_ext1=$image_array->getClientOriginalExtension();
   $new_image_name1=rand(123456,999999).".".$image_ext1;
   $destination_path1=public_path("/coupon_image/");
   $image_array->move($destination_path1,$new_image_name1);*/


   $image_name = uniqid() . '.' . $image->getClientOriginalExtension();

   $destinationPath = public_path('/coupon_image');

   $resize_image = Image::make($image->getRealPath());

   $resize_image->resize(416,420, function($constraint){
   // $constraint->aspectRatio();
   })->save($destinationPath . '/' . $image_name);

  /* $destinationPath = public_path('/resize2');

   $image->move($destinationPath, $image_name);*/







   $coupon=new coupon_banner;
   $coupon->title=$title;
   $coupon->coupon_value=$coupon_value;
   $coupon->coupon_validity=$coupon_validity;
   $coupon->coupon_image=$image_name;
   $coupon->coupon_code=$coupon_code;
   $coupon->min_price=$min_price;
   $coupon->coupon_type=$coupon_type;
   $coupon->cat_id=$cat_id;
   $coupon->remember_token = $req->input('_token');
   $coupon->save();
   //$coupon=DB::table('coupons')->get();
   return redirect()->route('view_coupon');


 }

 public function update_coupon()
 {
 $coupon_id=$_GET['coupon_id'];

   $coupon=DB::table('coupon_banners')->where('id','=',$coupon_id)->first();
    
    return view('/admin/update_coupon')->with('coupon',$coupon);

 }


 public function update_coupon_action(Request $req)
 {
   $coupon_id=$req->input('coupon_id');
   $title=$req->input('coupon_title');
   $coupon_validity=$req->input('coupon_validity');
   $coupon_value=$req->input('coupon_value');
   $coupon_code2=$req->input('coupon_code');
   $min_price=$req->input('min_price');
   $coupon_code=strtoupper($coupon_code2);
   $cat_id=$req->cat_id;
   $coupon_type=$req->coupon_type;

   if($req->hasFile('coupon_image'))
   {
    $image =$req->file('coupon_image');

         /*  $image_ext1=$image_array1->getClientOriginalExtension();
           $new_image_name1=rand(123456,999999).".".$image_ext1;
           $destination_path1=public_path("/coupon_image/");
           $image_array1->move($destination_path1,$new_image_name1);*/



           $image_name = uniqid() . '.' . $image->getClientOriginalExtension();

   $destinationPath = public_path('/coupon_image');

   $resize_image = Image::make($image->getRealPath());

   $resize_image->resize(416,420, function($constraint){
   // $constraint->aspectRatio();
   })->save($destinationPath . '/' . $image_name);

  /* $destinationPath = public_path('/resize2');

   $image->move($destinationPath, $image_name);
*/

try{
           $image=DB::table('coupon_banners')->where('id','=',$coupon_id)->first();
           $link="coupon_image/".$image->coupon_image;
       
           unlink($link);
          }
          catch (\Exception $e)
         {
              
         }
         DB::table('coupon_banners')->where('id',$coupon_id)->update(['title' => $title,'coupon_validity'=>$coupon_validity,'coupon_value'=>$coupon_value,'coupon_image'=> $image_name,'coupon_code'=>$coupon_code,'min_price'=>$min_price,'cat_id'=>$cat_id,'coupon_type'=>$coupon_type]);
       }else{
         DB::table('coupon_banners')->where('id',$coupon_id)->update(['title' => $title,'coupon_validity'=>$coupon_validity,'coupon_value'=>$coupon_value,'coupon_code'=>$coupon_code,'min_price'=>$min_price,'cat_id'=>$cat_id,'coupon_type'=>$coupon_type]);
       }


//$coupon=DB::table('coupons')->get();
return redirect()->route('view_coupon');


      }
      public function delete_coupon()
      {
      $coupon_id=$_GET['coupon_id'];
     try{

    
      $image=DB::table('coupon_banners')->where('id','=',$coupon_id)->first();
      $link="coupon_image/".$image->coupon_image;
  
      unlink($link);
    }
    catch (\Exception $e)
   {
        
   }
      $image=DB::table('coupon_banners')->where('id','=',$coupon_id)->delete();
        return redirect()->route('view_coupon');
         
     
      }
     
      
public function admin_pincode()
{
    $pin=DB::table('pincodes')->get();
    return view('admin.pincode')->with('pin',$pin);

}
public function admin_add_pincode()
{
    
    return view('admin.add_pincode');
}

public function admin_add_pincode_code(Request $req){
    $pin=$req->input('pin');

    $minimum_order=$req->input('minimum_order');
    $charge=$req->input('charge');
    
  
    for($i=0;$i<count($pin);$i++)
    {
    $pincode = new pincode;
    $pincode->pincode =$pin[$i];
    
    $pincode->min_price =$minimum_order[$i];
    $pincode->delivery_charge=$charge[$i];
    
    $pincode->remember_token = $req->input('_token');
    $pincode->save();
    }
  
    return redirect()->route('pincode');
   
  
  }
  public function admin_update_pincode()
  {
      $id=$_GET['id'];
      $pin=DB::table('pincodes')->where('id',$id)->first();
      return view('admin.update_pincode')->with('pin',$pin);
  }

  public function admin_update_pincode_code(Request $req){

    $id=$req->input('pin_id');

    $delivary_charge=$req->input('delivary_charge');
    $minimum_order=$req->input('minimum_order');
    
    
   
    
    DB::table('pincodes')
    ->where('id', $id)
    ->update(['delivery_charge' => $delivary_charge,'min_price'=>$minimum_order]);
    
      return redirect()->route('pincode');
  }

  public function pincode_status(Request $req){

    $id=$req->id;
  
  
  if($req->s=='y'){
    DB::table('pincodes')
    ->where('id', $id)
    ->update(['active_status' => 'NO']);
  }else{
    DB::table('pincodes')
    ->where('id', $id)
    ->update(['active_status' => 'YES']);
  }
  
    return redirect()->route('pincode');
   
  }
public function delete_pincode(Request $req)
{
    $id=$req->id;
    DB::table('pincodes')->where('id', $id)->delete();
    return redirect()->route('pincode');
}

public function normal_user(Request $req)
{
    
    $user=DB::table('users')->where('role', 'USER')->where('usertype', 'USER')->get();
    return view('admin.normal_user')->with('user',$user);
}
public function mediator_user(Request $req)
{
    $user=DB::table('users')->where('role', 'USER')->where('usertype', 'MEDIATOR')->get();
    return view('admin.mediator_user')->with('user',$user);
}
public function subscribe_user(Request $req)
{
    $user=DB::table('users')->where('role', 'USER')->where('subscribe', 'YES')->get();
    return view('admin.mediator_user')->with('user',$user);
}
public function user_block(Request $req)
{
    
    $id=$req->id;
    if($req->value=='true'){
        $value='YES';
    }else{
       $value='NO';
    }
   DB::table('users')->where('id',$id)->update(['block'=>$value]);
   //return view('/admin/view_user')->with('users',$users);

}
public function mediator_approve(Request $req)
{
    
    $id=$req->id;
    if($req->value=='true'){
        $value='YES';
    }else{
       $value='NO';
    }
   DB::table('users')->where('id',$id)->update(['status'=>$value]);
   return redirect()->back();

}
public function db_backup()
{
    
    
    function backup_tables($host, $user, $pass, $dbname, $tables = '*') {
    $link = mysqli_connect($host,$user,$pass, $dbname);
    
    // Check connection
    if (mysqli_connect_errno())
    {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    exit;
    }
    
    mysqli_query($link, "SET NAMES 'utf8'");
    
    //get all of the tables
    if($tables == '*')
    {
    $tables = array();
    $result = mysqli_query($link, 'SHOW TABLES');
    while($row = mysqli_fetch_row($result))
    {
    $tables[] = $row[0];
    }
    }
    else
    {
    $tables = is_array($tables) ? $tables : explode(',',$tables);
    }
    
    $return = '';
    //cycle through
    foreach($tables as $table)
    {
    $result = mysqli_query($link, 'SELECT * FROM '.$table);
    $num_fields = mysqli_num_fields($result);
    $num_rows = mysqli_num_rows($result);
    
    //$return.= 'DROP TABLE IF EXISTS '.$table.';';
    $row2 = mysqli_fetch_row(mysqli_query($link, 'SHOW CREATE TABLE '.$table));
    $return.= "\n\n".$row2[1].";\n\n";
    $counter = 1;
    
    //Over tables
    for ($i = 0; $i < $num_fields; $i++)
    {   //Over rows
    while($row = mysqli_fetch_row($result))
    {  
    if($counter == 1){
    $return.= 'INSERT INTO '.$table.' VALUES(';
    } else{
    $return.= '(';
    }
    
    //Over fields
    for($j=0; $j<$num_fields; $j++)
    {
    $row[$j] = addslashes($row[$j]);
    $row[$j] = str_replace("\n","\\n",$row[$j]);
    if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
    if ($j<($num_fields-1)) { $return.= ','; }
    }
    
    if($num_rows == $counter){
    $return.= ");\n";
    } else{
    $return.= "),\n";
    }
    ++$counter;
    }
    }
    $return.="\n\n\n";
    }
    
    //echo json_encode($return);
    //save file
    
    $fileName = 'database_backup\database_backup_'.time().'.sql';
    $handle = fopen($fileName,'w+');
    fwrite($handle,$return);
    if(fclose($handle)){
  
         $file = $fileName;

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
        
        }
      
      
    }
    
    }
    
    //MySQL server and database
    $dbhost = env('DB_HOST');
    $dbuser = env('DB_USERNAME');
    $dbpass = env('DB_PASSWORD');
    $dbname = env('DB_DATABASE');
    $tables = '*';
    
    //Call the core function
    backup_tables($dbhost, $dbuser, $dbpass, $dbname, $tables);
    
    
    
    
    
}
public function inventory_report()
{
  $product1=DB::table('products')->join('product_prices','products.product_id','=','product_prices.product_id')->get();
    return view('admin.inventory_report')->with('product',$product1);
}
public function add_product()
{
    $product1=DB::table('products')->select('product_code')->orderby('product_code','desc')->first();
    $product_count=DB::table('products')->select('product_code')->orderby('product_code','desc')->count();
    if($product_count==0){
        $product=sprintf("%06d",1);
    }else{
        $product=sprintf("%06d",$product1->product_code+1);
    }
    $cat=DB::table('cats')->get();
    $brand=DB::table('brands')->get();
    return view('admin.add_product')->with('cat',$cat)->with('brand',$brand)->with('product_code',$product);
}
public function view_product()
{
    $product=DB::table('products')->get();
    return view('admin.view_product')->with('product',$product);
}

public function category(){

    $cat=DB::table('cats')->get();
    return view('admin.category')->with('cat',$cat);
}
public function add_category(){

      return view('admin.add_category');
}
public function category_action(Request $req)
 {
    
     $cat1=$req->input('cat_name');
     $image_array3=$req->file('cat_image');
     for($i=0;$i<count($cat1);$i++)
     {


      $image_name = uniqid() . '.' . $image_array3[$i]->getClientOriginalExtension();


      $destinationPath = public_path('/cat_background');

      $resize_image = Image::make($image_array3[$i]->getRealPath());
   $resize_image->resize(500,400, function($constraint){
   })->save($destinationPath . '/' . $image_name);



  
     $contact = new cat;
     $contact->cat_name =$cat1[$i];
     $contact->cat_background =$image_name;
    
     $contact->remember_token = $req->input('_token');
     $contact->save();
     }

     return redirect('view_category')->with('success','Category Add Successfully!');
    
 }
 public function cat_update()
 {

    $cat_id=$_GET['cat_id'];
    
     
     

      $cat=DB::table('cats')->where('cat_id',$cat_id)->get();
      return view('admin.category_update')->with('cat',$cat);
 }
 public function cat_update_action(Request $req)
 {
   $cat_id=$req->input('cat_id');
   $cat_name=$req->input('cat_name');

  

      




        if($req->hasFile('cat_image'))
        {
                $image_array1=$req->file('cat_image');
              /* $image_ext1=$image_array1->getClientOriginalExtension();
                $new_image_name1=rand(123456,999999).".".$image_ext1;
                $destination_path1=public_path("/cat_icon/");
                $image_array1->move($destination_path1,$new_image_name1);
     
     */
     
                $image_name = uniqid() . '.' . $image_array1->getClientOriginalExtension();
                $destinationPath = public_path('cat_background');
                $resize_image = Image::make($image_array1->getRealPath());
                $resize_image->resize(500,400, function($constraint){
                })->save($destinationPath . '/' . $image_name);
           
     
     
     
     try{
                         $image2=DB::table('cats')->where('cat_id',$cat_id)->first();
                        $link="cat_background/".$image2->cat_background;
                        unlink($link);
     }
     catch (\Exception $e)
    {
         
    }
                
                DB::table('cats')->where('cat_id',$cat_id)->update(['cat_name' => $cat_name,'cat_background' => $image_name]);
     
            }else{
              DB::table('cats')->where('cat_id',$cat_id)->update(['cat_name' => $cat_name]);
            }

















       return redirect('view_category')->with('success','Category Update Successfully!');
  


   
 }
 public function cat_delete()
 {

    $cat_id=$_GET['cat_id'];
 
    try{
      $image2=DB::table('cats')->where('cat_id',$cat_id)->first();
     $link="cat_background/".$image2->cat_background;
     unlink($link);
}
catch (\Exception $e)
{

}

      DB::table('cats')->where('cat_id','=',$cat_id)->delete();

      
      return redirect()->back()->with('success','Category Delete Successfully!');
 }

 public function sub_cat_delete()
 {

    $sub_cat_id=$_GET['sub_cat_id'];


      DB::table('sub_cats')->where('sub_cat_id','=',$sub_cat_id)->delete();

      $sub_sub_cat=DB::table('sub_cats')->get();

      return redirect()->back()->with('success','Sub category Delete Successfully!');
    
 }
 public function sub_category_action(Request $req)
 {

     $cat_name=$req->input('cat_id');
     $sub_cat=$req->input('sub_cat_name');
    
     $gst=$req->input('gst');
   
     for($i=0;$i<count($sub_cat);$i++)
     {
    
     $contact = new sub_cat;
     $contact->cat_id =$cat_name;
     $contact->sub_cat_name =$sub_cat[$i];

     $contact->gst =$gst[$i];
    
     $contact->remember_token = $req->input('_token');
     $contact->save();
     }

     $sub_sub_cat=DB::table('sub_cats')->get();
     
     $cat=DB::table('cats')->get();
     return redirect('view_sub_category?cat_id='.$cat_name)->with('success','Sub category Add Successfully!');
  
 }
 public function add_sub_category()
 {
     if(isset($_GET['cat_id']))
     {
       $cat=$_GET['cat_id'];
         $cat=DB::table('cats')->where('cat_id',$cat)->get();
        
     }
     else
     {
         $cat=DB::table('cats')->get();
     }
     return view('admin.add_sub_category')->with('cat',$cat);  
 }
 public function sub_cat_update()
 {

    $sub_cat_id=$_GET['sub_cat_id'];


      $sub_cat=DB::table('sub_cats')->where('sub_cat_id','=',$sub_cat_id)->get();
      return view('admin.sub_category_update')->with('sub_cat',$sub_cat);
     
 }
 
 public function sub_cat_update_action(Request $req)
 {
    $sub_cat_id=$req->input('sub_cat_id');
   $sub_cat_name=$req->input('sub_cat_name');

   $gst=$req->input('gst');


   DB::table('sub_cats')->where('sub_cat_id',$sub_cat_id)->update(['sub_cat_name' => $sub_cat_name,'gst' => $gst]);
     



       $ca=DB::table('sub_cats')->where('sub_cat_id',$sub_cat_id)->first();;
       
       return redirect('view_sub_category?cat_id='.$ca->cat_id)->with('success','Sub category Update Successfully!');


 
 }
 public function view_sub_category(Request $req)
 {
 
 $cat_id=$req->cat_id;
 $cat_name=DB::table('cats')->where('cat_id',$cat_id)->first();
   $sub_cat=DB::table('sub_cats')->where('cat_id',$cat_id)->get();
 
 return view('admin.view_sub_category')->with('cats',$sub_cat)->with('cat_name',$cat_name);
 
 }
 public function sub_cat_ajax()
{
    return view('/admin/sub_cat_ajax');
}
public function sub_sub_cat_ajax()
{
    return view('/admin/sub_sub_cat_ajax');
}
public function add_sub_sub_category()
{
   
      $sub_sub_cat=$_GET['sub_sub_cat'];
        $sub_sub_cat=DB::table('sub_cats')->where('sub_cat_id',$sub_sub_cat)->get();
       
   


    return view('admin.add_sub_sub_category')->with('sub_sub_cat',$sub_sub_cat);
}

public function sub_sub_category_action(Request $req)
{
   $cat=$req->file('icon');
    $sub_cat_name=$req->input('sub_cat_id');
    $sub_sub_cat_name=$req->input('sub_sub_cat_name');
    for($i=0;$i<count($sub_sub_cat_name);$i++)
    {
     


    $contact = new sub_sub_cat;
    $contact->sub_cat_id =$sub_cat_name;
    $contact->sub_sub_cat_name =$sub_sub_cat_name[$i];

    $contact->remember_token = $req->input('_token');
    $contact->save();
    }
    $sub_sub_cat=DB::table('sub_sub_cats')->get();
    
    $cat=DB::table('cats')->get();
    return redirect('view_sub_sub_category?sub_cat_id='.$sub_cat_name)->with('success','Sub sub category Update Successfully!');
   }
   public function view_sub_sub_category(Request $req)
   {
   
     $sub_cat_id=$req->sub_cat_id;
     $cat_name=DB::table('cats')->join('sub_cats','sub_cats.cat_id','=','cats.cat_id')->where('sub_cat_id',$sub_cat_id)->first();
     $sub_sub_cat=DB::table('sub_sub_cats')->where('sub_cat_id',$sub_cat_id)->get();
      
   return view('admin.view_sub_sub_category')->with('cats',$sub_sub_cat)->with('cat_name',$cat_name);
   
   }
   public function sub_sub_cat_update()
   {
      $sub_sub_cat_id=$_GET['sub_sub_cat_id'];
      $sub_sub_cat=DB::table('sub_sub_cats')->where('sub_sub_cat_id','=',$sub_sub_cat_id)->get();
      return view('admin.sub_sub_category_update')->with('sub_sub_cat',$sub_sub_cat);
   }
   public function sub_sub_cat_update_action(Request $req)
 {
   $sub_sub_cat_id=$req->input('sub_sub_cat_id');
    $sub_sub_cat_name=$req->input('sub_sub_cat_name');
 

   
       
            DB::table('sub_sub_cats')->where('sub_sub_cat_id',$sub_sub_cat_id)->update(['sub_sub_cat_name' => $sub_sub_cat_name]);
   
        



        $ca=DB::table('sub_sub_cats')->where('sub_sub_cat_id',$sub_sub_cat_id)->first();;
       
        return redirect('view_sub_sub_category?sub_cat_id='.$ca->sub_cat_id)->with('success','Sub sub category update Successfully!');
  



 }
   public function all_cats_deail()
   {
      $cats=DB::table('cats')->get();
  
       return view('/admin/all_cats_details')->with('cat',$cats);
   }
   public function sub_sub_cat_delete()
   {
  
      $sub_sub_cat_id=$_GET['sub_sub_cat_id'];
  
  
        DB::table('sub_sub_cats')->where('sub_sub_cat_id','=',$sub_sub_cat_id)->delete();
  
        $sub_sub_cat=DB::table('sub_sub_cats')->get();
  
  
       return redirect()->back()->with('success','Sub Sub category Delete Successfully!');
      
   }
  
   public function brand(Request $req)
   {
    $brands=DB::table('brands')->get();
    return view('/admin/brand')->with('brand',$brands);
   }
   public function add_brand(Request $req)
   {
    return view('/admin/add_brand');
   }
   public function add_brand_action(Request $req)
   {
    $cat1=$req->input('brand_name');
       $cat=$req->file('brand_image');
    
       for($i=0;$i<count($cat1);$i++)
       {
        $image_name = uniqid() . '.' . $cat[$i]->getClientOriginalExtension();
        $destinationPath = public_path('brand_logo');
        $resize_image = Image::make($cat[$i]->getRealPath());
        $resize_image->resize(200,60, function($constraint){
        })->save($destinationPath . '/' . $image_name);
   
        
       $contact = new brand;
       $contact->brand_name =$cat1[$i];
       $contact->brand_image =$image_name;
       $contact->remember_token = $req->input('_token');
       $contact->save();
       }
  
       return redirect('brand')->with('success','Brand Add Successfully!');
      
   }
   public function update_brand(Request $req)
   {
    $brands=DB::table('brands')->where('brand_id',$req->id)->first();
    return view('/admin/update_brand')->with('brand',$brands);
   }
   public function update_brand_action(Request $req)
   {
    $brand_name=$req->input('brand_name');
    $brand_id=$req->input('id');
   
   if($req->hasFile('brand_image'))
   {
           $image_array1=$req->file('brand_image');
         /* $image_ext1=$image_array1->getClientOriginalExtension();
           $new_image_name1=rand(123456,999999).".".$image_ext1;
           $destination_path1=public_path("/cat_icon/");
           $image_array1->move($destination_path1,$new_image_name1);

*/

           $image_name = uniqid() . '.' . $image_array1->getClientOriginalExtension();
           $destinationPath = public_path('brand_logo');
           $resize_image = Image::make($image_array1->getRealPath());
           $resize_image->resize(200,60, function($constraint){
           })->save($destinationPath . '/' . $image_name);
      



try{
           $image2=DB::table('brands')->where('brand_id',$brand_id)->first();
           $link="brand_logo/".$image2->brand_image;
           unlink($link);

          }
          catch (\Exception $e)
         {
              
         }
           DB::table('brands')->where('brand_id',$brand_id)->update(['brand_name' => $brand_name,'brand_image' => $image_name]);

       }else{
        DB::table('brands')->where('brand_id',$brand_id)->update(['brand_name' => $brand_name]);
       }


       return redirect('brand')->with('success','Brand Update Successfully!');
   }
   public function delete_brand(Request $req)
   {
    $brand_id=$req->input('id');
    $image2=DB::table('brands')->where('brand_id',$brand_id)->first();
    $link="brand_logo/".$image2->brand_image;
    unlink($link);
    $brands=DB::table('brands')->where('brand_id',$brand_id)->delete();
    return redirect('brand')->with('success','Brand Delete Successfully!');
   }

   public function approve1(Request $qq)
    {
        
        $product_id=$qq->product_id;
        $status=$qq->y;
        if($status=='yes')
    {
       
        DB::table('products')->where('product_id',$product_id)->update(['active_status' =>'YES']);
    }
    else{

        DB::table('products')->where('product_id',$product_id)->update(['active_status' =>'NO']);

    }
    return redirect('/view_product');
}
public function view_details()
{
    $product_id=$_GET['product_id'];


    $feature=DB::table('product_features')->where('product_id',$product_id)->get();
    $specification=DB::table('product_spacifications')->where('product_id',$product_id)->get();
    $price=DB::table('product_prices')->where('product_id',$product_id)->get();
   // $color2=DB::table('product_colors')->where('product_id',$product_id)->get();
    $image=DB::table('product_images')->where('product_id',$product_id)->get();

    $product2=DB::table('products')->where('product_id',$product_id)->first();
    $cat=DB::table('cats')->where('cat_id',$product2->cat_id)->first();
    $sub_cat=DB::table('sub_cats')->where('sub_cat_id',$product2->sub_cat_id)->first();
    $sub_sub_cat=DB::table('sub_sub_cats')->where('sub_sub_cat_id',$product2->sub_sub_cat_id)->first();

    $brand=DB::table('brands')->where('brand_id',$product2->brand_id)->first();
    
 
return view('admin.view_details')->with('product_id',$product_id)->with('feature',$feature)->with('product2',$product2)->with('price',$price)->with('specification',$specification)->with('image',$image)->with('cat',$cat)->with('sub_cat',$sub_cat)->with('sub_sub_cat',$sub_sub_cat)->with('brand',$brand);

}


protected function update_product_details(Request $req)
{
   $product_id=$req->product_id;
   $pro=DB::table('products')->where('product_id',$product_id)->first();
   $brand=DB::table('brands')->get();
   $cat=DB::table('cats')->get();
   return view('admin.update_product_details')->with('pro',$pro)->with('cat1',$cat)->with('brand',$brand);
 }


 protected function update_product_details_code(Request $req)
{

    $validatedData = $req->validate([
        'product_name' => 'required',
        'hsn_code' => 'required',
        'brand_id' => 'required',
        'cat_id' => 'required',
        'sub_cat_id' => 'required',
        'sub_sub_cat_id' => 'required',
    ]);

       
    


   $product_id=$req->product_id;
   $product_name=$req->product_name;
   $hsn_code=$req->hsn_code;
   $brand_id=$req->brand_id;
   $cat_id=$req->cat_id;
   $sub_cat_id=$req->sub_cat_id;
   $sub_sub_cat_id=$req->sub_sub_cat_id;
  

   $pro=DB::table('products')->where('product_id',$product_id)->update(['product_name'=>$product_name,'hsn_code'=>$hsn_code,'cat_id'=>$cat_id,'sub_cat_id'=>$sub_cat_id,'sub_sub_cat_id'=>$sub_sub_cat_id,'brand_id'=>$brand_id]);
   return redirect("view_details?product_id=$product_id");
 }


 protected function update_product_description(Request $req){
   $product_id=$req->product_id;
   $pro=DB::table('products')->where('product_id',$product_id)->first();
   return view('admin.update_product_description')->with('pro',$pro)->with('product_id',$product_id);
  
 }

 protected function update_product_description_code(Request $req){
   $product_id=$req->product_id;
    $description=$req->editor1;


   $pro=DB::table('products')->where('product_id',$product_id)->update(['description'=>$description]);
   return redirect("view_details?product_id=$product_id");
  
 }




 protected function add_features(Request $req){
    $product_id=$req->id;


  
   $pro=DB::table('products')->where('product_id',$product_id)->first();
   return view('admin.add_feature')->with('pro',$pro)->with('product_id',$product_id);
  
 }

 protected function add_features_code(Request $req){
   $product_id=$req->id;

   
   $token=$req->_token;
   $feture=$req->input('feature');

   for($i=0;$i<count($feture);$i++)
   {
       $features= new product_features;
       $feature1=$feture[$i];
       $features->features=$feature1;
       $features->product_id=$product_id;
       $features->remember_token=$token;
       $features->save();
   }

   return redirect("view_details?product_id=$product_id");
  
 }

 function delete_feature(Request $req)
 {
     $product_id=$req->product_id;
     $feature=$req->id;
     $pro=DB::table('product_features')->where('product_feature_id',$feature)->delete();
     return redirect("view_details?product_id=$product_id");

 }


 protected function add_specification(Request $req){
   $product_id=$req->id;
   $pro=DB::table('products')->where('product_id',$product_id)->first();
   return view('admin.add_specification')->with('pro',$pro)->with('product_id',$product_id);
  
 }


 protected function add_specification_code(Request $req){
   $product_id=$req->id;


   $token=$req->_token;

 $specification_name=$req->input('specification');
 $specification_type=$req->input('specification1');

  
   for($i=0;$i<count($specification_name);$i++)
   {
   
       $specification= new product_spacification;
       $specification_name1=$specification_name[$i];
       $specification_type1=$specification_type[$i];
       $specification->title=$specification_name1;
       $specification->description=$specification_type1;
       $specification->product_id=$product_id;
       $specification->remember_token=$token;
       $specification->save();
   }
   


   return redirect("view_details?product_id=$product_id");
  
 }

 

 function delete_specification(Request $req)
 {
     $specification_id=$req->id;

     $product_id=$req->product_id;

     $pro=DB::table('product_spacifications')->where('product_spacifications_id',$specification_id)->delete();

     return redirect("view_details?product_id=$product_id");

 }
 




 public function add_product_image(Request $req)
{
    $id=$req->input('product_id');
   
    $image_array3=$req->file('image');
  
    for($i=0;$i<count($image_array3);$i++)
    {

/*
      $image_ext4=$image[$i]->getClientOriginalExtension();
      $new_image_name4=rand(123456,999999).".".$image_ext4;
      $destination_path4=public_path("/product_image/");
      $image[$i]->move($destination_path4,$new_image_name4);
 
*/

       $image_name[$i] = uniqid() . '.' . $image_array3[$i]->getClientOriginalExtension();

       $destinationPath = public_path('/product_image');
       $destinationPath1 = public_path('/small_product_image');

       $resize_image[$i] = Image::make($image_array3[$i]->getRealPath());

       $resize_image[$i]->resize(1000,1000, function($constraint){
       // $constraint->aspectRatio();
       })->save($destinationPath . '/' . $image_name[$i]);

      $resize_image[$i]->resize(255,255, function($constraint){
           // $constraint->aspectRatio();
           })->save($destinationPath1 . '/' . $image_name[$i]);


       $contact = new product_image;
       $contact->product_id =$id;
     
       $contact->image =$image_name[$i];
       $contact->remember_token = $req->input('_token');
       $contact->save();
    }
    return redirect('/view_details?product_id='.$id.'');
   /* $banner=DB::table('display_banners')->get();
   return view('admin.display_banner')->with('banner',$banner);*/

}
public function delete_product_image()
{
    $id=$_GET['id'];
try{
    $image=DB::table('product_images')->where('product_image_id',$id)->first();
    $link="product_image/".$image->image;
    $link1="small_product_image/".$image->image;

    unlink($link);
    unlink($link1);
  }
  catch (\Exception $e)
 {
      
 }
    $delete_query=DB::table('product_images')->where('product_image_id',$id)->delete();
    return redirect()->back();
   /* $banner=DB::table('display_banners')->get();
   return view('admin.display_banner')->with('banner',$banner);*/

}
protected function update_price(){
   $price_id=$_GET['product_prices_id'];

   $pro=DB::table('product_prices')->where('product_prices_id',$price_id)->first();
   return view('admin.update_price')->with('price1',$pro);
  
 }
 protected function update_price_code(Request $req){


   $id=$req->input('product_price_id');
   $size=$req->input('size');
   $mrp=$req->input('mrp');
   $selling_price=$req->input('selling_price');
   $mediator_price=$req->input('mediator_price');



   DB::table('product_prices')->where('product_prices_id',$id)->update(['size'=> $size,'mrp'=> $mrp,'selling_price'=> $selling_price,'mediator_price'=> $mediator_price]);

  


   $product_id=$req->input('product_id');
   return redirect('/view_details?product_id='.$product_id.'');
 }

 public function inventory_details(Request $req)
 {
    $product_price_id=$req->product_price_id;
    $product_id=$req->product_id;
   $product1=DB::table('products')->join('product_prices','products.product_id','=','product_prices.product_id')->where('product_prices.product_id',$product_id)->where('product_prices.product_prices_id',$product_price_id)->first();
   $stoke_trans=DB::table('stock_trasanctions')->where('product_price_id','=',$product_price_id)->get();
     return view('admin.inventory_details')->with('product',$product1)->with('stoke_trans',$stoke_trans);
 }
 public function add_stock(Request $req)
 {
    $product_price_id=$req->product_price_id;
    $product_id=$req->product_id;
   $product1=DB::table('products')->join('product_prices','products.product_id','=','product_prices.product_id')->where('product_prices.product_id',$product_id)->where('product_prices.product_prices_id',$product_price_id)->first();
   $stoke_trans=DB::table('stock_trasanctions')->where('product_price_id','=',$product_price_id)->get();
     return view('admin.add_stock')->with('product',$product1)->with('stoke_trans',$stoke_trans);
 }

 public function add_stock_code(Request $req)
 {
     $product_price_id=$req->product_price_id;
     $product_id=$req->product_id;
     $transfer_id=$req->transfer_id;
    $remarks=$req->remarks;
    $transfer_qty=$req->transfer_qty;

    $product1=DB::table('products')->join('product_prices','products.product_id','=','product_prices.product_id')->where('product_prices.product_id',$product_id)->where('product_prices.product_prices_id',$product_price_id)->first();
  
    $total_stock=($product1->total_stock)+$transfer_qty;

    $available_stock=($product1->available_stock)+$transfer_qty;
    DB::table('product_prices')->where('product_prices.product_id',$product_id)->where('product_prices.product_prices_id',$product_price_id)->update(['total_stock'=>$total_stock,'available_stock'=>$available_stock]);

    $stock= new stock_trasanction;
    $stock->product_id=$product_id;
    $stock->product_price_id=$product_price_id; 
    $stock->quantity=$transfer_qty;
    $stock->type='CREDIT';
    $stock->remarks='ADD STOCK';
    $stock->transfer_id=$transfer_id;
    $stock->total_quantity=$available_stock;

    $stock->save();

    return redirect('inventory_details?product_id='.$product_id.'&product_price_id='.$product_price_id);
 
 }
 
 public function view_promotion_banner()
 {
     
    $banner1=DB::table('promotion_banners')->limit(4)->get();
    $banner2=DB::table('promotion_banners')->skip(4)->take(2)->get();
   
    return view('admin.promotion_banner')->with('banner1',$banner1)->with('banner2',$banner2);
 }
 public function add_promotion_banner(Request $req)
 {
    $image=$req->file('img_prom');
    $dis_id=$req->input('dis_id');


   /* $image_ext1=$image_array->getClientOriginalExtension();
				$new_image_name1=rand(123456,999999).".".$image_ext1;
				$destination_path1=public_path("/banner/");
                $image_array->move($destination_path1,$new_image_name1);*/
                try{
                   $image2=DB::table('promotion_banners')->where('dis_id',$dis_id)->first();
                $link="promotion_banner_image/".$image2->image;
                unlink($link);
                }
                catch (\Exception $e)
                {
                     
                }

                $image_name = time() . '.' . $image->getClientOriginalExtension();

                $destinationPath = public_path('/promotion_banner_image');
           
                $resize_image = Image::make($image->getRealPath());
           
                $resize_image->resize(575,200, function($constraint){
                // $constraint->aspectRatio();
                })->save($destinationPath . '/' . $image_name);
           
               /* $destinationPath = public_path('/resize2');
           
                $image->move($destinationPath, $image_name);*/
           

                DB::table('promotion_banners')->where('dis_id',$dis_id)->update(['image' =>$image_name]);

                return redirect()->back();
 }




 public function pending_order(Request $req)
 {
  $booking_details=DB::table('bookings')
  ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
  ->join('users','users.id','=','bookings.customer_id')
  ->where('book_multi_items.order_status',1)
  ->orwhere('book_multi_items.order_status',2)
  ->orwhere('book_multi_items.order_status',3)
  ->groupby('book_multi_items.booking_id')
  ->get();

   return view('admin.pending_order')->with('booking',$booking_details);

 }

 public function cancel_order(Request $req)
 {
  $booking_details=DB::table('bookings')
  ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
  ->join('users','users.id','=','bookings.customer_id')
  ->where('book_multi_items.order_status',8)
  ->groupby('book_multi_items.booking_id')
  ->get();

   return view('admin.cancel_booking')->with('booking',$booking_details);

 }


 public function return_order(Request $req)
 {
  $booking_details=DB::table('bookings')
  ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
  ->join('users','users.id','=','bookings.customer_id')
  ->where('book_multi_items.order_status',5)
  ->orwhere('book_multi_items.order_status',6)
  ->groupby('book_multi_items.booking_id')
  ->get();

   return view('admin.return_booking')->with('booking',$booking_details);

 }


 public function complete_return_order(Request $req)
 {

  
   
 
  if(!isset($req->complete_order))
  {
  $booking_details=DB::table('bookings')
  ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
  ->join('users','users.id','=','bookings.customer_id')
  ->where('book_multi_items.order_status',7)
  
  ->groupby('book_multi_items.booking_id')
  ->get();
  }
  

   return view('admin.complete_return_order')->with('booking',$booking_details);

 }

 










 public function complete_order(Request $req)
 {
  $booking_details=DB::table('bookings')
  ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
  ->join('users','users.id','=','bookings.customer_id')
  ->where('book_multi_items.order_status',4)
  ->groupby('book_multi_items.booking_id')
  ->get();

   return view('admin.complete_order')->with('booking',$booking_details);

 }


 

 public function view_booking_details(Request $req)
 {
  $booking_id=$_GET['booking_id'];

  $booking_details=DB::table('bookings')->where('booking_id',$booking_id)->get();
  //for book_muti_items
  if(isset($req->cancel)){
    $multi_details=DB::table('bookings')->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')->join('products','products.product_id','=','book_multi_items.product_id')->join('users','users.id','=','bookings.customer_id')->where('bookings.booking_id',$booking_id)->where('book_multi_items.order_status',8)->get();
 
    








  }elseif(isset($req->return)){
    $multi_details=DB::table('bookings')->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')->join('products','products.product_id','=','book_multi_items.product_id')->join('users','users.id','=','bookings.customer_id')->where('bookings.booking_id',$booking_id)->where('book_multi_items.order_status',5)->orwhere('book_multi_items.order_status',6)->get();
 
  }elseif(isset($req->complete_return)){
    $multi_details=DB::table('bookings')->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')->join('products','products.product_id','=','book_multi_items.product_id')->join('users','users.id','=','bookings.customer_id')->where('bookings.booking_id',$booking_id)->where('book_multi_items.order_status',7)->get();
 
  }elseif(isset($req->pending_order)){
    $multi_details=DB::table('bookings')->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')->join('products','products.product_id','=','book_multi_items.product_id')->join('users','users.id','=','bookings.customer_id')->where('bookings.booking_id',$booking_id)->where('book_multi_items.order_status',1)->orwhere('book_multi_items.order_status',2)->orwhere('book_multi_items.order_status',3)->get();
 
  }elseif(isset($req->complete_order)){
    $multi_details=DB::table('bookings')->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')->join('products','products.product_id','=','book_multi_items.product_id')->join('users','users.id','=','bookings.customer_id')->where('bookings.booking_id',$booking_id)->where('book_multi_items.order_status',4)->get();
 
  }
  elseif(isset($req->complete_return_order)){
    $multi_details=DB::table('bookings')->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')->join('products','products.product_id','=','book_multi_items.product_id')->join('users','users.id','=','bookings.customer_id')->where('bookings.booking_id',$booking_id)->where('book_multi_items.order_status',7)->get();

  }
  else{
    $multi_details=DB::table('bookings')->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')->join('products','products.product_id','=','book_multi_items.product_id')->join('users','users.id','=','bookings.customer_id')->where('bookings.booking_id',$booking_id)->get();
  }
   
  
  
  return view('admin.view_booking_details')->with('booking_details',$booking_details)->with('multi_details',$multi_details);
}

public function order_status(Request $req)
{

  $booking_id=$req->booking_id;
   $booking_multi_id=$req->book_multi_id;
  $staTUS=DB::table('book_multi_items')->where('booking_id',$booking_id)->where('multi_id',$booking_multi_id)->first();
  $staTUS1=DB::table('bookings')->where('booking_id',$booking_id)->first();

  $order_price=$staTUS1->price;
  $wallet_price=$staTUS1->wallet_amount;
  $product_price=$staTUS->product_price;
  $user_id=$staTUS1->customer_id;
  $order_id=$staTUS1->order_id;
  
  //print_r($staTUS);
  if($staTUS->order_status==3){
     DB::table('book_multi_items')->where('booking_id',$booking_id)->where('multi_id',$booking_multi_id)->update(['book_multi_items.order_status'=>$staTUS->order_status+1,'book_multi_items.delivery_date'=>date('Y-m-d')]);

  }
  elseif($staTUS->order_status==6)
  {
    DB::table('book_multi_items')->where('booking_id',$booking_id)->where('multi_id',$booking_multi_id)->update(['book_multi_items.order_status'=>$staTUS->order_status+1]);

if($wallet_price!=0)
{
  $persentage=($product_price/$order_price)*100;
  $refound_amount=$wallet_price*($persentage/100);


      $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
        $wallet_amount1=$wallet->wallet_ammount;
    
        $remaining_wallet_amount=$wallet_amount1+$refound_amount;
        $user_wallet1=new user_wallet_transaction;
        $user_wallet1->user_id=$user_id;
        $user_wallet1->transaction_amount=$refound_amount;
        $user_wallet1->after_transaction_amount=$remaining_wallet_amount;
        $user_wallet1->remarks='Return Refund';
        $user_wallet1->order_id=$order_id;
        $user_wallet1->tranasaction_type='Credit';
        $user_wallet1->booking_id=$booking_id;
        $user_wallet1->remember_token=$token;
        $user_wallet1->save();
        DB::table('user_wallets')->where('user_id',$user_id)->update(['wallet_ammount' => $remaining_wallet_amount]); 
       


}




  }
  else{
    DB::table('book_multi_items')->where('booking_id',$booking_id)->where('multi_id',$booking_multi_id)->update(['book_multi_items.order_status'=>$staTUS->order_status+1]);

  }
 
}


public function refund(Request $req)
{
    
    $token=$req->_token;
    $booking_id=$req->booking_id;
    $refound_amount=$req->refound_amount;
    $multi_id=$req->multi_id;
    $user_id=$req->user_id;
    $order_id=$req->order_id;
    
    

    $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
    $wallet_amount1=$wallet->wallet_ammount;
    
        $remaining_wallet_amount=$wallet_amount1+$refound_amount;
        $user_wallet1=new user_wallet_transaction;
        $user_wallet1->user_id=$user_id;
        $user_wallet1->transaction_amount=$refound_amount;
        $user_wallet1->after_transaction_amount=$remaining_wallet_amount;
        $user_wallet1->remarks='Return Refund';
        $user_wallet1->order_id=$order_id;
        $user_wallet1->tranasaction_type='Credit';
        $user_wallet1->booking_id=$booking_id;
        $user_wallet1->remember_token=$token;
        $user_wallet1->save();
        DB::table('user_wallets')->where('user_id',$user_id)->update(['wallet_ammount' => $remaining_wallet_amount]); 
        DB::table('book_multi_items')->where('multi_id',$multi_id)->update(['refund_status' => 'YES']); 

  
    
}

  function refund1(Request $req)
  {
    $token=$req->_token;
    $booking_multi_id=$req->booking_multi_id;
    DB::table('book_multi_items')->where('multi_id',$booking_multi_id)->update(['refund_status' => 'YES']); 
  }


public function htmlPDF58()
{

    $id=$_GET['id'];
    $book_multi=DB::table('book_multi_items')->join('bookings','book_multi_items.booking_id','=','bookings.booking_id')->join('users','bookings.customer_id','=','users.id')->where('multi_id',$id)->first();
    return view('admin.invoice')->with('book',$book_multi);
    
}

public function generatePDF58()
{
    
    $data = ['title' => 'SSJ Jewellers Invoice'];
    $pdf = PDF::loadView('admin.invoice1', $data)->setPaper('a4', 'landscape');
    return $pdf->download('invoice.pdf');
}



 //tax_report
 public function tax_report()
 {
  return view('/admin/tax_report');
 }

 
 public function tax_report_ajax()
 {
$date11=$_POST['date1'];
 $date1 = date("Y-m-d h:i:s", strtotime($date11));
 $date22=$_POST['date2'];
  $date2 = date("Y-m-d h:i:s", strtotime($date22));
$token=$_POST['_token'];

$text='Tax Report of '.date("d M, Y", strtotime($date11)).' to '.date("d M, Y", strtotime($date22));
$booking=DB::table('bookings')
                   ->join('book_multi_items','book_multi_items.booking_id','=','bookings.booking_id')
                   ->join('products','products.product_id','=','book_multi_items.product_id')
                   ->join('sub_cats','sub_cats.sub_cat_id','=','products.sub_cat_id')
                   ->join('users','users.id','=','bookings.customer_id')
                   ->whereBetween('book_multi_items.created_at', [$date1, $date2])
                   ->where('book_multi_items.order_status','=',4)
        
                   ->get();
                   $booking_count=DB::table('bookings')
                   ->join('book_multi_items','book_multi_items.booking_id','=','bookings.booking_id')
                   ->join('products','products.product_id','=','book_multi_items.product_id')
                   ->join('sub_cats','sub_cats.sub_cat_id','=','products.sub_cat_id')
                   ->join('users','users.id','=','bookings.customer_id')
                   ->whereBetween('book_multi_items.created_at', [$date1, $date2])
                   ->where('book_multi_items.order_status','=',4)
        
                   ->count();


  return view('/admin/tax_report_ajax')->with('booking',$booking)->with('text',$text)->with('booking_count',$booking_count);
 }


 //sales_report
 
 public function sales_report()
 {
  return view('/admin/sales_report');
 }

 
 public function sales_report_ajax()
 {
  $date11=$_POST['date1'];
  $date1 = date("Y-m-d h:i:s", strtotime($date11));
  $date22=$_POST['date2'];
   $date2 = date("Y-m-d h:i:s", strtotime($date22));
$token=$_POST['_token'];

$text='Sales Report of '.date("d M, Y", strtotime($date11)).' to '.date("d M, Y", strtotime($date22));
$booking=DB::table('bookings')
                   ->join('book_multi_items','book_multi_items.booking_id','=','bookings.booking_id')
                   ->join('products','products.product_id','=','book_multi_items.product_id')
                   ->join('sub_cats','sub_cats.sub_cat_id','=','products.sub_cat_id')
                   ->join('users','users.id','=','bookings.customer_id')
                   ->whereBetween('book_multi_items.created_at', [$date1, $date2])
                   ->where('book_multi_items.order_status','=',4)
                   
                   ->get();
                   $booking_count=DB::table('bookings')
                   ->join('book_multi_items','book_multi_items.booking_id','=','bookings.booking_id')
                   ->join('products','products.product_id','=','book_multi_items.product_id')
                   ->join('sub_cats','sub_cats.sub_cat_id','=','products.sub_cat_id')
                   ->join('users','users.id','=','bookings.customer_id')
                   ->whereBetween('book_multi_items.created_at', [$date1, $date2])
                   ->where('book_multi_items.order_status','=',4)
                   
                   ->count();


  return view('/admin/sales_report_ajax')->with('booking',$booking)->with('text',$text)->with('booking_count',$booking_count);
 }

 
}
