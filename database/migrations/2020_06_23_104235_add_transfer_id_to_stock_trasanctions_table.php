<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransferIdToStockTrasanctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock_trasanctions', function (Blueprint $table) {
            $table->string('remarks')->nullable();
            $table->string('transfer_id')->default(00000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stock_trasanctions', function (Blueprint $table) {
            $table->dropColumn('remarks');
            $table->dropColumn('transfer_id');
        });
    }
}
