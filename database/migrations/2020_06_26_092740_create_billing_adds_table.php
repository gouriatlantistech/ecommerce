<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingAddsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_adds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('user_id')->nullable();
            $table->String('address')->nullable();
            $table->String('landmark')->nullable();
            $table->String('location')->nullable();
            $table->String('pincode')->nullable();
            $table->String('city')->nullable();
            $table->String('district')->nullable();
            $table->String('state')->nullable();

            $table->rememberToken();
            $table->timestamps();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_adds');
    }
}
