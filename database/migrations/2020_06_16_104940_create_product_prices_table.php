<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_prices', function (Blueprint $table) {
            $table->bigIncrements('product_prices_id');
            $table->foreignId('product_id')->default(0);
            $table->string('size')->default(0);
            $table->integer('mrp')->default(0);
            $table->integer('selling_price')->default(0);
            $table->integer('mediator_price')->default(0);
            $table->integer('total_stock')->default(0);
            $table->integer('available_stock')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_prices');
    }
}
