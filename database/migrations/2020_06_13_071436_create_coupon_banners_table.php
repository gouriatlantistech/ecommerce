<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('coupon_image')->nullable();
            $table->string('title')->nullable();
      
            $table->integer('coupon_value')->default(0);
            $table->string('coupon_code')->nullable();
            $table->string('coupon_validity')->nullable();
            $table->integer('min_price')->default(0);
            $table->string('active_status')->default('YES');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_banners');
    }
}
