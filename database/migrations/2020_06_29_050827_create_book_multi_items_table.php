<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookMultiItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_multi_items', function (Blueprint $table) {
            $table->bigIncrements('multi_id');

            $table->integer('booking_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->integer('product_prices_id')->nullable();
            $table->String('quantity1')->nullable();
            $table->String('review_status')->default('NO');
            $table->String('order_status')->default('1');
            $table->String('delivery_date')->nullable();
            $table->String('weight1')->nullable();
            $table->String('redeem_status')->default('NO');
            $table->String('payment_status')->nullable();
            $table->String('return_status')->nullable();
            $table->String('product_price')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_multi_items');
    }
}
