<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoneCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('none_carts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->default(0);
            $table->integer('quantity')->default(1);
            $table->foreignId('product_id')->default(0);
            $table->foreignId('product_price_id')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('none_carts');
    }
}
