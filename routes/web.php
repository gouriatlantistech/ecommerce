<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::group(['domain' =>  env("USER_APP_URL")], function () {
Route::get('/', 'HomeController@index')->name('/');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/shop', 'HomeController@shop')->name('shop');
Route::post('/fetch_data', 'HomeController@fetch_data')->name('fetch_data');

Route::get('/product_details', 'HomeController@product_details')->name('product_details');


Route::post('loginWithOtp', 'UserController@loginWithOtp');
Route::get('loginWithOtp', 'UserController@loginWithOtp1')->name('loginWithOtp');
Route::get('smsRequest', 'UserController@smsRequest');
Route::post('sendOtp', 'UserController@sendOtp');
Route::post('sendOtp1', 'HomeController@sendOtp1');
Route::post('sendOtp11', 'HomeController@sendOtp11');
Route::get('mediator_register', 'UserController@register1')->name('mediator_register');
Route::post('mediator_register', 'UserController@register');


Route::post('/wishlist_ajax', 'HomeController@wishlist_ajax');
Route::post('/addtowishlist_ajax', 'HomeController@addtowishlist_ajax')->name('addtowishlist_ajax');
Route::post('/addtowishlist_ajax1', 'HomeController@addtowishlist_ajax1')->name('addtowishlist_ajax1');

Route::post('/removetowishlist_ajax', 'HomeController@removetowishlist_ajax')->name('removetowishlist_ajax');


Route::post('/cart_ajax', 'HomeController@cart_ajax');
Route::post('/addtocart_ajax', 'HomeController@addtocart_ajax')->name('addtocart_ajax');
Route::post('/add_to_noe_cart', 'HomeController@add_to_noe_cart')->name('add_to_noe_cart');
Route::post('/add_to_cart_ajax1', 'HomeController@add_to_cart_ajax1')->name('add_to_cart_ajax1');
Route::post('/removetocart_ajax', 'HomeController@removetocart_ajax')->name('removetocart_ajax');
Route::post('/toggle', 'HomeController@toggle')->name('toggle');
Route::post('/toggle_wish', 'HomeController@toggle_wish')->name('toggle_wish');



Route::post('/price', 'HomeController@price')->name('price');

// check out section 
Route::get('/aa', 'HomeController@aa')->name('aa');
Route::post('/wallet', 'HomeController@wallet')->name('wallet');

Route::get('/checkout', 'AfterLoginController@checkout')->name('checkout');
Route::post('/address_submit', 'AfterLoginController@address_submit')->name('address_submit');
Route::post('/fetch_address_checkout', 'AfterLoginController@fetch_address_checkout')->name('fetch_address_checkout');
Route::post('/address_update', 'AfterLoginController@address_update')->name('address_update');
Route::post('/shipping_calculation', 'AfterLoginController@shipping_calculation')->name('shipping_calculation');
Route::post('/wallet_calculation', 'AfterLoginController@wallet_calculation')->name('wallet_calculation');
Route::post('/coupon_calculation', 'AfterLoginController@coupon_calculation')->name('coupon_calculation');
Route::post('/pay', 'AfterLoginController@pay')->name('pay');
Route::post('/order_submit', 'AfterLoginController@order_submit')->name('order_submit');
Route::get('/successfull', 'AfterLoginController@successfull')->name('successfull');

Route::post('/return_status', 'AfterLoginController@return_status')->name('return_status');
Route::post('/cencel_status', 'AfterLoginController@cencel_status')->name('cencel_status');



//check_out End
Route::get('/wishlist', 'AfterLoginController@wishlist')->name('wishlist');
Route::post('/wishlist_page_ajax', 'AfterLoginController@wishlist_page_ajax')->name('wishlist_page_ajax');

Route::get('/cart', 'AfterLoginController@cart')->name('cart');
Route::post('/cart_page_ajax', 'AfterLoginController@cart_page_ajax')->name('cart_page_ajax');
Route::post('/update_qty', 'AfterLoginController@update_qty')->name('update_qty');


Route::get('/profile', 'AfterLoginController@profile')->name('profile');
Route::post('/update_profile', 'AfterLoginController@update_profile')->name('update_profile');
Route::post('/change_password','AfterLoginController@change_password');


Route::post('billing_address', 'AfterLoginController@billing_address')->name('billing_address');
Route::post('fetch_address', 'AfterLoginController@fetch_address')->name('fetch_address');

Route::get('order', 'AfterLoginController@order')->name('order');

Route::post('/subscribe', 'HomeController@subscribe');
Route::post('/unsubscribe', 'HomeController@unsubscribe');
Route::post('/pinckeck', 'HomeController@pinckeck')->name('pinckeck');

Route::get('/refer', 'AfterLoginController@refer')->name('refer');
Route::post('/refersend', 'AfterLoginController@refersend');
Route::post('/sendReview', 'AfterLoginController@sendReview');
Route::post('/fafasend','HomeController@fafasend');

Route::get('/contact', 'HomeController@contact')->name('contact');
Route::post('/contactmail','HomeController@contactmail');

Route::get('/about', 'HomeController@about')->name('about');
Route::get('/faq', 'HomeController@faq')->name('faq');
Route::get('/privacy', 'HomeController@privacy')->name('privacy');
Route::get('/trams', 'HomeController@trams')->name('trams');
Route::get('/return', 'HomeController@return')->name('return');

Route::get('/offer', 'HomeController@offer')->name('offer');
Route::get('/my_wallet', 'AfterLoginController@my_wallet')->name('my_wallet');
Route::post('/account', 'AfterLoginController@account')->name('account');

Route::get('/reset_link', 'AccountsController@reset_link')->name('reset_link');
Route::get('/reset1', 'AccountsController@reset1')->name('reset1');
Route::post('/resetPassword1', 'AccountsController@resetPassword1')->name('resetPassword1');
Route::post('reset_password_without_token', 'AccountsController@validatePasswordRequest')->name('reset_password_without_token');



});

Route::group(['domain' =>  env("ADMIN_APP_URL")], function () {
    Route::get('admin-login', 'UserController@admin_login')->name('admin_login');
    Route::get('/admin_logout', 'AdminController@admin_logout')->name('admin_logout');
    Route::get('/', 'AdminController@index')->name('admin_home');
    Route::get('/developer_setting', 'AdminController@developer_setting')->name('developer_setting');

    Route::get('/display_banner','AdminController@display_banner')->name('display_banner');
    Route::post('/add_display_banner','AdminController@add_display_banner');
    Route::get('/delete_display_banner','AdminController@delete_display_banner')->name('delete_display_banner');

     //coupon
     Route::get('/view_coupon','AdminController@view_coupon')->name('view_coupon');
     Route::get('/add_coupon','AdminController@add_coupon')->name('add_coupon');
     Route::post('/add_coupon_action','AdminController@add_coupon_action');
     Route::get('/update_coupon','AdminController@update_coupon')->name('update_coupon');
     Route::post('/update_coupon_action','AdminController@update_coupon_action');
     Route::get('/delete_coupon','AdminController@delete_coupon')->name('delete_coupon');
    
    

    //pincode
Route::get('/pincode','AdminController@admin_pincode')->name('pincode');
Route::get('/add_pincode','AdminController@admin_add_pincode');
Route::post('/add_pincode_code','AdminController@admin_add_pincode_code');
Route::get('/update_pincode','AdminController@admin_update_pincode');
Route::post('/update_pincode_code','AdminController@admin_update_pincode_code');
Route::get('/pincode_status','AdminController@pincode_status');
Route::get('/delete_pincode','AdminController@delete_pincode');


//user
Route::get('/normal_user','AdminController@normal_user')->name('normal_user');
Route::get('/mediator_user','AdminController@mediator_user')->name('mediator_user');
Route::get('/subscribe_user','AdminController@subscribe_user')->name('subscribe_user');
Route::post('/user_block','AdminController@user_block');

Route::get('/inventory_report','AdminController@inventory_report')->name('inventory_report');
Route::post('/sub_cat_ajax','AdminController@sub_cat_ajax');
Route::post('/sub_sub_cat_ajax','AdminController@sub_sub_cat_ajax');
Route::get('/view_product','AdminController@view_product')->name('view_product');

Route::get('/view_category','AdminController@category')->name('view_category');
Route::get('/add_category','AdminController@add_category')->name('add_category');
Route::post('/category_action','AdminController@category_action');
Route::get('/delete_category','AdminController@cat_delete');
Route::post('/cat_update_action','AdminController@cat_update_action');
Route::get('/update_category','AdminController@cat_update');

Route::post('/sub_cat_update_action','AdminController@sub_cat_update_action');
Route::get('/update_sub_category','AdminController@sub_cat_update');
Route::get('/delete_sub_category','AdminController@sub_cat_delete');
Route::post('/sub_category_action','AdminController@sub_category_action');
Route::get('/add_sub_category','AdminController@add_sub_category');
Route::get('/view_sub_category','AdminController@view_sub_category')->name('view_sub_category');
Route::get('/all_cats_details', 'AdminController@all_cats_deail')->name('all_cats_details');

Route::get('/view_sub_sub_category','AdminController@view_sub_sub_category')->name('view_sub_sub_category');
Route::get('/add_sub_sub_category','AdminController@add_sub_sub_category');
Route::post('/sub_sub_category_action','AdminController@sub_sub_category_action');
Route::get('/update_sub_sub_category','AdminController@sub_sub_cat_update');
Route::post('/sub_sub_cat_update_action','AdminController@sub_sub_cat_update_action');
Route::get('/delete_sub_sub_category','AdminController@sub_sub_cat_delete');

Route::get('/brand','AdminController@brand')->name('brand');
Route::get('/add_brand','AdminController@add_brand');
Route::post('/add_brand_action','AdminController@add_brand_action');
Route::get('/update_brand','AdminController@update_brand');
Route::post('/update_brand_action','AdminController@update_brand_action');
Route::get('/delete_brand','AdminController@delete_brand');

Route::get('/add_product','AdminController@add_product')->name('add_product');
Route::post('/product_action','add_product@product_action');
Route::get('/view_details','AdminController@view_details');


Route::get('/approve1','AdminController@approve1');

Route::get('/add_specification','AdminController@add_specification');
Route::get('/add_features','AdminController@add_features');
Route::get('/update_product_details','AdminController@update_product_details');
Route::get('/update_product_description','AdminController@update_product_description');
Route::post('/update_product_description_code','AdminController@update_product_description_code');
Route::post('/update_product_details_code','AdminController@update_product_details_code');


Route::post('/add_product_image','AdminController@add_product_image');
Route::get('/delete_product_image','AdminController@delete_product_image');

Route::post('/add_features_code','AdminController@add_features_code');
Route::post('/add_specification_code','AdminController@add_specification_code');
Route::get('/delete_feature','AdminController@delete_feature');
Route::get('/delete_specification','AdminController@delete_specification');
Route::get('/update_price','AdminController@update_price');
Route::post('/update_price','AdminController@update_price_code');

Route::get('/inventory_details','AdminController@inventory_details')->name('inventory_details');
Route::get('/add_stock','AdminController@add_stock');
Route::post('/add_stock_code','AdminController@add_stock_code');

Route::get('/promotion_banner','AdminController@view_promotion_banner');
Route::post('/add_promotion_banner','AdminController@add_promotion_banner');

Route::get('/mediator_approve','AdminController@mediator_approve');

Route::get('/pending_order','AdminController@pending_order');
Route::get('/view_booking_details','AdminController@view_booking_details');
Route::post('/order_status','AdminController@order_status');
Route::post('/refund','AdminController@refund');
Route::post('/refund1','AdminController@refund1');
//review





Route::get('/complete_order','AdminController@complete_order');
Route::get('/cancel_order','AdminController@cancel_order');
Route::get('/return_order','AdminController@return_order');
Route::get('/pending_return_order','AdminController@pending_return_order');
Route::get('/complete_return_order','AdminController@complete_return_order');

Route::get('/invoice','AdminController@htmlPDF58');
Route::get('generatePDF58','AdminController@generatePDF58');

//tax_report
Route::get('/tax_report','AdminController@tax_report')->name('tax_report');
Route::post('/tax_report_ajax','AdminController@tax_report_ajax')->name('tax_report_ajax');

//sales_report
Route::get('/sales_report','AdminController@sales_report')->name('sales_report');
Route::post('/sales_report_ajax','AdminController@sales_report_ajax')->name('sales_report_ajax');

Route::get('/db_backup','AdminController@db_backup')->name('db_backup');
});